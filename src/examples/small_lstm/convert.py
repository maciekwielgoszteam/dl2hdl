import time

import torch

from examples.small_lstm.model import MyNet
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.model import MyHdlModel


def main():
    print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Creating model...')

    model = MyNet()
    model.load_state_dict(torch.load('MyNet.pt'))

    hdl_model = MyHdlModel(torch_model=model, seq_len=6, fxp=FixedDef(6, 10))
    hdl_model.convert(path='', name='my_net')

    print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')


if __name__ == '__main__':
    main()
