import os
import time

import numpy as np
import torch

from examples.small_lstm.model import MyNet
from dl2hdl.basic.cpp_if import torch_data_set_to_cpp_string, numpy_ndaray_to_cpp_str
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.layers.tests.torch_dataset import RandomSeries
from dl2hdl.model import MyHdlModel
from dl2hdl.tests.hdl_test import HdlTest


class TestMyNet(HdlTest):

    def setUp(self):
        super().setUp()
        self.trace_save_path = ''
        self.vhdl_output_path = ''
        self.data_path = ''
        self.model_path = ''
        self.raw_name = 'MyNet'
        self.fxp = FixedDef(6, 10)
        self.test_sequences = 5
        self.seq_len = 6
        self.pruning = 0.25
        self.net_name = self.raw_name

    def prun(self, model, prun=None):
        prun = prun if prun is not None else self.pruning
        self.prun_weight(model.lstm_0.weight_ih_l0, prun)
        self.prun_weight(model.lstm_0.weight_hh_l0, prun)
        self.prun_weight(model.lstm_1.weight_ih_l0, prun)
        self.prun_weight(model.lstm_1.weight_hh_l0, prun)
        self.prun_weight(model.linear_2.weight, prun)
        self.prun_weight(model.linear_4.weight, prun)
        return model

    def test_my_net(self):
        new = False
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: testing...')
        test_set = RandomSeries(shape=(self.seq_len, 4), num_of_samples=self.test_sequences)
        model = MyNet()
        model_path = os.path.join(self.model_path, self.net_name + '.pt')
        if new:
            model = self.prun(model)
            torch.save(model.state_dict(), model_path)
        else:
            model.load_state_dict(torch.load(model_path))

        hdl_model = MyHdlModel(model, seq_len=self.seq_len, fxp=self.fxp)
        out_true = self.infer_net(model, test_set)
        out_true = [each[0].tolist() for each in out_true]
        out_data = self._single_run(test_set.data, hdl_model, fxp=self.fxp)
        out_data = [[self.fxp.to_float(y) for y in x] for x in out_data]

        out_true_np = np.asarray(out_true)
        out_data_np = np.asarray(out_data)
        path = os.path.join(self.data_path, self.net_name)
        os.makedirs(path, exist_ok=True)
        file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=self.fxp)
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_data_np), 'outputs')
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_true_np), 'outputs_true')
        os.makedirs(path, exist_ok=True)
        f = open(os.path.join(path, 'test_data.h'), 'w')
        print(file_str, file=f)
        f.close()

        numpy_array_to_txt_vec(self.fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(self.fxp.to_fixed(out_data_np), path, 'test_output')

        max_diff_allowed = 1. / 2 ** 3
        print(f'out_data:\n{out_data}')
        print(f'out_true:\n{out_true}')
        out_diff = np.abs(out_data_np - out_true_np)
        max_diff = np.amax(out_diff)
        mean_diff = np.mean(out_diff)
        print(f'max_diff_allowed = {max_diff_allowed}')
        print(f'max_diff = {max_diff}')
        print(f'mean_diff = {mean_diff}')
        self.assertTrue(max_diff < max_diff_allowed)
