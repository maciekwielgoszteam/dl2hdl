import torch
import torch.nn as nn
from ml.layers.lstm_hard import LSTMHardSigmoid


class MyNet(nn.Module):

    def __init__(self):
        super().__init__()
        self.lstm_0 = LSTMHardSigmoid(input_size=4, hidden_size=6, batch_first=True)
        self.lstm_1 = LSTMHardSigmoid(input_size=6, hidden_size=8, batch_first=True)
        self.linear_2 = nn.Linear(in_features=8, out_features=4)
        self.relu_3 = nn.ReLU()
        self.linear_4 = nn.Linear(in_features=4, out_features=2)
        self.softmax_5 = nn.Softmax(dim=1)

    def _init_hidden(self, device, batch_size=1):
        return ((torch.zeros(1, batch_size, 6).to(device), torch.zeros(1, batch_size, 6).to(device)),
                (torch.zeros(1, batch_size, 8).to(device), torch.zeros(1, batch_size, 8).to(device)))

    def forward(self, input_seq):
        hidden_init = self._init_hidden(input_seq.device, input_seq.shape[0])
        self.lstm_0.flatten_parameters()
        self.lstm_1.flatten_parameters()
        outputs, hidden = self.lstm_0(input_seq, hidden_init[0])
        outputs, hidden = self.lstm_1(outputs, hidden_init[1])
        outputs = self.linear_2(outputs[:, -1, :])
        outputs = self.relu_3(outputs)
        outputs = self.linear_4(outputs)
        outputs = self.softmax_5(outputs)
        return outputs
