import torch.nn as nn
from dl2hdl.model import MyHdlModel
from dl2hdl.basic.fixed_def import FixedDef


class MyNet(nn.Module):

    def __init__(self):
        super().__init__()
        self.linear_0 = nn.Linear(in_features=8, out_features=4)
        self.relu_1 = nn.ReLU()
        self.linear_2 = nn.Linear(in_features=4, out_features=2)
        self.softmax_3 = nn.Softmax(dim=1)

    def forward(self, inputs):
        outputs = self.linear_0(inputs)
        outputs = self.relu_1(outputs)
        outputs = self.linear_2(outputs)
        outputs = self.softmax_3(outputs)
        return outputs


hdl_model = MyHdlModel(torch_model=MyNet(), fxp=FixedDef(4, 4))
hdl_model.convert(path='', name='my_net')
