def torch_to_native(foo):
    def converter(x, *args, **kwargs):
        assert x.shape[0] == 1, 'Batches with more than 1 sample are not supported'
        x = x.reshape(x.shape[1:])
        return foo(x, *args, **kwargs)

    return converter


def torch_to_native_to_torch(foo):
    def converter(x, *args, **kwargs):
        assert x.shape[0] == 1, 'Batches with more than 1 sample are not supported'
        x = x.reshape(x.shape[1:])
        tmp = foo(x, *args, **kwargs)
        return tmp.reshape((1,) + tmp.shape)

    return converter
