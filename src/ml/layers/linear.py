import numpy as np
from dl2hdl.basic.fixed_def import FixedDef


def linear_raw(in_fm, coeffs=None, fxp=None):
    assert isinstance(fxp, FixedDef) or fxp is None, 'Provided fixed point definition is not valid'
    assert coeffs is not None, 'Provide coefficients'
    out_fm = np.zeros(coeffs.layer.out_features)
    for outF in range(coeffs.layer.out_features):
        for inF in range(len(in_fm)):
            if fxp is None:
                out_fm[outF] += in_fm[inF] * coeffs.weight[outF, inF]
            else:
                tmp = fxp.emul_mul(in_fm[inF], coeffs.weight[outF, inF])
                out_fm[outF] = fxp.emul_add(out_fm[outF], tmp)
        if fxp is None:
            out_fm[outF] += coeffs.bias[outF]
        else:
            out_fm[outF] = fxp.emul_add(out_fm[outF], coeffs.bias[outF])
    return out_fm


def linear(in_fm, out_features, weights, bias):
    tmp = np.zeros((1, out_features))
    for outF in range(tmp.shape[1]):
        for inF in range(in_fm.shape[1]):
            tmp[0, outF] += in_fm[0, inF] * weights[outF, inF]
        tmp[0, outF] += bias[outF]
    return tmp


def main():
    in_features = 8
    out_features = 2

    x = np.random.rand(1, in_features)
    w = np.random.rand(in_features, out_features)
    b = np.random.rand(out_features)
    y = linear(in_fm=x, out_features=out_features, weights=w, bias=b)
    print('x: ' + str(x))
    print('w: ' + str(w))
    print('b: ' + str(b))
    print('y: ' + str(y))


if __name__ == '__main__':
    main()
