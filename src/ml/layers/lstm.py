import numpy as np


def lstm_raw(input_data, hidden_data, coeffs):
    def sigmoid(x):
        return 1. / (1. + np.exp(-x))

    np_sigmoid = np.vectorize(sigmoid)

    sum_1 = lstm_raw_lin(input_data, hidden_data[0], coeffs)

    i_1 = np_sigmoid(sum_1[0:coeffs.hidden_size])
    f_1 = np_sigmoid(sum_1[coeffs.hidden_size:2 * coeffs.hidden_size])
    g_1 = np.tanh(sum_1[2 * coeffs.hidden_size:3 * coeffs.hidden_size])
    o_1 = np_sigmoid(sum_1[3 * coeffs.hidden_size:4 * coeffs.hidden_size])

    c_1 = np.multiply(f_1, hidden_data[1]) + np.multiply(i_1, g_1)
    h_1 = np.multiply(o_1, np.tanh(c_1))

    return h_1, (h_1, c_1)


def lstm_raw_lin(input_data, prev_out, coeffs):
    input_proc = np.dot(coeffs.w_ih, np.transpose(input_data)) + coeffs.b_ih
    hidden_proc = np.dot(coeffs.w_hh, np.transpose(prev_out)) + coeffs.b_hh
    return input_proc + hidden_proc
