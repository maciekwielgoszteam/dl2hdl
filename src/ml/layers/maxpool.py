import numpy as np

from ml.utils import conv_torch


def maxpool2d_raw(in_fm, pool_size=(2, 2)):
    if type(pool_size) is int:
        pool_size = (pool_size, pool_size)
    else:
        assert np.ndim(pool_size) == 1, 'Can only accept tuples with one dimension'
        assert len(pool_size) == 2, 'pool size can only have 2 parameters in 2D pooling'
    tmp = np.zeros((in_fm.shape[0], int(in_fm.shape[1] / pool_size[0]), int(in_fm.shape[2] / pool_size[1])))
    for outD in range(tmp.shape[0]):
        for outH in range(tmp.shape[1]):
            for outW in range(tmp.shape[2]):
                max_val = in_fm[outD, outH * pool_size[0], outW * pool_size[1]]
                for poolH in range(pool_size[0]):
                    for poolW in range(pool_size[1]):
                        if in_fm[outD, outH * pool_size[0] + poolH, outW * pool_size[1] + poolW] > max_val:
                            max_val = in_fm[outD, outH * pool_size[0] + poolH, outW * pool_size[1] + poolW]
                tmp[outD, outH, outW] = max_val
    return tmp


@conv_torch.torch_to_native
def maxpool2d_t2n(*args, **kwargs):
    return maxpool2d_raw(*args, **kwargs)


@conv_torch.torch_to_native_to_torch
def maxpool2d_torch_if(*args, **kwargs):
    return maxpool2d_raw(*args, **kwargs)


def maxpool1d_raw(in_fm, pool_size=(2,)):
    if type(pool_size) is not int:
        assert np.ndim(pool_size) == 1, 'Can only accept tuples with one dimension'
        assert len(pool_size) == 1, 'pool size can only have 2 parameters in 2D pooling'
        pool_size = pool_size[0]
    tmp = np.zeros((in_fm.shape[0], int(in_fm.shape[1] / pool_size)))
    for outD in range(tmp.shape[0]):
        for outH in range(tmp.shape[1]):
            max_val = in_fm[outD, outH * pool_size]
            for poolH in range(1, pool_size):
                if in_fm[outD, outH * pool_size + poolH] > max_val:
                    max_val = in_fm[outD, outH * pool_size + poolH]
            tmp[outD, outH] = max_val
    return tmp


@conv_torch.torch_to_native
def maxpool1d_t2n(*args, **kwargs):
    return maxpool1d_raw(*args, **kwargs)


@conv_torch.torch_to_native_to_torch
def maxpool1d_torch_if(*args, **kwargs):
    return maxpool1d_raw(*args, **kwargs)


def maxpool2d(in_fm, pool_size=(2, 2)):
    tmp = np.zeros((1, in_fm.shape[1], int(in_fm.shape[2] / pool_size[0]), int(in_fm.shape[3] / pool_size[1])))
    for outD in range(tmp.shape[1]):
        for outH in range(tmp.shape[2]):
            for outW in range(tmp.shape[3]):
                max_val = in_fm[0, outD, outH * pool_size[0], outW * pool_size[1]]
                for poolH in range(pool_size[0]):
                    for poolW in range(pool_size[1]):
                        if in_fm[0, outD, outH * pool_size[0] + poolH, outW * pool_size[1] + poolW] > max_val:
                            max_val = in_fm[0, outD, outH * pool_size[0] + poolH, outW * pool_size[1] + poolW]
                tmp[0, outD, outH, outW] = max_val
    return tmp


def maxpool1d(in_fm, pool_size=2):
    tmp = np.zeros((1, in_fm.shape[1], int(in_fm.shape[2] / pool_size[0]), int(in_fm.shape[3] / pool_size[1])))
    for outD in range(tmp.shape[1]):
        for outH in range(tmp.shape[2]):
            for outW in range(tmp.shape[3]):
                max_val = in_fm[0, outD, outH * pool_size[0], outW * pool_size[1]]
                for poolH in range(pool_size[0]):
                    for poolW in range(pool_size[1]):
                        if in_fm[0, outD, outH * pool_size[0] + poolH, outW * pool_size[1] + poolW] > max_val:
                            max_val = in_fm[0, outD, outH * pool_size[0] + poolH, outW * pool_size[1] + poolW]
                tmp[0, outD, outH, outW] = max_val
    return tmp


def main():
    x = np.random.rand(1, 3, 4, 4)
    y = maxpool2d(in_fm=x, pool_size=(2, 2))
    print('x: ' + str(x))
    print('y: ' + str(y))


if __name__ == '__main__':
    main()
