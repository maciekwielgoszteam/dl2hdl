import numpy as np


def relu(in_fm):
    tmp = np.zeros(in_fm.shape)
    for outD in range(tmp.shape[1]):
        for outH in range(tmp.shape[2]):
            for outW in range(tmp.shape[3]):
                if in_fm[0, outD, outH, outW] > 0:
                    tmp[0, outD, outH, outW] = in_fm[0, outD, outH, outW]
                else:
                    tmp[0, outD, outH, outW] = 0
    return tmp


def sgmoid(in_fm):
    tmp = np.zeros(in_fm.shape)
    for outF in range(tmp.shape[1]):
        tmp[0, outF] = 1.0 / (1.0 - np.exp(-in_fm[0, outF]))
    return tmp


def softmax(in_fm):
    tmp = np.zeros(in_fm.shape)
    exp_sum = 0
    for outF in range(tmp.shape[1]):
        tmp[0, outF] = np.exp(in_fm[0, outF])
        exp_sum += tmp[0, outF]
    for outF in range(tmp.shape[1]):
        tmp[0, outF] /= exp_sum
    return tmp


def main():
    x = (np.random.rand(1, 3, 4, 4) * 2) - 1
    y = relu(in_fm=x)
    print('x: ' + str(x))
    print('y: ' + str(y))

    f = np.random.rand(1, 6)
    s = softmax(f)
    print('f: ' + str(f))
    print('s: ' + str(s))


if __name__ == '__main__':
    main()
