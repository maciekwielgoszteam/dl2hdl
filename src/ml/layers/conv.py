import numpy as np
from dl2hdl.basic.fixed_def import FixedDef


def out_shape(input_shape, kernel_size, padding=0, stride=1):
    output_shape = list(input_shape).copy()
    output_shape[0] = input_shape[0]
    output_shape[1] = input_shape[1]
    for i in range(2, len(input_shape)):
        output_shape[i] = int((input_shape[i] - kernel_size[i - 2] + 2 * padding) / stride + 1)
    return output_shape


def conv_1d_raw(in_fm, coeffs=None, fxp=None):
    assert isinstance(fxp, FixedDef) or fxp is None, 'Provided fixed point definition is not valid'
    shape = (coeffs.layer.out_channels, in_fm.shape[1] - coeffs.layer.kernel_size[0] + 1)
    out_fm = np.zeros(shape)
    for outD in range(out_fm.shape[0]):
        for outH in range(out_fm.shape[1]):
            for inD in range(in_fm.shape[0]):
                for ker in range(coeffs.layer.kernel_size[0]):
                    if fxp is None:
                        out_fm[outD, outH] += in_fm[inD, outH + ker] * coeffs.weight[outD, inD, ker]
                    else:
                        tmp = fxp.emul_mul(in_fm[inD, outH + ker], coeffs.weight[outD, inD, ker])
                        out_fm[outD, outH] = fxp.emul_add(out_fm[outD, outH], tmp)
            if fxp is None:
                out_fm[outD, outH] += coeffs.bias[outD]
            else:
                out_fm[outD, outH] = fxp.emul_add(out_fm[outD, outH], fxp.quantize_float(coeffs.bias[outD]))
    return out_fm


def conv2d(in_fm, out_channels, kernel_size, weights, bias):
    shape = out_shape(in_fm.shape, kernel_size)
    shape[1] = out_channels
    tmp = np.zeros(shape)
    for outD in range(tmp.shape[1]):
        for outH in range(tmp.shape[2]):
            for outW in range(tmp.shape[3]):
                for inD in range(in_fm.shape[1]):
                    for kerH in range(kernel_size[0]):
                        for kerW in range(kernel_size[1]):
                            tmp[0, outD, outH, outW] += in_fm[0, inD, outH + kerH, outW + kerW] * weights[
                                outD, inD, kerH, kerW]
                tmp[0, outD, outH, outW] += bias[outD]
    return tmp


def main():
    in_fm_shape = (1, 2, 6, 6)
    out_channels = 4
    kernel_shape = (4, 4)

    x = np.random.rand(*in_fm_shape)
    w = np.random.rand(out_channels, in_fm_shape[1], *kernel_shape)
    b = np.random.rand(out_channels)
    y = conv2d(in_fm=x, out_channels=out_channels, kernel_size=kernel_shape, weights=w, bias=b)
    print('x: ' + str(x))
    print('w: ' + str(w))
    print('b: ' + str(b))
    print('y: ' + str(y))


if __name__ == '__main__':
    main()
