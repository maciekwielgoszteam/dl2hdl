import unittest

from dl2hdl.layers.tests.layer_test_util import diff_to_lsb

from myhdl import Signal
from myhdl import intbv, delay, block, instances, instance, always_comb, bin as mbin

from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.fixed_point._fixbv import fixbv

from config.config import config


class TestFixbv(unittest.TestCase):

    def testIncreaseAndDecreaseResolution(self):
        max_lsb_diff = 2
        fxp = FixedDef(8, 8)

        test_vectors = [fxp.quantize_float(i/10.0) for i in range(-20, 20, 2)]
        # test_vectors = [fxp.quantize_float(i) for i in (-2.4, 2.1, 0.0, 2.1)]
        expected = [fxp.emul_mul(t, 50.0) for t in test_vectors]

        @block
        def resolution_up_down(in_data, out_data, fxp=None, debug=False):
            assert fxp is not None, 'You have to provide fixed point definition'
            res_mul = 2
            in_format = (fxp.width, fxp.m - 1)
            calc_format = (fxp.width * res_mul, fxp.m * res_mul - 1)

            const_50_sig = Signal(fixbv(value=50.0, format=calc_format, round_mode='round'))

            in_sig_lp = Signal(fixbv(value=0, format=in_format, round_mode='round'))
            in_sig = Signal(fixbv(value=0, format=calc_format, round_mode='round'))
            out_sig = Signal(fixbv(value=0, format=calc_format, round_mode='round'))

            shift = int((in_sig._wl - in_sig_lp._wl) / 2)

            @always_comb
            def in_logic():
                if debug: print('in_data_signed %s' % (in_data.signed()))
                in_sig_lp.next = in_data.signed()

            @always_comb
            def upscale_logic():
                if debug:
                    print('in_sig_lp val=%s bin=%s' % (in_sig_lp.val, mbin(in_sig_lp.val, in_sig_lp._wl)))
                    print('in_sig wl=%s, shift=%s' % (in_sig._wl, shift))
                in_sig.next = in_sig_lp.val << shift



            @always_comb
            def downscale_logic():
                if debug:
                    print('in_sig val=%s bin=%s' % (in_sig.val, mbin(in_sig.val, in_sig._wl)))
                # if debug: print('in_sig x const %s' % (in_sig * const_hundr_sig))
                out_sig.next = in_sig * const_50_sig

            @always_comb
            def return_logic():
                if debug:
                    print('out_sig val=%s hex=%s bin=%s' % (out_sig.val, hex(out_sig.val), mbin(out_sig.val, out_sig._wl)))
                    print('out_sig shift = %s bin=%s' %
                          ((out_sig.val >> 8) & 0xFFFF, bin((out_sig.val >> 8) & 0xFFFF)))
                    # print('out_sig_lp val=%s bin=%s' % (out_sig_lp.val, mbin(out_sig_lp.val, out_sig_lp._wl)))
                out_data.next = intbv((out_sig.val >> shift))[fxp.width:]

            return instances()

        @block
        def tb():
            in_sig = Signal(intbv(0)[fxp.width:])
            out_sig = Signal(intbv(0)[fxp.width:])
            inputs = [Signal(intbv(fxp.to_fixed(tvec))[fxp.width:]) for tvec in test_vectors]
            dut = resolution_up_down(in_sig, out_sig, fxp=fxp, debug=True)
            dut.convert(hdl='VHDL', path=config.get_path_out_vhdl(), initial_values=True)

            @instance
            def stimulus():
                for i, e in zip(inputs, expected):
                    print('INPUT = %.8f' % i.val)
                    in_sig.next = i
                    yield delay(10)
                    ii = fxp.to_float(in_sig.signed())
                    oo = fxp.to_float(out_sig.signed())
                    print("%.8f -> %.8f exp %.8f (diff_lsb %d)" % (
                         ii, oo, e, diff_to_lsb(abs(e-oo), fxp=fxp)))
                    self.assertLess(diff_to_lsb(abs(e-oo), fxp=fxp), max_lsb_diff)

            return instances()

        tb_inst = tb()
        tb_inst.config_sim(backend='myhld', trace=False)
        tb_inst.run_sim(quiet=1)


if __name__ == '__main__':
    unittest.main(verbosity=2)
