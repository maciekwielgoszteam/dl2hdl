from myhdl import block, always_comb, ConcatSignal, Signal, instances, always_seq, intbv
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.fixed_point import fixbv
import math


@block
def assign_list(a, b, size, width):
    @always_comb
    def logic():
        a.next = ConcatSignal(*b)

    a.driven = True

    return logic


assign_list.vhdl_code = \
    """
    process($b) begin
        for i in 0 to ($size - 1) loop
            $a((($size - i) * $width)-1 downto ($size - i - 1) * $width) <= $b(i);
        end loop;
    end process;
    """


@block
def signed_to_unsigned(a, b):
    @always_comb
    def logic():
        a.next = b

    a.driven = True
    return logic


signed_to_unsigned.vhdl_code = \
    """
    $a <= unsigned($b);
    """


@block
def assign_list_of_bits(a, b, size):
    @always_comb
    def logic():
        a.next = ConcatSignal(*b)

    a.driven = True

    return logic


assign_list_of_bits.vhdl_code = \
    """
    process($b) begin
        for i in 0 to ($size - 1) loop
            $a(i) <= $b(i);
        end loop;
    end process;
    """


@block
def or_list_of_bits(a, b, size):
    c = Signal(bool())

    @always_comb
    def logic():
        c.next = 0
        for i in range(len(b)):
            if b[i] == 1:
                c.next = 1

    @always_comb
    def out_val():
        if c == 1:
            a.next = 1
        else:
            a.next = 0

    # a.driven = True

    return instances()


@block
def pow_3_5_7_9(in_data, out_data, fxp=None):
    """
    Generate a list of integer powers of in_data to 3, 5, 7, 9
    Assumes passed signals are of resolution that allows for multiplications with no precision loss
    :param in_data: Port, input signal
    :param out_data: Port, concatenated list of products = [x**3, x**5, x**7, x**9]
    :param n: Parameter, maximal product required
    :param fxp: Parameter, fixed point definition
    :return:
    """
    assert isinstance(fxp, FixedDef), 'You have to provide fixed point definition'

    x_3_w = Signal(intbv()[fxp.width * 3:])
    x_5_w = Signal(intbv()[fxp.width * 5:])
    x_7_w = Signal(intbv()[fxp.width * 7:])
    x_9_w = Signal(intbv()[fxp.width * 9:])
    x_3 = Signal(intbv()[fxp.width:])
    x_5 = Signal(intbv()[fxp.width:])
    x_7 = Signal(intbv()[fxp.width:])
    x_9 = Signal(intbv()[fxp.width:])

    @always_comb
    def pow_logic():
        x_3_w.next = in_data * in_data * in_data
        x_5_w.next = in_data * in_data * in_data * in_data * in_data
        x_7_w.next = in_data * in_data * in_data * in_data * in_data * in_data * in_data
        x_9_w.next = in_data * in_data * in_data * in_data * in_data * in_data * in_data * in_data * in_data

    @always_comb
    def prec_logic():
        x_3.next = x_3_w[fxp.f * 3 + fxp.m:fxp.f * 2]
        x_5.next = x_5_w[fxp.f * 5 + fxp.m:fxp.f * 4]
        x_7.next = x_7_w[fxp.f * 7 + fxp.m:fxp.f * 6]
        x_9.next = x_9_w[fxp.f * 9 + fxp.m:fxp.f * 8]

    x_c = ConcatSignal(x_3, x_5, x_7, x_9)

    @always_comb
    def return_logic():
        out_data.next = x_c

    return instances()


@block
def add_seq(clk, reset, a, b, c):
    """
    Sequential addition
    :param clk: clock signal
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param a: addend
    :param b: addend
    :param c: sum
    """

    @always_seq(clk.posedge, reset)
    def logic():
        c.next = a + b

    return logic


@block
def add_comb(a, b, c):
    """
    Combinational addition
    :param a: addend
    :param b: addend
    :param c: sum
    """

    @always_comb
    def logic():
        c.next = a + b

    return logic


def get_seq_levels(levels, step):
    if step == 0:
        return 0
    else:
        return int(math.ceil(levels / step))


def get_indices(in_size):
    calc_elems_len = 2 * in_size - 1
    levels = int(math.ceil(math.log2(calc_elems_len)))
    if levels == 0:
        indices = [[0]]
    else:
        indices = [[0] for _ in range(levels)]
        indices[0] = list(range(in_size))

        carry = 0
        for l in range(1, levels):
            prev_last = indices[l - 1][-1]
            if l >= 2:
                if (len(indices[l - 2]) + carry) % 2 != 0:
                    carry = 1
                else:
                    carry = 0
            indices[l] = list(range(prev_last + 1, prev_last + 1 + ((len(indices[l - 1]) + carry) // 2)))
    return indices


@block
def accumulate(clk, reset, in_data, out_data, seq_step=3, fxp=None):
    """
    Performs fully connected neural network layer computations for single output point
    :param clk: clock
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param in_data: concatenated data to sum
    :type in_data: Signal(fixbv)
    :param out_data: concatenated summed data
    :type out_data: Signal(fixbv)
    :param seq_step: after how many steps insert register
    :type seq_step: int
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :return: None
    :raises: Assertion errors
    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
    in_size = len(in_data) // fxp.width
    calc_elems_len = 2 * in_size - 1
    indices = get_indices(in_size)
    calc_elems = [Signal(intbv()[fxp.width:].signed()) for _ in range(calc_elems_len)]
    calc_elems_helper = [Signal(intbv()[fxp.width:].signed()) for _ in range(calc_elems_len)]

    add_i = []

    @always_comb
    def mul_logic():
        for i in range(calc_elems_len):
            if i < in_size:
                calc_elems[i].next = in_data[((in_size - 1) - i) * fxp.width + fxp.width:((in_size - 1) - i) * fxp.width].signed()
            else:
                calc_elems[i].next = calc_elems_helper[i]

    if seq_step == 1:
        @always_seq(clk.posedge, reset)
        def add_seq_logic():
            for i in range(in_size, calc_elems_len):
                calc_elems_helper[i].next = calc_elems[2 * i - 2 * in_size] + calc_elems[2 * i - 2 * in_size + 1]
    else:
        com_ind = []
        seq_ind = []
        for l in range(1, len(indices)):
            if seq_step == 0:
                com_ind += indices[l]
            else:
                if l % seq_step == 0:
                    seq_ind += indices[l]
                else:
                    com_ind += indices[l]

        for i in range(in_size, calc_elems_len):
            if i in seq_ind:
                add_i.append(add_seq(clk, reset, calc_elems[2 * i - 2 * in_size], calc_elems[2 * i - 2 * in_size + 1], calc_elems_helper[i]))

        for i in range(in_size, calc_elems_len):
            if i in com_ind:
                add_i.append(add_comb(calc_elems[2 * i - 2 * in_size], calc_elems[2 * i - 2 * in_size + 1], calc_elems_helper[i]))

    @always_comb
    def return_logic():
        out_data.next = calc_elems[calc_elems_len - 1]

    return instances()
