from myhdl import block, always, instances, Signal, always_comb


@block
def vec_and(input_vec, val):
    input_vec.read = True

    c = Signal(bool())

    @always_comb
    def logic():
        c.next = 1
        for i in range(len(input_vec)):
            if input_vec[i] == 0:
                c.next = 0

    @always_comb
    def out_val():
        if c == 1:
            val.next = 1
        else:
            val.next = 0

    # val.driven = True

    return instances()

# requires vhdl2008
# vec_and.vhdl_code = \
#     """
#     $val <= and $input_vec;
#     """


@block
def vec_nand(input_vec, val):
    input_vec.read = True
    c = Signal(bool())

    @always_comb
    def logic():
        c.next = 1
        for i in range(len(input_vec)):
            if input_vec[i] == 1:
                c.next = 0

    @always_comb
    def out_val():
        if c == 1:
            val.next = 1
        else:
            val.next = 0

    # val.driven = True

    return instances()

# requires vhdl2008
# vec_nand.vhdl_code = \
#     """
#     $val <= nand $input_vec;
#     """


@block
def vec_or(input_vec, val):
    input_vec.read = True
    c = Signal(bool())

    @always_comb
    def logic():
        c.next = 0
        for i in range(len(input_vec)):
            if input_vec[i] == 1:
                c.next = 1

    @always_comb
    def out_val():
        if c == 1:
            val.next = 1
        else:
            val.next = 0

    val.driven = True

    return instances()

# requires vhdl2008
# vec_or.vhdl_code = \
#     """
#     $val <= or
#      $input_vec;
#     """
