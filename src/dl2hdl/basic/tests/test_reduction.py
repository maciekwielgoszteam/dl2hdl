import os
from unittest import TestCase, skip

from myhdl import block, Signal, instances, intbv, instance, delay, StopSimulation, ConcatSignal

from config.config import config
from dl2hdl.basic.reduction import vec_and


class test_reduction(TestCase):

    def setUp(self):
        self.trace_save_path = config.get_path_out_testbench()
        self.vhdl_output_path = config.get_path_out_vhdl()
        os.makedirs(os.path.dirname(self.trace_save_path), exist_ok=True)
        os.makedirs(os.path.dirname(self.vhdl_output_path), exist_ok=True)

    def _and_single_run(self, test_data, conv=False):
        @block
        def and_tb(data, out_data, conv=False):
            # in_data = Signal(intbv()[len(data):])
            in_data_bits = [Signal(bool(0)) for _ in range(len(data))]
            out_val = Signal(bool(0))

            in_data = ConcatSignal(*in_data_bits)

            @instance
            def stim_write():
                yield delay(24)
                for i in range(len(data)):
                    in_data_bits[i].next = data[i]

            uut = vec_and(in_data, out_val)

            @instance
            def stim_read():
                yield delay(44)
                out_data.append(out_val)
                if conv:
                    uut.convert(hdl='VHDL', path=self.vhdl_output_path, initial_values=True)

                raise StopSimulation()

            return instances()

        out_data = []
        tb = and_tb(data=test_data, out_data=out_data, conv=conv)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return out_data

    # @skip('Not fully implemented')
    def test_and(self):
        seq_len = 6
        test_set = [1] * seq_len
        out_data = self._and_single_run(test_set, conv=False)
        self.fail()
