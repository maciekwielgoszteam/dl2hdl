from unittest import TestCase

from myhdl import block, Signal, instances, ResetSignal, always_seq, always_comb, intbv, ConcatSignal, instance, delay, StopSimulation

from config.config import config
from dl2hdl.axi.axis import Axis
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.basic.interface_map import receiver

import os
import numpy as np


class TestReceiver(TestCase):

    def single_run(self, test_data, concat_samples_num, trace_save_path, vhdl_output_path, conv=False, fxp=None):
        @block
        def recever_tb(data, out_data, concat_samples_num, vhdl_output_path=None, fxp=None):
            in_fm = Axis(intbv()[fxp.width:])
            out_fm = Axis(intbv()[fxp.width * concat_samples_num:])
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)

            @instance
            def reset_gen():
                reset.next = 0
                yield delay(24)
                yield clk.negedge
                reset.next = 1

            @instance
            def stim_write():
                yield reset.posedge
                yield clk.negedge

                for samples_nested in data:
                    in_fm.tvalid.next = 1
                    in_fm.tlast.next = 0
                    idx = 0
                    sample = np.asarray(samples_nested).flatten()

                    while True:
                        if in_fm.tready == 1:
                            idx += 1
                        in_fm.tdata.next = intbv(fxp.to_fixed(sample[idx]))[fxp.width:].unsigned()
                        if idx == len(sample) - 1:
                            in_fm.tlast.next = 1
                            break
                        yield clk.negedge
                    yield clk.negedge
                    in_fm.tvalid.next = 0
                    yield clk.negedge

            uut = receiver(clk, reset, in_fm, out_fm, fxp=fxp)

            @instance
            def stim_read():
                yield reset.posedge
                yield delay(24)
                yield clk.negedge
                out_fm.tready.next = 1
                for data_iter in range(len(data)):
                    while True:
                        yield clk.negedge
                        if out_fm.tvalid == 1:
                            out_data.append(out_fm.tdata.signed())
                            if out_fm.tlast == 1:
                                break

                for i in range(10):
                    yield clk.negedge

                # Print output for testbench
                # for sample in out_data:
                #     print(sample)

                if vhdl_output_path is not None:
                    uut.convert(hdl='VHDL', path=vhdl_output_path, initial_values=True)

                raise StopSimulation()

            return instances()

        out_data = []
        if not conv:
            vhdl_output_path = None
        tb = recever_tb(data=test_data, out_data=out_data, concat_samples_num=concat_samples_num, vhdl_output_path=vhdl_output_path, fxp=fxp)
        tb.config_sim(trace=True, directory=trace_save_path)
        tb.run_sim()
        return out_data

    def concat_array(self, data, cols, rows, fxp=None):
        assert isinstance(fxp, FixedDef)
        data = np.asarray(fxp.to_fixed(data)).reshape((rows, cols))
        concated = []
        for row in data:
            to_concat = []
            for c in row:
                to_concat.append(intbv(int(c))[fxp.width:])
            concated.append(ConcatSignal(*to_concat).val)
        return concated

    def test_receiver_sim(self):
        trace_save_path = config.get_path_out_testbench()
        vhdl_output_path = config.get_path_out_vhdl()
        os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
        os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)
        num_of_series = 3
        fxp = FixedDef(8, 8)
        for col in range(2, 16):
            with self.subTest(col=col):
                test_data = [np.random.rand(col * num_of_series)]
                out_data = self.single_run(test_data, col, trace_save_path, vhdl_output_path, conv=False, fxp=fxp)
                out_true = self.concat_array(test_data, cols=col, rows=num_of_series, fxp=fxp)
                self.assertListEqual(out_data, out_true)

    def test_receiver_conv(self):
        trace_save_path = config.get_path_out_testbench()
        vhdl_output_path = config.get_path_out_vhdl()
        os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
        os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)
        concat_samples_num = 4
        num_of_series = 3
        fxp = FixedDef(8, 8)
        test_data = [np.random.rand(concat_samples_num * num_of_series)]
        out_data = self.single_run(test_data, concat_samples_num, trace_save_path, vhdl_output_path, conv=True, fxp=fxp)
