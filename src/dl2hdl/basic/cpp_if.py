from dl2hdl.layers.tests.torch_dataset import RandomBin
from dl2hdl.basic.fixed_def import FixedDef
from config.config import config
import numpy as np
import os
import torch


def _conv_hex(val, bit_width=32):
    width = bit_width // 4
    mask_str = 'f' * width
    return f'{(val & int(mask_str, 16)):#0{width+2}x}'


def _conv_int(val, bit_width=32):
    return int(val)

def _conv_id(val, bit_width=32):
    return val


def numpy_ndaray_to_cpp_str(data, name, conv_foo=_conv_hex, out_bit_width=32):
    assert isinstance(data, np.ndarray)
    shape_str = ''.join([f'[{x}]' for x in data.shape])
    type_name = ''
    if data.dtype == np.float64:
        type_name = 'double'
    elif data.dtype == np.int:
        type_name = 'unsigned'
    data_str = f'{type_name} {name}{shape_str} = {{\n'
    data_str += _ND_conv(data, conv_foo, out_bit_width)
    data_str += '};\n'
    return data_str


def _1D_conv(array, conv_foo, out_bit_width=32):
    series_str = ''
    for sample in array:
        series_str += f'{conv_foo(sample, out_bit_width)}, '
    return series_str


def _ND_conv(array, conv_foo, out_bit_width=32):
    if array.dtype == np.float64:
        conv_foo = _conv_id
    data_str = ''
    if array.ndim == 1:
        data_str += '\t'
        data_str += _1D_conv(array, conv_foo, out_bit_width)
        data_str += '\n'
    else:
        for elem in array:
            data_str += '\t{\n\t'
            data_str += _ND_conv(elem, conv_foo, out_bit_width)
            data_str += '\t},\n'
    return data_str


def torch_data_set_to_cpp_string(data_set, name, fxp=None, out_bit_width=32):
    assert isinstance(fxp, FixedDef)
    data = fxp.to_fixed(data_set.data.numpy())
    labels = data_set.labels
    if isinstance(labels, torch.Tensor):
        labels = labels.numpy()
    data_str = numpy_ndaray_to_cpp_str(data, name + '_data', _conv_hex, out_bit_width)
    label_str = numpy_ndaray_to_cpp_str(labels.astype(int), name + '_labels', _conv_int, out_bit_width)
    file_str = data_str + label_str
    return file_str


if __name__ == '__main__':
    fxp = FixedDef(8, 8)
    test_set = RandomBin(shape=(8,), num_of_samples=3, seed=2)
    name = 'test_set'
    path = config.get_path_out_data()
    path = os.path.join(path, 'test_data.cpp')
    torch_data_set_to_cpp_string(test_set, name, path=path, fxp=fxp)
