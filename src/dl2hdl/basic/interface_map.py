from myhdl import block, Signal, instances, always_seq, always_comb, intbv, ConcatSignal


@block
def receiver(clk, reset, in_data, out_data, fxp=None):
    delay = 1
    num_of_samples = len(out_data.tdata) // fxp.width
    fm_data = [Signal(intbv(0)[fxp.width:]) for _ in range(num_of_samples)]
    counter = Signal(intbv(0, min=0, max=num_of_samples+delay))

    if num_of_samples == 1:

        @always_comb
        def map_ready():
            if counter <= num_of_samples - 1:
                in_data.tready.next = out_data.tready
            else:
                in_data.tready.next = 0

        @always_seq(clk.posedge, reset)
        def control_flow():
            if in_data.tvalid == 1 and out_data.tready == 1:
                fm_data[0].next = in_data.tdata[fxp.width:]
                if counter >= num_of_samples - 1 + delay:
                    counter.next = 0
                    out_data.tvalid.next = 1
                    if in_data.tlast == 1:
                        out_data.tlast.next = 1
                    else:
                        out_data.tlast.next = 0
                else:
                    counter.next = counter + 1
                    out_data.tvalid.next = 0
            else:
                if in_data.tvalid == 0:
                    out_data.tvalid.next = 0

        if num_of_samples > 1:
            out_data_c = ConcatSignal(*fm_data)
        else:
            out_data_c = fm_data[0]

        @always_comb
        def map_data():
            out_data.tdata.next = out_data_c
        # @always_comb
        # def map_logic():
        #     in_data.tready.next = out_data.tready
        #     out_data.tvalid.next = in_data.tvalid
        #     out_data.tlast.next = in_data.tlast
        #     out_data.tdata.next = in_data.tdata[fxp.width:]
    else:
        @always_comb
        def map_ready():
            in_data.tready.next = out_data.tready

        @always_seq(clk.posedge, reset)
        def control_flow():
            if in_data.tvalid == 1 and out_data.tready == 1:
                fm_data[counter].next = in_data.tdata[fxp.width:]
                if counter >= num_of_samples - 1:
                    counter.next = 0
                    out_data.tvalid.next = 1
                    if in_data.tlast == 1:
                        out_data.tlast.next = 1
                    else:
                        out_data.tlast.next = 0
                else:
                    counter.next = counter + 1
                    out_data.tvalid.next = 0
            else:
                if in_data.tvalid == 0:
                    out_data.tvalid.next = 0

        if num_of_samples > 1:
            out_data_c = ConcatSignal(*fm_data)
        else:
            out_data_c = fm_data[0]

        @always_comb
        def map_data():
            out_data.tdata.next = out_data_c

    return instances()


@block
def transmitter(clk, reset, in_data, out_data, fxp=None):
    num_of_samples = len(in_data.tdata) // fxp.width
    fm_data_tmp = [Signal(intbv(0)[fxp.width:]) for _ in range(num_of_samples)]
    fm_data = [Signal(intbv(0)[fxp.width:]) for _ in range(num_of_samples)]
    counter = Signal(intbv(0, min=0, max=num_of_samples+1))
    ready = Signal(bool(0))
    last = Signal(bool(0))
    out_wide = Signal(intbv()[32:].signed())

    if num_of_samples == 1:
        @always_comb
        def map_logic():
            in_data.tready.next = out_data.tready
            out_data.tvalid.next = in_data.tvalid
            out_data.tlast.next = in_data.tlast
            out_wide.next = in_data.tdata.signed()

        @always_comb
        def map_out():
            out_data.tdata.next = out_wide[32:]
    else:
        @always_comb
        def if_ready():
            if out_data.tready == 1 and counter == 0:
                ready.next = 1
            else:
                ready.next = 0

        @always_comb
        def map_ready():
            in_data.tready.next = ready

        @always_comb
        def map_in():
            for i in range(num_of_samples):
                fm_data_tmp[i].next = in_data.tdata[((num_of_samples - 1) - i) * fxp.width + fxp.width:((num_of_samples - 1) - i) * fxp.width]

        @always_seq(clk.posedge, reset)
        def control_flow():
            if in_data.tvalid == 1 and ready == 1:
                for i in range(num_of_samples):
                    fm_data[i].next = fm_data_tmp[i]
                counter.next = 1
                out_data.tvalid.next = 1
                out_wide.next = fm_data_tmp[0].signed()
                out_data.tlast.next = 0
                if in_data.tlast == 1:
                    last.next = 1
                else:
                    last.next = 0
            elif counter >= 1 and counter < num_of_samples:  # note: MyHDL does not convert chained comparison correctly :(
                out_data.tvalid.next = 1
                out_wide.next = fm_data[counter].signed()
                if out_data.tready == 1:
                    if counter >= num_of_samples - 1:
                        counter.next = 0
                        if last == 1:
                            out_data.tlast.next = 1
                        else:
                            out_data.tlast.next = 0
                    else:
                        counter.next = counter + 1
            else:
                out_data.tvalid.next = 0
                out_data.tlast.next = 0

        @always_comb
        def map_out():
            out_data.tdata.next = out_wide[32:]

    return instances()
