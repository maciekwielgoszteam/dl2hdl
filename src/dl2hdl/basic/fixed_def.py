from myhdl import Signal, intbv, always_comb, block, instances
from config.config import config
import numpy as np
import torch
import warnings


class FixedDef:
    """Super-low budget signed fixed-point implementation."""

    # TODO: add support for add/mul functions for different fixed point representations
    # TODO: Check convertibility with user defined vhdl_code

    def __init__(self, m=config.get_fixbv_iwl(), f=config.get_fixbv_fwl()):
        """
        :param m: Number of bits before the radix point.
        :param f: Number of bits after the radix point.
        """
        self.m = m
        self.f = f
        self.width = self.m + self.f
        self.max_val = (1 << (m + f - 1)) - 1
        self.min_val = -(1 << (m + f - 1))

        # print("representation %d.%d (%d, %d)" % (self.m, self.f, self.min_val, self.max_val))

    def __str__(self):
        return f'FixedPoint({self.m},{self.f})'

    def make_signal(self, init_value):
        return Signal(intbv(init_value, min=self.min_val, max=self.max_val))

    def emul_add(self, x, y):
        return self.to_float(self.to_fixed(x) + self.to_fixed(y))

    def emul_mul(self, x, y):
        return self.to_float((self.to_fixed(x) * self.to_fixed(y)) >> self.f)

    @block
    def fx_add(self, z, x, y):
        assert len(x) == self.width, f'Wrong fixed point definition for value {x}'
        assert len(y) == self.width, f'Wrong fixed point definition for value {y}'
        assert len(z) == self.width, f'Wrong fixed point definition for value {z}'
        add_wide = Signal(intbv()[self.m + self.f:].signed())

        @always_comb
        def add_logic():
            add_wide.next = x.signed() + y.signed()

        @always_comb
        def logic():
            z.next = add_wide[self.m + self.f:]

        return instances()

    @block
    def fx_mul(self, z, x, y):
        assert len(x) == self.width, f'Wrong fixed point definition for value {x}'
        assert len(y) == self.width, f'Wrong fixed point definition for value {y}'
        assert len(z) == self.width, f'Wrong fixed point definition for value {z}'
        mul_wide = Signal(intbv()[(self.m + self.f) * 2:].signed())

        @always_comb
        def u_logic():
            mul_wide.next = x.signed() * y.signed()

        @always_comb
        def logic():
            z.next = mul_wide[self.m + self.f + self.f:self.f]

        return instances()

    @block
    def fx_div(self, z, x, y):
        """
        z = x / y
        :param z: result
        :param x: dividend
        :param y: divider
        :return: None
        :raises Assertion errors
        """
        assert len(x) == self.width, f'Wrong fixed point definition for value {x}'
        assert len(y) == self.width, f'Wrong fixed point definition for value {y}'
        assert len(z) == self.width, f'Wrong fixed point definition for value {z}'

        fxp_wide = FixedDef(self.m + self.f, self.f)

        x_wide = Signal(intbv()[fxp_wide.width:].signed())
        z_wide = Signal(intbv()[fxp_wide.width:].signed())

        @always_comb
        def x_map():
            x_wide.next = x.signed()

        @always_comb
        def div_logic():
            if y == 0:
                if __debug__:
                    warnings.warn('Zero division')
            else:
                z_wide.next = (x_wide << self.f) // y

        @always_comb
        def z_map():
            z.next = z_wide[self.width:]

        return instances()

    @block
    def fixed_sub(self, z, x, y):
        """FixedOp: Return the difference of the fixed point inputs x and y via z."""

        @always_comb
        def logic():
            z.next = x - y

        return logic

    def _to_fixed_single(self, x):
        try:
            if x < self.to_float(intbv(1 << (self.width - 1))[self.width:].signed()):
                return int(1 << self.width - 1)
            elif x > self.to_float(intbv((1 << self.width - 1) - 1)[self.width:].signed()):
                return int((1 << self.width - 1) - 1)
            else:
                return int(round(x * (1 << self.f)))
        except ValueError as e:
            if str(e) != "cannot convert float NaN to integer":
                raise
            else:
                return 0

    def to_fixed(self, x):
        """Compute the nearest fixed-point representation of x."""
        if isinstance(x, np.ndarray):
            to_fixed_vec = np.vectorize(self._to_fixed_single)  # TODO: shouldn't it be: np.vectorize(self.to_fixed) ?
            return to_fixed_vec(x)
        elif isinstance(x, torch.Tensor):
            return self.to_fixed(x.numpy())
        elif isinstance(x, list):
            return self.to_fixed(np.asarray(x)).tolist()
        elif isinstance(x, intbv):
            return self.to_fixed(float(x))  # TODO: not checked
        elif isinstance(x, (float, np.floating)):
            return self._to_fixed_single(x)
        else:
            raise TypeError

    def _to_float_single(self, x):
        return int(x) / float(1 << self.f)

    def to_float(self, x):
        """Compute the nearest floating-point representation of fixed-point value x."""
        if isinstance(x, np.ndarray):
            to_float_vec = np.vectorize(self._to_float_single)
            return to_float_vec(x)
        elif isinstance(x, torch.Tensor):
            return self.to_float(x.numpy())
        elif isinstance(x, list):
            return self.to_float(np.asarray(x)).tolist()
        elif isinstance(x, intbv):
            return self.to_float(int(x))
        elif isinstance(x, (int, np.integer)):
            return self._to_float_single(x)
        else:
            raise TypeError

    def quantize_float(self, x):
        return self.to_float(self.to_fixed(x))

    def get_appendix(self):
        return '_fx_' + str(self.m) + '_' + str(self.f)
