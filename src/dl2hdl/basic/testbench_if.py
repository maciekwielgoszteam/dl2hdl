import numpy as np
import os


def _conv_hex(val, bit_width=32):
    width = bit_width // 4
    mask_str = 'f' * width
    return f'{(val & int(mask_str, 16)):#0{width+2}x}'


def numpy_array_to_txt_vec(array, path, name):
    assert array.dtype == np.int

    f_data = open(os.path.join(path, name + '_data.txt'), 'w')
    f_last = open(os.path.join(path, name + '_last.txt'), 'w')

    if array.ndim == 2:
        for series in array:
            for idx, tdata in enumerate(series):
                if idx == len(series) - 1:
                    tlast = 1
                else:
                    tlast = 0
                print(tlast, file=f_last)
    elif array.ndim == 3:
        for batch in array:
            for series in batch:
                for idx, tdata in enumerate(series):
                    if idx == len(series) - 1:
                        tlast = 1
                    else:
                        tlast = 0
                    print(tlast, file=f_last)
    else:
        raise ValueError(f'array ndim = {array.ndim}, only 2 and 3 are supported')

    array = array.flatten()
    for sample in array:
        print(_conv_hex(sample)[2:], file=f_data)

    f_data.close()
    f_last.close()
