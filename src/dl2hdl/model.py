import torch.nn as nn

from dl2hdl.axi.axis import Axis
from dl2hdl.axi.common import packet_block
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.basic.interface_map import receiver, transmitter
from dl2hdl.layers.activation.softmax import softmax
from dl2hdl.layers.linear import linear, LinearCoeffs
from dl2hdl.layers.lstm import LSTM, LSTMCoeffs
from ml.layers.lstm_hard import LSTMHardSigmoid
from myhdl import block, Signal, intbv, instances, ResetSignal


class MyHdlModel:
    supported_layers = (nn.Linear, nn.LSTM, LSTMHardSigmoid, nn.Softmax, nn.ReLU)
    sequenced_layers = (nn.LSTM, LSTMHardSigmoid)

    def __init__(self, torch_model, fxp=FixedDef(8, 8), seq_len=None):
        """
        Creates HDL model

        :param torch_model: model class derived from torch.nn.Module
        :param fxp: fixed point definition
        :param seq_len: length of the sequence, required only for recurrent networks
        """
        assert isinstance(torch_model, nn.Module)
        assert isinstance(fxp, FixedDef)
        self._fxp = fxp
        self._layers = []
        self._seq_len = seq_len
        for net_module in torch_model.modules():
            if len(list(net_module.modules())) == 1:
                if isinstance(net_module, self.supported_layers):
                    self._layers.append(net_module)
                else:
                    raise NotImplementedError(f'Layer type {type(net_module)} not supported')
        for layer in self._layers:
            if isinstance(layer, self.sequenced_layers):
                assert self._seq_len is not None, 'You have to provide sequence length while using recurrent layers'
        self.axis_width = 32

    @block
    def get_hdl_block(self, clk, reset, in_sig, out_sig):
        coeffs = []
        for layer in self._layers:
            if isinstance(layer, nn.Linear):
                coeffs.append(LinearCoeffs(layer, self._fxp))
            elif isinstance(layer, (nn.LSTM, LSTMHardSigmoid)):
                coeffs.append(LSTMCoeffs(layer, self._fxp))

        connection = []
        if isinstance(self._layers[0], nn.Linear):
            connection.append(Axis(intbv()[self._fxp.width * coeffs[0].layer.in_features:]))
        elif isinstance(self._layers[0], (nn.LSTM, LSTMHardSigmoid)):
            connection.append(Axis(intbv()[self._fxp.width * coeffs[0].layer.input_size:]))

        trainable_idx = 0
        for i in range(len(self._layers)):
            if isinstance(self._layers[i], nn.Linear):
                connection.append(Axis(intbv()[self._fxp.width * coeffs[trainable_idx].layer.out_features:]))
                trainable_idx += 1
            elif isinstance(self._layers[i], (nn.LSTM, LSTMHardSigmoid)):
                connection.append(Axis(intbv()[self._fxp.width * coeffs[trainable_idx].layer.hidden_size:]))
                trainable_idx += 1
            elif isinstance(self._layers[i], nn.Softmax):
                connection.append(Axis(intbv()[len(connection[-1].tdata):]))

        layers = [receiver(clk, reset,
                           in_data=in_sig,
                           out_data=connection[0],
                           fxp=self._fxp)]
        trainable_idx = 0

        l_i = 0
        c_i = 0
        while l_i < len(self._layers):
            if isinstance(self._layers[l_i], nn.Linear):
                connection_in = connection[c_i]
                if l_i > 0:
                    if isinstance(self._layers[l_i - 1], (nn.LSTM, LSTMHardSigmoid)):
                        connection_in = Axis(intbv()[self._fxp.width * coeffs[trainable_idx].layer.in_features:])
                        layers.append(packet_block(clk, reset,
                                                   bus_in=connection[c_i],
                                                   bus_out=connection_in,
                                                   pass_cntr=self._seq_len))
                activation = None
                try:
                    if isinstance(self._layers[l_i + 1], nn.ReLU):
                        activation = 'relu'
                except IndexError:
                    pass
                layers.append(linear(clk, reset,
                                     in_fm=connection_in,
                                     out_fm=connection[c_i + 1],
                                     coeffs=coeffs[trainable_idx],
                                     fxp=self._fxp,
                                     activation=activation))
                trainable_idx += 1
                c_i += 1
            elif isinstance(self._layers[l_i], nn.LSTM):
                layers.append(LSTM(clk, reset,
                                   in_data=connection[c_i],
                                   out_data=connection[c_i + 1],
                                   coeffs=coeffs[trainable_idx],
                                   sequence_length=self._seq_len,
                                   sigmoid_type='soft',
                                   fxp=self._fxp))
                trainable_idx += 1
                c_i += 1
            elif isinstance(self._layers[l_i], LSTMHardSigmoid):
                layers.append(LSTM(clk, reset,
                                   in_data=connection[c_i],
                                   out_data=connection[c_i + 1],
                                   coeffs=coeffs[trainable_idx],
                                   sequence_length=self._seq_len,
                                   sigmoid_type='hard',
                                   fxp=self._fxp))
                trainable_idx += 1
                c_i += 1
            elif isinstance(self._layers[l_i], nn.Softmax):
                layers.append(softmax(clk, reset,
                                      in_fm=connection[c_i],
                                      out_fm=connection[c_i + 1],
                                      fxp=self._fxp))
                c_i += 1
            elif isinstance(self._layers[l_i], nn.ReLU):
                try:
                    if not isinstance(self._layers[l_i - 1], nn.Linear):
                        raise NotImplementedError('ReLU is supported only as layer following linear layer')
                except IndexError:
                    raise NotImplementedError('ReLU is supported only as layer following linear layer')
            else:
                raise NotImplementedError(f'layer {self._layers[l_i]} is not implemented')
            l_i += 1
        layers.append(transmitter(clk, reset,
                                  in_data=connection[len(connection) - 1],
                                  out_data=out_sig,
                                  fxp=self._fxp))

        return instances()

    def convert(self, path, name):
        in_fm = Axis(intbv()[self.axis_width:])
        out_fm = Axis(intbv()[self.axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)
        uut = self.get_hdl_block(clk, reset, in_fm, out_fm)
        uut.convert(hdl='VHDL', path=path, name=name)
