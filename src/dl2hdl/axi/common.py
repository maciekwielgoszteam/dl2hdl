from myhdl import intbv, block, Signal, always_comb, instances, ConcatSignal, always_seq

from dl2hdl.fixed_point import fixbv


@block
def elementwise_sum_comb(in_a, in_b, out_fm, fxp=None):
    """
    Elementwise addition of two concatenated lists of signals of given size

    :param in_a: Port, concatenated list of signals, input A
    :param in_b: Port, concatenated list of signals, input B
    :param out_fm: Port, concatenated list of signals, output A + B
    :param fxp: Parameter, fixed point definition
    :return: instances
    """
    assert fxp is not None, 'You have to provide fixed point definition'
    assert len(in_a.tdata) == len(in_b.tdata), 'Inputs must be of same length'

    size = len(out_fm.tdata) // fxp.width

    a_sigs = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    b_sigs = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    out_sig = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    out_sig_u = [Signal(intbv()[fxp.width:]) for _ in range(size)]

    @always_comb
    def map_to_signals():
        for ii in range(size):
            a_sigs[ii].next = in_a.tdata[((size - 1) - ii) * fxp.width + fxp.width:((size - 1) - ii) * fxp.width].signed()
            b_sigs[ii].next = in_b.tdata[((size - 1) - ii) * fxp.width + fxp.width:((size - 1) - ii) * fxp.width].signed()

    @always_comb
    def add_logic():
        for mi in range(size):
            out_sig[mi].next = a_sigs[mi] + b_sigs[mi]

    @always_comb
    def to_unsigned_logic():
        for mi in range(size):
            out_sig_u[mi].next = out_sig[mi].signed()[len(out_sig[mi]):0]

    out_sig_concat = ConcatSignal(*out_sig_u)

    @always_comb
    def return_logic():
        out_fm.tdata.next = out_sig_concat

        if in_a.tvalid == 1 and in_b.tvalid == 1:
            out_fm.tvalid.next = 1
        else:
            out_fm.tvalid.next = 0

        if out_fm.tready == 1 and (in_b.tvalid == 1 or (in_a.tvalid == 0 and in_b.tvalid == 0)):
            in_a.tready.next = 1
        else:
            in_a.tready.next = 0
        if out_fm.tready == 1 and (in_a.tvalid == 1 or (in_a.tvalid == 0 and in_b.tvalid == 0)):
            in_b.tready.next = 1
        else:
            in_b.tready.next = 0

        if in_a.tlast == 1 and in_b.tlast == 1:
            out_fm.tlast.next = 1
        else:
            out_fm.tlast.next = 0

    return instances()


@block
def elementwise_mul_comb(in_a, in_b, out_fm, fxp=None):
    """
    Elementwise multiplication of two concatenated lists of signals of given size

    :param in_a: Port, concatenated list of signals, input A
    :param in_b: Port, concatenated list of signals, input B
    :param out_fm: Port, concatenated list of signals, output A * B
    :param fxp: Parameter, fixed point definition
    :return: instances
    """
    assert fxp is not None, 'You have to provide fixed point definition'
    assert len(in_a.tdata) == len(in_b.tdata), 'Inputs must be of same length'

    size = len(out_fm.tdata) // fxp.width

    a_sigs = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    b_sigs = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    out_sig = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    out_sig_u = [Signal(intbv()[fxp.width:]) for _ in range(size)]

    @always_comb
    def map_to_signals():
        for ii in range(size):
            a_sigs[ii].next = in_a.tdata[((size - 1) - ii) * fxp.width + fxp.width:((size - 1) - ii) * fxp.width].signed()
            b_sigs[ii].next = in_b.tdata[((size - 1) - ii) * fxp.width + fxp.width:((size - 1) - ii) * fxp.width].signed()

    @always_comb
    def mul_logic():
        for mi in range(size):
            out_sig[mi].next = a_sigs[mi] * b_sigs[mi] >> fxp.f
            if __debug__:
                out_sig[mi].next = a_sigs[mi] * b_sigs[mi]

    @always_comb
    def to_unsigned_logic():
        for mi in range(size):
            out_sig_u[mi].next = out_sig[mi].signed()[len(out_sig[mi]):0]

    out_sig_concat = ConcatSignal(*out_sig_u)

    @always_comb
    def return_logic():
        out_fm.tdata.next = out_sig_concat

        if in_a.tvalid == 1 and in_b.tvalid == 1:
            out_fm.tvalid.next = 1
        else:
            out_fm.tvalid.next = 0

        if out_fm.tready == 1 and (in_b.tvalid == 1 or (in_a.tvalid == 0 and in_b.tvalid == 0)):
            in_a.tready.next = 1
        else:
            in_a.tready.next = 0
        if out_fm.tready == 1 and (in_a.tvalid == 1 or (in_a.tvalid == 0 and in_b.tvalid == 0)):
            in_b.tready.next = 1
        else:
            in_b.tready.next = 0

        if in_a.tlast == 1 and in_b.tlast == 1:
            out_fm.tlast.next = 1
        else:
            out_fm.tlast.next = 0

    return instances()


@block
def broadcast_2_comb(bus_a, bus_b, bus_in):
    @always_comb
    def bus_map():
        bus_a.tdata.next = bus_in.tdata
        bus_b.tdata.next = bus_in.tdata

        if bus_a.tready == 1 and bus_b.tready == 1:
            bus_in.tready.next = 1
        else:
            bus_in.tready.next = 0

        if bus_in.tvalid == 1 and bus_b.tready == 1:
            bus_a.tvalid.next = 1
        else:
            bus_a.tvalid.next = 0
        if bus_in.tvalid == 1 and bus_a.tready == 1:
            bus_b.tvalid.next = 1
        else:
            bus_b.tvalid.next = 0

        bus_a.tlast.next = bus_in.tlast
        bus_b.tlast.next = bus_in.tlast

    return bus_map


@block
def split_4_comb(bus_a, bus_b, bus_c, bus_d, bus_in):
    width = len(bus_in.tdata) // 4

    @always_comb
    def bus_map():
        bus_a.tdata.next = bus_in.tdata[width * 4:width * 3]
        bus_b.tdata.next = bus_in.tdata[width * 3:width * 2]
        bus_c.tdata.next = bus_in.tdata[width * 2:width * 1]
        bus_d.tdata.next = bus_in.tdata[width * 1:width * 0]

        if bus_a.tready == 1 and bus_b.tready == 1 and bus_c.tready == 1 and bus_d.tready == 1:
            bus_in.tready.next = 1
        else:
            bus_in.tready.next = 0

        if bus_in.tvalid == 1 and bus_b.tready == 1 and bus_c.tready == 1 and bus_d.tready == 1:
            bus_a.tvalid.next = 1
        else:
            bus_a.tvalid.next = 0

        if bus_in.tvalid == 1 and bus_a.tready == 1 and bus_c.tready == 1 and bus_d.tready == 1:
            bus_b.tvalid.next = 1
        else:
            bus_b.tvalid.next = 0

        if bus_in.tvalid == 1 and bus_a.tready == 1 and bus_b.tready == 1 and bus_d.tready == 1:
            bus_c.tvalid.next = 1
        else:
            bus_c.tvalid.next = 0

        if bus_in.tvalid == 1 and bus_a.tready == 1 and bus_b.tready == 1 and bus_c.tready == 1:
            bus_d.tvalid.next = 1
        else:
            bus_d.tvalid.next = 0

        bus_a.tlast.next = bus_in.tlast
        bus_b.tlast.next = bus_in.tlast
        bus_c.tlast.next = bus_in.tlast
        bus_d.tlast.next = bus_in.tlast

    return bus_map


class Reg:
    def __init__(self, data_type=intbv(0)[32:]):
        self.data = Signal(data_type)
        self.saved = Signal(bool(0))


@block
def axis_reg(clk, reset, bus_s, bus_m, clear_reg, set_reg, value, last):
    """
    Registered buffer for AXI Stream interface, results in one clk cycle delay

    :param clk: clock
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param bus_s: slave bus
    :type bus_s: Axis
    :param bus_m: master bus
    :type bus_m: Axis
    :param clear_reg: clears saved flag, higher priority than set_reg
    :type clear_reg: Signal(bool)
    :param set_reg: sets (overwrites if necessary) register to specified value
    :type set_reg: Signal(bool)
    :param value: value to set
    :type value: Signal(intbv)
    :param last: tlast signal associated with value
    :type last: Signal(bool)
    :return: MyHDL instances
    """
    reg = Reg(intbv()[len(bus_m.tdata):])
    last_reg = Signal(bool(0))

    @always_seq(clk.posedge, reset)
    def seq_logic():
        if clear_reg == 1:
            reg.saved.next = 0
        else:
            if set_reg == 1:
                reg.saved.next = 1
                reg.data.next = value
                last_reg.next = last
            else:
                if reg.saved == 1:
                    if bus_m.tready == 1:
                        reg.saved.next = 0
                else:
                    if bus_s.tvalid == 1:
                        reg.saved.next = 1
                        reg.data.next = bus_s.tdata
                        last_reg.next = bus_s.tlast

    @always_comb
    def comb_logic():
        if set_reg == 1:
            bus_s.tready.next = 0
            bus_m.tvalid.next = 0
        else:
            bus_m.tvalid.next = reg.saved
            bus_m.tdata.next = reg.data
            bus_m.tlast.next = last_reg
            if reg.saved == 1:
                bus_s.tready.next = 0
            else:
                bus_s.tready.next = 1

    return instances()


@block
def packet_block(clk, reset, bus_in, bus_out, pass_cntr=1):
    """
    Discards AXI stream packets, letting pass every n-th depending on parameter.
    Example for pass_cntr = 3:
     pkt_1   pkt_2   pkt_3   pkt_4   pkt_5   pkt_6   pkt_7
    blocked blocked passed  blocked blocked passed  blocked

    :param clk: clock
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param bus_in: bus with packets to block
    :type bus_in: Axis
    :param bus_out: bus with only passed packets
    :type bus_out: Axis
    :param pass_cntr: counter for packets to pass
    :type pass_cntr: int
    :return: None
    :raises: Assertion errors
    """
    assert pass_cntr >= 1 and isinstance(pass_cntr, int), 'pass_counter has to be an integer grater or equal to 1'
    bus_in_tready = Signal(bool(0))
    cntr = Signal(intbv(val=1, min=0, max=pass_cntr+1))

    @always_comb
    def bus_ctrl():
        bus_out.tdata.next = bus_in.tdata
        bus_out.tlast.next = bus_in.tlast
        if cntr == pass_cntr:
            bus_out.tvalid.next = bus_in.tvalid
            bus_in_tready.next = bus_out.tready
        else:
            bus_out.tvalid.next = 0
            bus_in_tready.next = 1

    @always_comb
    def ready_map():
        bus_in.tready.next = bus_in_tready

    @always_seq(clk.posedge, reset)
    def cntr_ctrl():
        if reset == reset.active:
            cntr.next = 1
        else:
            if bus_in_tready == 1 and bus_in.tvalid == 1 and bus_in.tlast == 1:
                if cntr < pass_cntr:
                    cntr.next = cntr + 1
                else:
                    cntr.next = 1

    return instances()
