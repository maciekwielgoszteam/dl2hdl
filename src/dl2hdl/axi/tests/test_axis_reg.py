from dl2hdl.tests.hdl_test import HdlTest
from myhdl import Signal, ResetSignal, block, instance, delay, StopSimulation, instances, intbv
from dl2hdl.axi.axis import Axis
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from dl2hdl.axi.common import axis_reg


class TestAxis_reg(HdlTest):

    def _single_run(self, conv=False):
        @block
        def axis_reg_tb(conv=False):
            axis_width = 32
            in_data_sig = Axis(intbv()[axis_width:])
            out_data_sig = Axis(intbv()[axis_width:])
            clear_reg = Signal(bool(0))
            set_reg = Signal(bool(0))
            value = Signal(intbv()[axis_width:])
            last_reg = Signal(bool(0))
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)
            rst_gen = aresetn_stim(reset, delay_time=24)

            uut = axis_reg(clk, reset, in_data_sig, out_data_sig, clear_reg, set_reg, value, last_reg)

            @instance
            def sim_write():
                yield reset.posedge
                yield clk.negedge

                in_data_sig.tvalid.next = 1
                in_data_sig.tlast.next = 0
                in_data_sig.tdata.next = 10
                sent = False
                while not sent:
                    yield clk.posedge
                    if in_data_sig.tready == 1:
                        yield clk.negedge
                        sent = True
                in_data_sig.tvalid.next = 0

                yield delay(37)
                yield clk.negedge
                in_data_sig.tvalid.next = 1
                in_data_sig.tlast.next = 0
                in_data_sig.tdata.next = 11
                sent = False
                while not sent:
                    yield clk.posedge
                    if in_data_sig.tready == 1:
                        yield clk.negedge
                        sent = True
                in_data_sig.tdata.next = 12
                sent = False
                while not sent:
                    yield clk.posedge
                    if in_data_sig.tready == 1:
                        yield clk.negedge
                        sent = True
                in_data_sig.tvalid.next = 0

                yield delay(48)
                yield clk.negedge
                in_data_sig.tvalid.next = 1
                in_data_sig.tlast.next = 0
                in_data_sig.tdata.next = 13
                sent = False
                while not sent:
                    yield clk.posedge
                    if in_data_sig.tready == 1:
                        yield clk.negedge
                        sent = True
                in_data_sig.tdata.next = 14
                sent = False
                while not sent:
                    yield clk.posedge
                    if in_data_sig.tready == 1:
                        yield clk.negedge
                        sent = True
                in_data_sig.tdata.next = 15
                sent = False
                while not sent:
                    yield clk.posedge
                    if in_data_sig.tready == 1:
                        yield clk.negedge
                        sent = True
                in_data_sig.tvalid.next = 0

                for _ in range(10):
                    yield clk.negedge

                if conv:
                    uut.convert(hdl='VHDL', path=self.vhdl_output_path)

                raise StopSimulation()

            @instance
            def stim_read():
                yield reset.posedge
                yield delay(24)
                yield clk.negedge
                out_data_sig.tready.next = 1
                while True:
                    yield clk.posedge
                    if out_data_sig.tvalid == 1:
                        print(int(str(out_data_sig.tdata), 16))

            return instances()

        tb = axis_reg_tb(conv=conv)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return True

    def test_axis_reg(self):
        success = self._single_run(conv=True)
        self.assertTrue(success)
