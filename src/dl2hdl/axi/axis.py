from myhdl import Signal, intbv, block, instance
from dl2hdl.basic.fixed_def import FixedDef
import numpy as np
from myhdl._Signal import _Signal
import time


class Axis:
    def __init__(self, data_type=intbv(0)[32:]):
        self.tdata = Signal(data_type)
        self.tready = Signal(bool(0))
        self.tvalid = Signal(bool(0))
        self.tlast = Signal(bool(0))

    @staticmethod
    @block
    def write(clk, reset, bus, data, fxp=None, verbose=False):
        """Writes data to bus using AXI Stream handshaking. For simulation only.

        :param clk: Bus clock
        :type clk: myhdl.Signal
        :param reset: Reset signal
        :type reset: myhdl.ResetSignal
        :param bus: AXI Stream bus to which tha data will be written, 32bit wide
        :type bus: Axis
        :param data: data array to write. Data should be organized as follows:
        +-----+-----+-----+------+-----+
        | A0  | A1  | ... | An-1 | An  |
        +-----+-----+-----+------+-----+
        | B0  | B1  | ... | Bn-1 | Bn  |
        +-----+-----+-----+------+-----+
        | ... | ... | ... | ...  | ... |
        +-----+-----+-----+------+-----+
        | Y0  | Y1  | ... | Yn-1 | Yn  |
        +-----+-----+-----+------+-----+
        | Z0  | Z1  | ... | Zn-1 | Zn  |
        +-----+-----+-----+------+-----+
        With the shape = (columns, rows)
        Each row will be send as separate packet - last data with tlast signal set to 1
        :type data: nested list, numpy.ndarray, list of myhdl.Signal
        :param fxp: Fixed point definition
        :type fxp: FixedDef
        :param verbose: whether to print additional information during run
        :type verbose: bool

        """

        @instance
        def sim():
            assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
            yield reset.posedge
            yield clk.negedge

            bus.tvalid.next = 1
            bus.tlast.next = 0
            data_f = data.reshape(-1, data.shape[-1])
            for sample_idx, samples_nested in enumerate(data_f, 1):
                if verbose:
                    print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Sending sample {sample_idx}/{len(data_f)}')
                idx = 0
                if isinstance(samples_nested, _Signal):  # FIXME: do not use protected member
                    sample = [samples_nested]
                elif isinstance(samples_nested, np.ndarray):
                    sample = np.asarray(samples_nested).flatten()
                    sample = [intbv(fxp.to_fixed(x))[fxp.width:].unsigned() for x in sample]
                else:
                    raise TypeError

                bus.tdata.next = sample[idx]
                sent = False
                while not sent:
                    yield clk.negedge
                    if bus.tready == 1:
                        if idx == len(sample) - 1:
                            sent = True
                        else:
                            idx += 1
                    if idx == len(sample) - 1:
                        bus.tlast.next = 1
                    if sent:
                        if len(sample) == 1:
                            bus.tlast.next = 1
                        else:
                            bus.tlast.next = 0
                    bus.tdata.next = sample[idx]
            bus.tvalid.next = 0

        return sim
