import torch
import numpy as np
from myhdl import block, Signal, instances, always_seq, always_comb, intbv, ConcatSignal
from dl2hdl.basic.fixed_def import FixedDef
import dl2hdl.basic.reduction as reduction
from dl2hdl.axi.axis import Axis
from dl2hdl.fixed_point import fixbv
from dl2hdl.basic.utils import accumulate, get_seq_levels, get_indices, signed_to_unsigned
import dl2hdl.basic.utils


class LinearCoeffs:
    """
    Wrapper class for linear layer coefficients and biases
    Holds both floating and fixed point representation
    """

    def __init__(self, from_torch=None, fxp=None):
        """
        Linear cell weights and biases wrapper ctor

        :param from_torch: torch.nn.Linear object of Linear layer
        :param fxp: fixed point representation
        """
        if fxp is not None:
            self._fxp = fxp

        if from_torch is not None:
            assert isinstance(from_torch, torch.nn.Linear)
            self._layer = from_torch

    @property
    def fxp(self):
        return self._fxp

    @fxp.setter
    def fxp(self, value):
        assert isinstance(value, FixedDef)
        self._fxp = value

    @property
    def layer(self):
        return self._layer

    @layer.setter
    def layer(self, value):
        assert isinstance(value, torch.nn.Linear)
        self._layer = value

    @property
    def weight(self):
        return self._layer.weight.data.cpu().numpy()

    @weight.setter
    def weight(self, value):
        if isinstance(value, list):
            value = torch.from_numpy(np.asarray(value))
        elif isinstance(value, np.ndarray):
            value = torch.from_numpy(value)
        self._layer.weight.data = value

    @property
    def bias(self):
        return self._layer.bias.data.cpu().numpy()

    @bias.setter
    def bias(self, value):
        if isinstance(value, list):
            value = torch.from_numpy(np.asarray(value))
        elif isinstance(value, np.ndarray):
            value = torch.from_numpy(value)
        self._layer.bias.data = value

    @property
    def weight_fxp(self):
        assert isinstance(self._fxp, FixedDef)
        return self._fxp.to_fixed(self.weight)

    @property
    def bias_fxp(self):
        assert isinstance(self._fxp, FixedDef)
        return self._fxp.to_fixed(self.bias)


@block
def add_seq(clk, reset, a, b, c):
    """
    Sequential addition

    :param clk: clock signal
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param a: addend
    :param b: addend
    :param c: sum
    """

    @always_seq(clk.posedge, reset)
    def logic():
        c.next = a + b

    return logic


@block
def add_comb(a, b, c):
    """
    Combinational addition

    :param a: addend
    :param b: addend
    :param c: sum
    """

    @always_comb
    def logic():
        c.next = a + b

    return logic


@block
def linear_single(clk, reset, in_fm, out_data, weights,  bias, fxp=None, activation=None):
    """
    Performs fully connected neural network layer computations for single output point

    :param clk: clock
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param in_fm: concatenated data to sum
    :type in_fm: AXI Stream
    :param out_data: concatenated summed data
    :type out_data: AXI Stream
    :param weights: weights for neuron
    :type weights: numpy ndarray (1D of (float, numpy float))
    :param bias: bias for neuron
    :type bias: (float, numpy floating)
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :param activation: activation function to use
    :type activation: string
    :return: None
    :raises: Assertion errors
    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
    assert isinstance(in_fm, Axis)
    assert isinstance(out_data, Axis)

    in_size = len(weights)
    seq_level_step = 3  # How often to insert register between level reduction, 0 - never
    if in_size > 16:
        seq_level_step = 2  # How often to insert register between level reduction, 0 - never
    indices = get_indices(in_size)
    seq_levels = get_seq_levels(len(indices), seq_level_step)
    assert seq_levels > 0, 'Implementation fails to work with 0 sequential levels, bug need to be fixed for that'
    bias_sig = Signal(fixbv(value=bias, format=(fxp.width, fxp.m - 1)), constant=True)
    weights_sig = [Signal(fixbv(value=w, format=(fxp.width, fxp.m - 1)), constant=True) for w in weights]

    calc_elems_len = 2 * in_size - 1
    fm_sigs_fs = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(in_size)]
    fm_sigs = [Signal(intbv()[fxp.width:].signed()) for _ in range(in_size)]
    calc_elems = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(calc_elems_len)]
    calc_elems_helper = [Signal(intbv()[fxp.width:].signed()) for _ in range(calc_elems_len)]
    out_data_sig = Signal(intbv()[fxp.width:].signed())
    if seq_level_step == 0:
        valid_buff = [Signal(bool(0)) for _ in range(seq_levels + 1)]
        last_buff = [Signal(bool(0)) for _ in range(seq_levels + 1)]
    else:
        valid_buff = [Signal(bool(0)) for _ in range(seq_levels)]
        last_buff = [Signal(bool(0)) for _ in range(seq_levels)]

    @always_seq(clk.posedge, reset)
    def map_to_signals():
        if out_data.tready == 1:
            for i in range(in_size):
                if in_fm.tvalid == 1:
                    fm_sigs[i].next = in_fm.tdata[((in_size - 1) - i) * fxp.width + fxp.width:((in_size - 1) - i) * fxp.width].signed()
                    valid_buff[0].next = 1
                    last_buff[0].next = in_fm.tlast
                else:
                    valid_buff[0].next = 0
                    last_buff[0].next = 0
            for i in range(1, seq_levels):
                valid_buff[i].next = valid_buff[i - 1]
                last_buff[i].next = last_buff[i - 1]

    @always_comb
    def map_to_intbv_signals():
        for i in range(in_size):
            fm_sigs_fs[i].next = fm_sigs[i]

    @always_comb
    def mul_logic():
        for i in range(calc_elems_len):
            if i < in_size:
                calc_elems[i].next = (fm_sigs_fs[i] * weights_sig[i]) >> fxp.f
                if __debug__:
                    calc_elems[i].next = (fm_sigs_fs[i] * weights_sig[i])  # >> fxp.f
            else:
                calc_elems[i].next = calc_elems_helper[i]

    add_i = []

    if seq_level_step == 1:
        @always_seq(clk.posedge, reset)
        def add_seq_logic():
            for i in range(in_size, calc_elems_len):
                calc_elems_helper[i].next = calc_elems[2 * i - 2 * in_size] + calc_elems[2 * i - 2 * in_size + 1]
    else:
        com_ind = []
        seq_ind = []
        for l in range(1, len(indices)):
            if seq_level_step == 0:
                com_ind += indices[l]
            else:
                if l % seq_level_step == 0:
                    seq_ind += indices[l]
                else:
                    com_ind += indices[l]

        for i in range(in_size, calc_elems_len):
            if i in seq_ind:
                add_i.append(add_seq(clk, reset, calc_elems[2 * i - 2 * in_size], calc_elems[2 * i - 2 * in_size + 1], calc_elems_helper[i]))

        for i in range(in_size, calc_elems_len):
            if i in com_ind:
                add_i.append(add_comb(calc_elems[2 * i - 2 * in_size], calc_elems[2 * i - 2 * in_size + 1], calc_elems_helper[i]))

    @always_comb
    def ctrl_axis():
        out_data_sig.next = calc_elems[calc_elems_len - 1] + bias_sig
        in_fm.tready.next = out_data.tready
        out_data.tvalid.next = valid_buff[seq_levels - 1]
        out_data.tlast.next = last_buff[seq_levels - 1]

    if activation is None:
        @always_comb
        def return_logic():
            out_data.tdata.next = out_data_sig[fxp.width:]
    elif activation == 'relu':
        @always_comb
        def return_logic():
            if out_data_sig > 0:
                out_data.tdata.next = out_data_sig[fxp.width:]
            else:
                out_data.tdata.next = 0
    else:
        raise NotImplementedError

    return instances()


@block
def dummy_assign(a, b):
    @always_comb
    def logic():
        a.next = b

    return logic


@block
def linear(clk, reset, in_fm, out_fm, coeffs=None, fxp=None, activation=None):
    """
    Performs fully connected neural network layer computations

    :param clk: clock
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param in_fm: concatenated input data
    :type in_fm: AXI Stream
    :param out_fm: concatenated output data
    :type out_fm: AXI Stream
    :param coeffs: object with weights, biases, sizes and fixed point
    :type coeffs: LinearCoeffs
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :param activation: activation function to use
    :type activation: string
    :return: None
    :raises: Assertion errors
    """
    assert isinstance(in_fm, Axis)
    assert isinstance(out_fm, Axis)
    assert isinstance(coeffs, LinearCoeffs), 'You have to provide valid coefficients definition'
    assert coeffs.layer.in_features > 0
    assert coeffs.layer.out_features > 0

    in_fm_sigs = [Axis(intbv()[len(in_fm.tdata):]) for _ in range(coeffs.layer.out_features)]
    in_fm_ready = Signal(bool(0))
    out_fm_sigs = [Axis(intbv()[fxp.width:]) for _ in range(coeffs.layer.out_features)]
    out_fm_valid_all = Signal(bool(0))
    out_fm_valid_none = Signal(bool(0))
    out_fm_last = Signal(bool(0))

    out_valid_sig = Signal(intbv()[coeffs.layer.out_features:])
    in_ready_sig = Signal(intbv()[coeffs.layer.out_features:])

    in_valid_sig = Signal(bool(0))

    @always_comb
    def in_axis_ctrl():
        if in_fm_ready == 1 and in_fm.tvalid == 1:
            in_valid_sig.next = 1
        else:
            in_valid_sig.next = 0

    for i in range(coeffs.layer.out_features):
        in_fm_sigs[i].tvalid = in_valid_sig
        in_fm_sigs[i].tlast = in_fm.tlast
        in_fm_sigs[i].tdata = in_fm.tdata

    if coeffs.layer.out_features > 1:
        in_fm_sigs_ready_c = ConcatSignal(*[x.tready for x in in_fm_sigs])
    else:
        in_fm_sigs_ready_c = in_fm_sigs[0].tready

    @always_comb
    def dlogic_ready():
        in_ready_sig.next = in_fm_sigs_ready_c

    rdy_i = reduction.vec_and(input_vec=in_ready_sig, val=in_fm_ready)

    @always_comb
    def in_rdy_ctrl():
        in_fm.tready.next = in_fm_ready

    linear_inst = list()
    for i in range(coeffs.layer.out_features):
        linear_inst.append(
            linear_single(clk, reset,
                          in_fm=in_fm_sigs[i],
                          out_data=out_fm_sigs[i],
                          weights=coeffs.weight[i],
                          bias=coeffs.bias[i],
                          fxp=fxp,
                          activation=activation))

    ass_i = []

    if coeffs.layer.out_features > 1:
        out_fm_sigs_valid_c = ConcatSignal(*[x.tvalid for x in out_fm_sigs])
    else:
        out_fm_sigs_valid_c = out_fm_sigs[0].tvalid

    @always_comb
    def logic_valid():
        out_valid_sig.next = out_fm_sigs_valid_c

    val_i = reduction.vec_and(input_vec=out_valid_sig, val=out_fm_valid_all)
    valn_i = reduction.vec_nand(input_vec=out_valid_sig, val=out_fm_valid_none)

    ass_i.append(dl2hdl.basic.utils.or_list_of_bits(out_fm_last, [x.tlast for x in out_fm_sigs], len(out_fm_sigs)))

    out_ready_sig = Signal(bool(0))

    @always_comb
    def out_ready_ctrl():
        if out_fm.tready == 1 and (out_fm_valid_all == 1 or out_fm_valid_none == 1):
            out_ready_sig.next = 1
        else:
            out_ready_sig.next = 0

    # FIXME: serious problem here:
    # this works in simulation but not in conversion
    # @always_comb
    # def test():
    #     for i in range(coeffs.layer.out_features):
    #         out_fm_sigs[i].tready.next = out_ready_sig

    # this works in conversion but not in simulation
    # for i in range(coeffs.layer.out_features):
    #     out_fm_sigs[i].tready = out_ready_sig

    # this works both in simulation and conversion

    for i in range(coeffs.layer.out_features):
        ass_i.append(dummy_assign(out_fm_sigs[i].tready, out_ready_sig))
    ############################## end of fixme

    # ass_i.append(dl2hdl.utils.assign_list(out_fm.tdata, [x.tdata for x in out_fm_sigs], len(out_fm_sigs), fxp.width))
    if coeffs.layer.out_features > 1:
        out_fm_sigs_data_c = ConcatSignal(*[x.tdata for x in out_fm_sigs])
    else:
        out_fm_sigs_data_c = out_fm_sigs[0].tdata

    # out_fm.tdata = ConcatSignal(*[x.tdata for x in out_fm_sigs])

    @always_comb
    def out_axis_ctrl():
        out_fm.tvalid.next = out_fm_valid_all
        out_fm.tlast.next = out_fm_last
        out_fm.tdata.next = out_fm_sigs_data_c

    return instances()
