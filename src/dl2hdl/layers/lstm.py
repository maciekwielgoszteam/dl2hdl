import torch
import torch.nn as nn
from dl2hdl.basic.fixed_def import FixedDef
from myhdl import block, intbv, Signal, instances, always_seq
from dl2hdl.layers.linear import linear
from dl2hdl.layers.linear import LinearCoeffs
import dl2hdl.axi.common as axis
from dl2hdl.layers.activation.sigmoid import sigmoid, hard_sigmoid
from dl2hdl.layers.activation.tanh import tanh
from dl2hdl.axi.axis import Axis
from ml.layers.lstm_hard import LSTMHardSigmoid


class LSTMCoeffs:
    """
    Wrapper class for LSTM layer coefficients and biases
    Holds both floating and fixed point representation
    """

    def __init__(self, from_torch=None, fxp=None):
        """
        Linear cell weights and biases wrapper ctor

        :param from_torch: torch.nn.LSTM object of LSTM recurrent layer
        :param fxp: fixed point representation
        """
        if fxp is not None:
            assert isinstance(fxp, FixedDef)
            self._fxp = fxp

        if from_torch is not None:
            assert isinstance(from_torch, (torch.nn.LSTM, LSTMHardSigmoid))
            self._layer = from_torch

    @property
    def fxp(self):
        return self._fxp

    @fxp.setter
    def fxp(self, value):
        assert isinstance(value, FixedDef)
        self._fxp = value

    @property
    def layer(self):
        return self._layer

    @property
    def weight_ih(self):
        return self._layer.weight_ih_l0.data.cpu().numpy()

    @property
    def bias_ih(self):
        return self._layer.bias_ih_l0.data.cpu().numpy()

    @property
    def weight_hh(self):
        return self._layer.weight_hh_l0.data.cpu().numpy()

    @property
    def bias_hh(self):
        return self._layer.bias_hh_l0.data.cpu().numpy()

    @property
    def weight_ih_fxp(self):
        return self.fxp.to_fixed(self.weight_ih)

    @property
    def bias_ih_fxp(self):
        return self.fxp.to_fixed(self.bias_ih)

    @property
    def weight_hh_fxp(self):
        return self.fxp.to_fixed(self.weight_hh)

    @property
    def bias_hh_fxp(self):
        return self.fxp.to_fixed(self.bias_hh)


@block
def LSTM_cell(clk, reset, in_data, in_cell, in_hidden, out_cell, out_hidden, coeffs=None, sigmoid_type='soft', fxp=None):
    """
    Single LSTM Cell

    :param clk: clock
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param in_data: concatenated input data
    :type in_data: AXI Stream
    :param in_hidden: concatenated input hidden data
    :type in_hidden: AXI Stream
    :param in_cell: concatenated input cell data
    :type in_cell: AXI Stream
    :param out_hidden: concatenated output hidden data
    :type out_hidden: AXI Stream
    :param out_cell: concatenated output cell data
    :type out_cell: AXI Stream
    :param coeffs: object with weights, biases, sizes and fixed point
    :type coeffs: LSTMCoeffs
    :param sigmoid_type: type of sigmoid to use: soft or hard
    :type sigmoid_type: string
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :return: None
    :raises: Assertion errors

    For each element in the input sequence, each layer computes the following
    function:

    .. math::

            \begin{array}{ll}
            i_t = \sigma(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi}) \\
            f_t = \sigma(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf}) \\
            g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hg} h_{(t-1)} + b_{hg}) \\
            o_t = \sigma(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho}) \\
            c_t = f_t c_{(t-1)} + i_t g_t \\
            h_t = o_t \tanh(c_t)
            \end{array}

    """

    linear_ih_result = Axis(intbv()[coeffs.layer.hidden_size * 4 * fxp.width:])
    linear_hh_result = Axis(intbv()[coeffs.layer.hidden_size * 4 * fxp.width:])
    linear_sum_result = Axis(intbv()[coeffs.layer.hidden_size * 4 * fxp.width:])
    gate_I = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    gate_F = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    gate_G = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    gate_O = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    gate_I_act = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    gate_F_act = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    gate_G_act = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    gate_O_act = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    mul_IG = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    mul_FCprev = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    Cnext = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    Cnext_raw = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])
    Cnext_act = Axis(intbv()[coeffs.layer.hidden_size * fxp.width:])

    if sigmoid_type == 'soft':
        LSTM_sigmoid = sigmoid
    elif sigmoid_type == 'hard':
        LSTM_sigmoid = hard_sigmoid
    else:
        raise NotImplementedError(f'{sigmoid_type} type of sigmoid is not supported')

    sum_i = []
    mul_i = []
    axis_i = []

    linear_ih = nn.Linear(in_features=coeffs.layer.input_size, out_features=coeffs.layer.hidden_size * 4)
    coeffs_ih = LinearCoeffs(from_torch=linear_ih, fxp=fxp)
    coeffs_ih.weight = coeffs.weight_ih
    coeffs_ih.bias = coeffs.bias_ih
    linear_hh = nn.Linear(in_features=coeffs.layer.hidden_size, out_features=coeffs.layer.hidden_size * 4)
    coeffs_hh = LinearCoeffs(from_torch=linear_hh, fxp=fxp)
    coeffs_hh.weight = coeffs.weight_hh
    coeffs_hh.bias = coeffs.bias_hh

    # W_{i} x_t + b_{i} + W_{h} h_{(t-1)} + b_{h}
    lin_ih = linear(clk, reset, in_fm=in_data, out_fm=linear_ih_result, coeffs=coeffs_ih, fxp=fxp)
    lin_hh = linear(clk, reset, in_fm=in_hidden, out_fm=linear_hh_result, coeffs=coeffs_hh, fxp=fxp)
    sum_i.append(axis.elementwise_sum_comb(in_a=linear_ih_result, in_b=linear_hh_result, out_fm=linear_sum_result, fxp=fxp))

    axis_i.append(axis.split_4_comb(bus_a=gate_I, bus_b=gate_F, bus_c=gate_G, bus_d=gate_O, bus_in=linear_sum_result))

    # i_t = \sigma(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi})
    act_i = LSTM_sigmoid(clk, reset, in_fm=gate_I, out_fm=gate_I_act, fxp=fxp)
    # f_t = \sigma(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf})
    act_f = LSTM_sigmoid(clk, reset, in_fm=gate_F, out_fm=gate_F_act, fxp=fxp)
    # g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hg} h_{(t-1)} + b_{hg})
    act_g = tanh(clk, reset, in_fm=gate_G, out_fm=gate_G_act, implementation='linear', fxp=fxp)
    # o_t = \sigma(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho})
    act_o = LSTM_sigmoid(clk, reset, in_fm=gate_O, out_fm=gate_O_act, fxp=fxp)

    # c_t = f_t c_{(t-1)} + i_t g_t
    mul_i.append(axis.elementwise_mul_comb(in_a=gate_I_act, in_b=gate_G_act, out_fm=mul_IG, fxp=fxp))
    mul_i.append(axis.elementwise_mul_comb(in_a=gate_F_act, in_b=in_cell, out_fm=mul_FCprev, fxp=fxp))
    sum_i.append(axis.elementwise_sum_comb(in_a=mul_IG, in_b=mul_FCprev, out_fm=Cnext, fxp=fxp))

    axis_i.append(axis.broadcast_2_comb(bus_a=Cnext_raw, bus_b=out_cell, bus_in=Cnext))

    # h_t = o_t \tanh(c_t)
    act_out = tanh(clk, reset, in_fm=Cnext_raw, out_fm=Cnext_act, implementation='linear', fxp=fxp)
    mul_i.append(axis.elementwise_mul_comb(in_a=Cnext_act, in_b=gate_O_act, out_fm=out_hidden, fxp=fxp))

    return instances()


@block
def LSTM(clk, reset, in_data, out_data, coeffs=None, sequence_length=1, sigmoid_type='soft', fxp=None):
    """
    Single LSTM layer

    :param clk: clock
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param in_data: concatenated input data
    :type in_data: AXI Stream
    :param out_data: concatenated output data
    :type out_data: AXI Stream
    :param coeffs: object with weights, biases, sizes and fixed point
    :type coeffs: LSTMCoeffs
    :param sequence_length: length ot hte sequence to analyse
    :type sequence_length: unsigned integer
    :param sigmoid_type: type of sigmoid to use: soft or hard
    :type sigmoid_type: string
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :return: None
    :raises: Assertion errors
    """

    hid_size = coeffs.layer.hidden_size

    cell = Axis(intbv()[hid_size * fxp.width:])
    cell_reg = Axis(intbv()[hid_size * fxp.width:])
    cell_internal = Axis(intbv()[hid_size * fxp.width:])
    cell_next = Axis(intbv()[hid_size * fxp.width:])

    hidden = Axis(intbv()[hid_size * fxp.width:])
    hidden_next = Axis(intbv()[hid_size * fxp.width:])
    hidden_reg_0 = Axis(intbv()[hid_size * fxp.width:])
    hidden_reg_1 = Axis(intbv()[hid_size * fxp.width:])

    clear_cell_0 = Signal(bool(0))
    set_cell_1 = Signal(bool(0))
    clear_hidden_0 = Signal(bool(0))
    set_hidden_1 = Signal(bool(0))

    counter = Signal(intbv(0, min=0, max=sequence_length + 1))

    @always_seq(clk.posedge, reset)
    def ctrl():
        if counter == 0:
            set_cell_1.next = 1
            set_hidden_1.next = 1
            clear_cell_0.next = 1
            clear_hidden_0.next = 1
            if in_data.tvalid == 1 and in_data.tready == 1:
                counter.next = 1
        else:
            set_cell_1.next = 0
            set_hidden_1.next = 0
            clear_cell_0.next = 0
            clear_hidden_0.next = 0
            if counter < sequence_length:
                if in_data.tvalid == 1 and in_data.tready == 1:
                    counter.next = counter + 1
            else:
                counter.next = 0

    lstm_cell_i = LSTM_cell(clk, reset,
                            in_data=in_data,
                            in_cell=cell,
                            in_hidden=hidden,
                            out_cell=cell_next,
                            out_hidden=hidden_next,
                            coeffs=coeffs,
                            sigmoid_type=sigmoid_type,
                            fxp=fxp)

    cell_reg_0_i = axis.axis_reg(clk, reset,
                                 bus_m=cell_reg,
                                 bus_s=cell_next,
                                 clear_reg=clear_cell_0,
                                 set_reg=0,
                                 value=0,
                                 last=1)

    cell_reg_1_i = axis.axis_reg(clk, reset,
                                 bus_m=cell_internal,
                                 bus_s=cell_reg,
                                 clear_reg=0,
                                 set_reg=set_cell_1,
                                 value=0,
                                 last=1)

    cell_reg_2_i = axis.axis_reg(clk, reset,
                                 bus_m=cell,
                                 bus_s=cell_internal,
                                 clear_reg=0,
                                 set_reg=0,
                                 value=0,
                                 last=1)

    hidden_broadcast_i = axis.broadcast_2_comb(bus_a=out_data,
                                               bus_b=hidden_reg_0,
                                               bus_in=hidden_next)

    hidden_reg_0_i = axis.axis_reg(clk, reset,
                                   bus_m=hidden_reg_1,
                                   bus_s=hidden_reg_0,
                                   clear_reg=clear_hidden_0,
                                   set_reg=0,
                                   value=0,
                                   last=1)

    hidden_reg_1_i = axis.axis_reg(clk, reset,
                                   bus_m=hidden,
                                   bus_s=hidden_reg_1,
                                   clear_reg=0,
                                   set_reg=set_hidden_1,
                                   value=0,
                                   last=1)

    return instances()
