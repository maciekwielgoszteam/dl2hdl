from myhdl import Signal, instances, block, always_seq, ConcatSignal, intbv, always_comb
import numpy as np


@block
def maxpool_single_comb(in_fm, maxpool, in_size=4, fxp=None):
    """
    Performs combinatinal maxpool operation over single pooling region

    Args:
        in_fm: Port, concatenated list of signals comprising a single region
        maxpool: Port, maximal value in region
        bitwidth: Parameter, width of the single input data
        in_size: parameter, number of signals in region
        fxp: Parameter, fixed point parameters definition, if not provided - unsigned comparison is used

    Returns:
        None

    Raises:
        AssertionErrors

    TODO: Tree like implementation instead of chain
    """
    assert type(in_size) is int, 'Flat pooling requires integer size'
    assert in_size > 1, 'Cannot perform max pooling over single element'
    assert fxp is not None, 'You have to provide fixed point definition'

    fm_sigs = [Signal(intbv()[fxp.width:]) for _ in range(in_size)]
    max_val = [Signal(intbv()[fxp.width:]) for _ in range(in_size - 1)]

    @always_comb
    def map_to_signals():
        for i in range(in_size):
            fm_sigs[i].next = in_fm[((in_size - 1) - i) * fxp.width + fxp.width:((in_size - 1) - i) * fxp.width]

    if fxp is None:
        @block
        def max_of_two(m, x, y):
            @always_comb
            def logic():
                if x > y:
                    m.next = x
                else:
                    m.next = y

            return logic
    else:
        @block
        def max_of_two(m, x, y):
            @always_comb
            def logic():
                if x.signed() > y.signed():
                    m.next = x
                else:
                    m.next = y

            return logic

    max_inst = list()
    max_inst.append(max_of_two(max_val[0], fm_sigs[0], fm_sigs[1]))
    for i in range(1, in_size - 1):
        max_inst.append(max_of_two(max_val[i], max_val[i - 1], fm_sigs[i + 1]))

    @always_comb
    def return_logic():
        maxpool.next = max_val[in_size - 2]

    return instances()


@block
def maxpool_flat_comb(in_fm, maxpool, in_size=4, pool_size=2, stride=None, fxp=None):
    """
    Performs combinatinal maxpool operation over single feature map

    Args:
        in_fm: Port, concatenated list of signals comprising a complete feature map
        maxpool: Port, concatenated list of maximal values in feature map
        in_size: Parameter, number of signals in region
        pool_size: Parameter, pooling size
        stride: Parameter, pooling stride, currently only stride == pool_size supported
        fxp: Parameter, fixed point parameters definition, if not provided - unsigned comparison is used

    Returns:
        None

    Raises:
        AssertionErrors
        ValueError: if stride is not equal to pool_size

    TODO: add stride support
    """

    assert fxp is not None, 'You have to provide fixed point definition'
    assert type(pool_size) is int, 'Flat pooling requires pool_size to be integer'
    if (stride is None) or (stride == pool_size):
        stride = pool_size
    else:
        raise ValueError('Stride different that pool size not supported')

    out_size = in_size // pool_size
    fm_sigs = [Signal(intbv()[fxp.width:]) for _ in range(in_size)]
    fm_concat = [Signal(intbv()[fxp.width * pool_size:]) for _ in range(out_size)]
    max_val = [Signal(intbv()[fxp.width:]) for _ in range(out_size)]
    maxpool_out_fm = Signal(intbv()[fxp.width * out_size:])

    @always_comb
    def map_in():
        for i in range(in_size):
            fm_sigs[i].next = in_fm[((in_size - 1) - i) * fxp.width + fxp.width:((in_size - 1) - i) * fxp.width]

    for i in range(out_size):
        fm_concat[i] = ConcatSignal(*fm_sigs[i * pool_size:i * pool_size + pool_size])

    max_inst = list()
    for i in range(out_size):
        max_inst.append(
            maxpool_single_comb(in_fm=fm_concat[i], maxpool=max_val[i], in_size=pool_size, fxp=fxp))

    maxpool_out_fm = ConcatSignal(*max_val)

    @always_comb
    def return_logic():
        maxpool.next = maxpool_out_fm

    return instances()


@block
def maxpool_flat_seq(clk, reset, in_fm, maxpool, in_size=4, pool_size=2, stride=None, fxp=None):
    """
    Performs sequential maxpool operation over single feature map

    Args:
        clk: Port, clock signal
        reset: Port, reset signal
        in_fm: Port, concatenated list of signals comprising a complete feature map
        maxpool: Port, concatenated list of maximal values in feature map
        in_size: Parameter, number of signals in region
        pool_size: Parameter, pooling size
        stride: Parameter, pooling stride, currently only stride == pool_size supported
        fxp: Parameter, ixed point parameters definition, if not provided - unsigned comparison is used

    Returns:
        None

    """
    maxpool_comb = Signal(intbv()[len(maxpool):])
    pool_flat_comb_inst = maxpool_flat_comb(in_fm=in_fm, maxpool=maxpool_comb, in_size=in_size, pool_size=pool_size, stride=stride, fxp=fxp)

    @always_seq(clk.posedge, reset=reset)
    def seq_proc():
        maxpool.next = maxpool_comb

    return instances()


@block
def maxpool_1d_comb(in_fm, maxpool, in_size=(8, 4), pool_size=2, stride=None, fxp=None):
    """
    Performs combinatinal maxpool operation

    Args:
        in_fm: Port, concatenated list of signals comprising a complete feature map
        maxpool: Port, concatenated list of maximal values in feature map
        in_size: Parameter, number of signals in region
        pool_size: Parameter, pooling size
        stride: Parameter, pooling stride, currently only stride == pool_size supported
        fxp: Parameter, fixed point parameters definition, if not provided - unsigned comparison is used

    Returns:
        None

    Raises:
        AssertionErrors
        ValueError: if stride is not equal to pool_size

    TODO: add stride support
    """
    assert fxp is not None, 'You have to provide fixed point definition'
    assert np.ndim(in_size) == 1, 'Can only accept tuples with one dimension'
    assert len(in_size) == 2, 'Input size can only have 2 parameters in 2D pooling'
    if type(pool_size) is not int:
        assert np.ndim(pool_size) == 1, 'Can only accept tuples with one dimension'
        assert len(pool_size) == 1, 'pool size can only have 2 parameters in 2D pooling'
        pool_size = pool_size[0]
    if (stride is None) or (stride == pool_size):
        stride = pool_size
    else:
        raise ValueError('Stride different that pool size not supported')

    out_fms = in_size[0]
    out_size = in_size[1] // pool_size
    single_fm_sigs = [Signal(intbv()[fxp.width * in_size[1]:]) for d in range(in_size[0])]
    max_val = [Signal(intbv()[fxp.width * out_size:]) for d in range(out_fms)]
    maxpool_out_fm = Signal(intbv()[fxp.width * out_size * out_fms:])

    max_idx = np.prod(in_size) * fxp.width

    in_size_0_int = in_size[0]
    in_size_1_int = in_size[1]

    @always_comb
    def map_in():
        for d in range(in_size_0_int):
            single_fm_sigs[d].next = in_fm[(in_size_0_int - d) * in_size_1_int * fxp.width: (in_size_0_int - 1 - d) * in_size_1_int * fxp.width]

    pool_fm_inst = list()
    for d in range(out_fms):
        pool_fm_inst.append(
            maxpool_flat_comb(in_fm=single_fm_sigs[d], maxpool=max_val[d], in_size=in_size[1], pool_size=pool_size, fxp=fxp))

    maxpool_out_fm = ConcatSignal(*max_val)

    @always_comb
    def return_logic():
        maxpool.next = maxpool_out_fm

    return instances()


@block
def maxpool_1d_seq(clk, reset, in_fm, maxpool, in_size=(8, 4), pool_size=2, stride=None, fxp=None):
    """
    Performs sequential maxpool operation

    Args:
        clk: Port, clock signal
        reset: Port, reset signal
        in_fm: Port, concatenated list of signals comprising a complete feature map
        maxpool: Port, concatenated list of maximal values in feature map
        in_size: Parameter, shape of signals in region
        pool_size: Parameter, pooling size
        stride: Parameter, pooling stride, currently only stride == pool_size supported
        fxp: Parameter, ixed point parameters definition, if not provided - unsigned comparison is used

    Returns:
        None

    Rises:
        AssertionErrors

    """
    assert np.ndim(in_size) == 1, 'Can only accept tuples with one dimension'
    assert len(in_size) == 2, 'Input size can only have 2 parameters in 2D pooling'

    maxpool_comb = Signal(intbv()[len(maxpool):])
    pool_flat_comb_inst = maxpool_1d_comb(in_fm=in_fm, maxpool=maxpool_comb, in_size=in_size, pool_size=pool_size, stride=stride, fxp=fxp)

    @always_seq(clk.posedge, reset=reset)
    def seq_proc():
        maxpool.next = maxpool_comb

    return instances()
