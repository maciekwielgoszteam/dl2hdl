from myhdl import block, Signal, always_comb, always_seq, instances, intbv, ConcatSignal
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.fixed_point import fixbv
from dl2hdl.layers.analysis.sigmoid_test import sigmoid_series_single
from dl2hdl.layers.analysis.sigmoid_test import sigmoid as sigmoid_true


@block
def sigmoid_single(clk, reset, in_data, out_data, dist_span=2, fxp=None):
    """
    Calculate sigmoid() for a single fixed point data signal
    sig(x) = (1 / 2.0) + (x / 4.0) - (x ** 3 / 48.0) + (x ** 5 / 480.0) - (x ** 7 * 17 / 80640) + (x ** 9 * 31 / 1451520) + ...
    http://mathworld.wolfram.com/SigmoidFunction.html
    :param clk: clock signal
    :param reset, Reset signal
    :param in_data: Port, input signal
    :param out_data: Port, output signal
    :param dist_span: parameter, span of data distribution
    :param fxp: Parameter, fixed point definition
    :raises: Assertion errors
    """
    assert fxp is not None, 'You have to provide fixed point definition'
    # Limits present ranges within which error of approximation is smaller than 0.5 LSB
    # None means it never is. As range is symmetric around 0 only one value is provided.
    limits = {
        '1': [None, None, None, None, None, None],
        '2': [0.5, None, None, None, None, None],
        '3': [0.25, 1.54, 1.99, 2.4, 2.27, None],
        '4': [0.12, 1.19, 1.81, 2.15, 2.27, 2.49],
        '5': [0.06, 0.93, 1.56, 1.93, 2.16, 2.33],
        '6': [0.03, 0.73, 1.34, 1.73, 1.99, 2.18],
        '7': [0.01, 0.57, 1.15, 1.56, 1.84, 2.04],
        '8': [0., 0.45, 1., 1.4, 1.69, 1.9]}

    # approx_n = 4
    # for idx, limit in enumerate(limits[str(fxp.f)], 1):
    #     if limit > dist_span:
    #         approx_n = idx - 1
    # we will use fixed value of approx_n for now...
    # FIXME: if necessary make code more generic - choose n, if necessary...
    approx_n = 4

    wide_fxp = FixedDef(fxp.m * (approx_n + 1), fxp.f * (approx_n + 1))

    Maclaurin_series_coeff = [1 / 2, 1 / 4, -1 / 48, 1 / 480, -17 / 80640, 32 / 1451520]
    series_const = [Signal(intbv(wide_fxp.to_fixed(x))[wide_fxp.width:], constant=True) for x in Maclaurin_series_coeff]

    in_data_w = Signal(intbv()[wide_fxp.width:].signed())
    pow_sig = [Signal(intbv()[wide_fxp.width:]) for _ in range(4)]

    pow_sig_1 = [Signal(intbv()[wide_fxp.width:]) for _ in range(3)]
    pow_sig_2_c = [Signal(intbv()[wide_fxp.width:]) for _ in range(2)]
    pow_sig_2_s = [Signal(intbv()[wide_fxp.width:]) for _ in range(2)]
    pow_sig_3_c = [Signal(intbv()[wide_fxp.width:]) for _ in range(2)]
    pow_sig_3_s = [Signal(intbv()[wide_fxp.width:]) for _ in range(2)]
    pow_sig_5 = Signal(intbv()[wide_fxp.width:])

    mul_sig_c = [Signal(intbv()[wide_fxp.width:]) for _ in range(approx_n)]
    mul_sig_s = [Signal(intbv()[wide_fxp.width:]) for _ in range(approx_n)]
    accu_sig = Signal(intbv()[wide_fxp.width:].signed())
    sigmoid_w = Signal(intbv()[wide_fxp.width:].signed())
    out_data_signed = Signal(intbv()[wide_fxp.width:].signed())

    @always_comb
    def map_in():
        in_data_w.next = in_data.signed() << (wide_fxp.f - fxp.f)

    mul_i = []
    mul_i.append(wide_fxp.fx_mul(pow_sig_2_c[0], in_data_w, in_data_w))
    mul_i.append(wide_fxp.fx_mul(pow_sig_3_c[0], pow_sig_2_s[0], pow_sig_1[0]))
    mul_i.append(wide_fxp.fx_mul(pow_sig_5, pow_sig_2_s[1], pow_sig_3_s[0]))

    @always_seq(clk.posedge, reset)
    def pow_logic_0():
        pow_sig_2_s[0].next = pow_sig_2_c[0]
        pow_sig_1[0].next = in_data_w[wide_fxp.width:]
        pow_sig_2_s[1].next = pow_sig_2_s[0]
        pow_sig_3_s[0].next = pow_sig_3_c[0]
        pow_sig_1[1].next = pow_sig_1[0]
        pow_sig_3_s[1].next = pow_sig_3_s[0]

        pow_sig[0].next = 1 * 2 ** wide_fxp.f
        pow_sig[1].next = pow_sig_1[1]
        pow_sig[2].next = pow_sig_3_s[1]
        pow_sig[3].next = pow_sig_5

    for i in range(approx_n):
        mul_i.append(wide_fxp.fx_mul(mul_sig_c[i], series_const[i], pow_sig[i]))

    @always_seq(clk.posedge, reset)
    def accy_map():
        for i in range(4):
            mul_sig_s[i].next = mul_sig_c[i]

    @always_seq(clk.posedge, reset)
    def accu_logic():
        accu_sig.next = mul_sig_s[0].signed() + mul_sig_s[1].signed() + mul_sig_s[2].signed() + mul_sig_s[3].signed()

    monotonicity_step = 1 / 2 ** fxp.f

    @always_comb
    def sig_neg_true():
        if accu_sig.signed() < 0:
            sigmoid_w.next = 0
        elif accu_sig.signed() > 1 * 2 ** wide_fxp.f:
            sigmoid_w.next = 1 * 2 ** wide_fxp.f
        else:
            sigmoid_w.next = accu_sig

    # if in_data.signed() < 0:
    #     if abs(sigmoid_true(in_data.signed()) - accu_sig) > sigmoid_true(in_data.signed()) or \
    #             sigmoid_series_single(in_data.signed() + monotonicity_step, approx_n) < accu_sig:
    #         @always_comb
    #         def sig_zero():
    #             sigmoid_w.next = 0
    #     else:
    #         @always_comb
    #         def sig_neg_true():
    #             sigmoid_w.next = accu_sig
    # if in_data.signed() >= 0:
    #     if abs(sigmoid_true(in_data.signed()) - accu_sig) > abs(sigmoid_true(in_data.signed()) - 1) or \
    #             sigmoid_series_single(in_data.signed() - monotonicity_step, approx_n) > accu_sig:
    #         @always_comb
    #         def sig_one():
    #             sigmoid_w.next = 1
    #     else:
    #         @always_comb
    #         def sig_pos_true():
    #             sigmoid_w.next = accu_sig

    @always_comb
    def signed_logic():
        out_data_signed.next = sigmoid_w

    @always_comb
    def return_logic():
        out_data.next = out_data_signed[fxp.f * (approx_n + 1) + fxp.m:fxp.f * approx_n]

    return instances()


@block
def sigmoid(clk, reset, in_fm, out_fm, fxp=None):
    """
    Performs sigmoid() over single feature map
    :param clk: clock
    :param reset: reset signal
    :param in_fm: input feature map
    :type in_fm: Axis
    :param out_fm: output feature map
    :type out_fm: Axis
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :raises AssertionErrors
    """
    assert fxp is not None, 'You have to provide fixed point definition'
    size = len(in_fm.tdata) // fxp.width

    fm_sigs = [Signal(intbv()[fxp.width:]) for _ in range(size)]
    sig_val = [Signal(intbv()[fxp.width:]) for _ in range(size)]

    seq_levels = 7
    valid_buff = [Signal(bool(0)) for _ in range(seq_levels)]
    last_buff = [Signal(bool(0)) for _ in range(seq_levels)]

    @always_seq(clk.posedge, reset)
    def axi_ctrl():
        if out_fm.tready == 1:
            for i in range(size):
                if in_fm.tvalid == 1:
                    fm_sigs[i].next = in_fm.tdata[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width]
                    valid_buff[0].next = 1
                    last_buff[0].next = in_fm.tlast
                else:
                    valid_buff[0].next = 0
                    last_buff[0].next = 0
            for i in range(1, seq_levels):
                valid_buff[i].next = valid_buff[i - 1]
                last_buff[i].next = last_buff[i - 1]

    sigmoid_inst = list()
    for i in range(size):
        sigmoid_inst.append(sigmoid_single(clk, reset, in_data=fm_sigs[i], out_data=sig_val[i], fxp=fxp))

    sig_out_fm = ConcatSignal(*sig_val)

    @always_comb
    def axis_map():
        out_fm.tdata.next = sig_out_fm
        in_fm.tready.next = out_fm.tready
        out_fm.tvalid.next = valid_buff[seq_levels - 1]
        out_fm.tlast.next = last_buff[seq_levels - 1]

    return instances()


@block
def hard_sigmoid_single_comb(in_data, out_data, scale_factor=0.2, fxp=None):
    """
    Calculate hard_sigmoid() for a single fixed point data signal
    hard_sig(x) = max(0., min(1., x * scale_factor + 0.5))
    :param clk: clock signal
    :param reset, Reset signal
    :param in_data: Port, input signal
    :param out_data: Port, output signal
    :param scale_factor: parameter, scale factor, default 0.2 gives least error but 0.125 and 0.25 make multiplication for free
    :param fxp: Parameter, fixed point definition
    :raises: Assertion errors
    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
    mul_const = Signal(intbv(fxp.to_fixed(scale_factor))[fxp.width:], constant=True)
    add_const = Signal(intbv(fxp.to_fixed(0.5))[fxp.width:], constant=True)
    one_const = Signal(intbv(fxp.to_fixed(1.0))[fxp.width:], constant=True)

    mul_sig = Signal(intbv()[fxp.width:])
    add_sig = Signal(intbv()[fxp.width:])

    mul_i = fxp.fx_mul(mul_sig, in_data, mul_const)
    add_i = fxp.fx_add(add_sig, mul_sig, add_const)

    @always_comb
    def return_logic():
        if add_sig.signed() < 0:
            out_data.next = 0
        elif add_sig.signed() > one_const:
            out_data.next = one_const
        else:
            out_data.next = add_sig

    return instances()


@block
def hard_sigmoid(clk, reset, in_fm, out_fm, scale_factor=0.2, fxp=None):
    """
    Performs combinational sigmoid() over single feature map
    :param clk: clock
    :param reset: reset signal
    :param in_fm: input feature map
    :type in_fm: Axis
    :param out_fm: output feature map
    :type out_fm: Axis
    :param scale_factor: parameter, scale factor, default 0.2 gives least error but 0.125 and 0.25 make multiplication for free
    :type scale_factor: float
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :raises AssertionErrors
    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
    size = len(in_fm.tdata) // fxp.width

    fm_sigs = [Signal(intbv()[fxp.width:]) for _ in range(size)]
    sig_val = [Signal(intbv()[fxp.width:]) for _ in range(size)]

    valid_buff = Signal(bool(0))
    last_buff = Signal(bool(0))

    @always_seq(clk.posedge, reset)
    def axi_ctrl():
        if out_fm.tready == 1:
            for i in range(size):
                if in_fm.tvalid == 1:
                    fm_sigs[i].next = in_fm.tdata[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width]
                    valid_buff.next = 1
                    last_buff.next = in_fm.tlast
                else:
                    valid_buff.next = 0
                    last_buff.next = 0

    sigmoid_inst = list()
    for i in range(size):
        sigmoid_inst.append(hard_sigmoid_single_comb(in_data=fm_sigs[i], out_data=sig_val[i], scale_factor=scale_factor, fxp=fxp))

    sig_out_fm = ConcatSignal(*sig_val)

    @always_comb
    def axis_map():
        out_fm.tdata.next = sig_out_fm
        in_fm.tready.next = out_fm.tready
        out_fm.tvalid.next = valid_buff
        out_fm.tlast.next = last_buff

    return instances()
