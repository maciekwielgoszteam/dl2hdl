from myhdl import block, Signal, intbv, always_seq, always_comb, ConcatSignal, instances
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.basic.utils import get_indices, get_seq_levels
from dl2hdl.math.exponent import exponent, exponent_lut

@block
def add_seq(clk, reset, a, b, c):
    """
    Sequential addition
    :param clk: clock signal
    :type clk: Signal(bool)
    :param reset: reset signal
    :type reset: ResetSignal
    :param a: addend
    :param b: addend
    :param c: sum
    """

    @always_seq(clk.posedge, reset)
    def logic():
        c.next = a + b

    return logic


@block
def add_comb(a, b, c):
    """
    Combinational addition
    :param a: addend
    :param b: addend
    :param c: sum
    """

    @always_comb
    def logic():
        c.next = a + b

    return logic


@block
def softmax(clk, reset, in_fm, out_fm, fxp=None, exp_lut=True):
    """
    Performs combinational sigmoid() over single feature map
    :param clk: clock
    :param reset: reset signal
    :param in_fm: input feature map
    :type in_fm: Axis
    :param out_fm: output feature map
    :type out_fm: Axis
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :param exp_lut: if true, exponent function wil be implemented in LUTs, if false, will be calculated
    :type exp_lut: bool
    :raises AssertionErrors
    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
    size = len(in_fm.tdata) // fxp.width

    seq_level_step = 2  # How often to insert register between level reduction in summation, 0 - never
    calc_elems_len = 2 * size - 1
    indices = get_indices(size)
    fm_sigs = [Signal(intbv()[fxp.width:]) for _ in range(size)]
    exp_sigs = [Signal(intbv()[fxp.width:]) for _ in range(size)]
    exp_sigs_split = [Signal(intbv()[fxp.width:]) for _ in range(calc_elems_len)]
    exp_sigs_helper = [Signal(intbv()[fxp.width:]) for _ in range(calc_elems_len)]
    sig_val = [Signal(intbv()[fxp.width:]) for _ in range(size)]

    seq_levels = get_seq_levels(len(indices), seq_level_step)
    # in_fm_tready = Signal(bool(0))
    valid_buff = [Signal(bool(0)) for _ in range(seq_levels)]
    last_buff = [Signal(bool(0)) for _ in range(seq_levels)]

    @always_seq(clk.posedge, reset)
    def axi_ctrl():
        if out_fm.tready == 1:
            for i in range(size):
                if in_fm.tvalid == 1:  # and in_fm_tready == 1:
                    fm_sigs[i].next = in_fm.tdata[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width]
                    # in_fm_tready.next = 0
                    valid_buff[0].next = 1
                    last_buff[0].next = in_fm.tlast
                else:
                    valid_buff[0].next = 0
                    last_buff[0].next = 0
            for i in range(1, seq_levels):
                valid_buff[i].next = valid_buff[i - 1]
                last_buff[i].next = last_buff[i - 1]

    exp_inst = []

    for i in range(size):
        if exp_lut:
            exp_inst.append(exponent_lut(value=fm_sigs[i], result=exp_sigs[i], fxp=fxp))
        else:
            exp_inst.append(exponent(clk, reset, in_data=fm_sigs[i], out_data=exp_sigs[i], fxp=fxp))


    # For reasons unknown to me I cannot assign exponent results directly to list of signals, but assigning to concatenated signal and then splitting works.
    exp_sigs_fm = ConcatSignal(*exp_sigs)

    @always_comb
    def exp_split_map():
        for i in range(calc_elems_len):
            if i < size:
                exp_sigs_split[i].next = exp_sigs_fm[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width]
            else:
                exp_sigs_split[i].next = exp_sigs_helper[i]

    add_i = []

    if seq_level_step == 1:
        @always_seq(clk.posedge, reset)
        def add_seq_logic():
            for i in range(size, calc_elems_len):
                exp_sigs_helper[i].next = exp_sigs_split[2 * i - 2 * size] + exp_sigs_split[2 * i - 2 * size + 1]
    else:
        com_ind = []
        seq_ind = []
        for l in range(1, len(indices)):
            if seq_level_step == 0:
                com_ind += indices[l]
            else:
                if l % seq_level_step == 0:
                    seq_ind += indices[l]
                else:
                    com_ind += indices[l]

        for i in range(size, calc_elems_len):
            if i in seq_ind:
                add_i.append(add_seq(clk, reset, exp_sigs_split[2 * i - 2 * size], exp_sigs_split[2 * i - 2 * size + 1], exp_sigs_helper[i]))

        for i in range(size, calc_elems_len):
            if i in com_ind:
                add_i.append(add_comb(exp_sigs_split[2 * i - 2 * size], exp_sigs_split[2 * i - 2 * size + 1], exp_sigs_helper[i]))

    div_i = []
    for i in range(size):
        div_i.append(fxp.fx_div(sig_val[i], exp_sigs_split[i], exp_sigs_split[calc_elems_len - 1]))

    sig_out_fm = ConcatSignal(*sig_val)

    @always_comb
    def axis_map():
        out_fm.tdata.next = sig_out_fm
        # in_fm.tready.next = in_fm_tready
        in_fm.tready.next = out_fm.tready
        out_fm.tvalid.next = valid_buff[seq_levels - 1]
        out_fm.tlast.next = last_buff[seq_levels - 1]

    return instances()


@block
def softmax2(clk, reset, in_fm, out_fm, fxp=None):
    """
    Performs combinational sigmoid() over single feature map that contains exactly 2 elements
    :param clk: clock
    :param reset: reset signal
    :param in_fm: input feature map
    :type in_fm: Axis
    :param out_fm: output feature map
    :type out_fm: Axis
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :raises AssertionErrors
    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
    size = len(in_fm.tdata) // fxp.width
    assert size == 2, 'this version of softmax is designed to work with size == 2'

    fm_sigs = [Signal(intbv()[fxp.width:]) for _ in range(size)]
    exp_sig_0 = Signal(intbv()[fxp.width:])
    exp_sig_1 = Signal(intbv()[fxp.width:])
    exp_sigs_sum = Signal(intbv()[fxp.width:])
    sig_val = [Signal(intbv()[fxp.width:]) for _ in range(size)]

    seq_levels = 7
    # in_fm_tready = Signal(bool(0))
    valid_buff = [Signal(bool(0)) for _ in range(seq_levels)]
    last_buff = [Signal(bool(0)) for _ in range(seq_levels)]

    @always_seq(clk.posedge, reset)
    def axi_ctrl():
        if out_fm.tready == 1:
            for i in range(size):
                if in_fm.tvalid == 1:  # and in_fm_tready == 1:
                    fm_sigs[i].next = in_fm.tdata[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width]
                    # in_fm_tready.next = 0
                    valid_buff[0].next = 1
                    last_buff[0].next = in_fm.tlast
                else:
                    valid_buff[0].next = 0
                    last_buff[0].next = 0
            for i in range(1, seq_levels):
                valid_buff[i].next = valid_buff[i - 1]
                last_buff[i].next = last_buff[i - 1]

    exp_inst = [exponent(clk, reset, in_data=fm_sigs[0], out_data=exp_sig_0, fxp=fxp),
                exponent(clk, reset, in_data=fm_sigs[1], out_data=exp_sig_1, fxp=fxp)]

    add_i = fxp.fx_add(exp_sigs_sum, exp_sig_0, exp_sig_1)

    div_i = [fxp.fx_div(sig_val[0], exp_sig_0, exp_sigs_sum),
             fxp.fx_div(sig_val[1], exp_sig_1, exp_sigs_sum)]

    sig_out_fm = ConcatSignal(*sig_val)

    @always_comb
    def axis_map():
        out_fm.tdata.next = sig_out_fm
        # in_fm.tready.next = in_fm_tready
        in_fm.tready.next = out_fm.tready
        out_fm.tvalid.next = valid_buff[seq_levels - 1]
        out_fm.tlast.next = last_buff[seq_levels - 1]

    return instances()
