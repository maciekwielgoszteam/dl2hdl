from myhdl import block, Signal, always_comb, always_seq, instances, intbv, ConcatSignal
from dl2hdl.basic.fixed_def import FixedDef


@block
def tanh_single_series(clk, reset, in_data, out_data, dist_span=2, fxp=None):
    """
    Calculate tanh() for a single fixed point data signal
    tanh(x)= x - (x**3 / 3.0) + (x**5 * 2.0 / 15.0) - (x**7 * 17.0 / 315.0) + (x**9 * 62.0 / 2835.0) + ...
    http://mathworld.wolfram.com/SigmoidFunction.html
    :param clk: clock signal
    :param reset, Reset signal
    :param in_data: Port, input signal
    :param out_data: Port, output signal
    :param dist_span: parameter, span of data distribution
    :param fxp: Parameter, fixed point definition
    :raises: Assertion errors
    """
    assert fxp is not None, 'You have to provide fixed point definition'
    # Limits present ranges within which error of approximation is smaller than 0.5 LSB
    # None means it never is. As range is symmetric around 0 only one value is provided.
    limits = {
        '1': [None, None, None, None, None, None],
        '2': [None, None, None, None, None, None],
        '3': [None, None, None, None, None, None],
        '4': [None, None, None, None, None, None],
        '5': [None, None, None, None, None, None],
        '6': [None, None, None, None, None, None],
        '7': [None, None, None, None, None, None],
        '8': [None, None, None, None, None, None]}
    # WARNING: limits not updated for tanh

    # approx_n = 3
    # for idx, limit in enumerate(limits[str(fxp.f)], 1):
    #     if limit > dist_span:
    #         approx_n = idx - 1
    # we will use fixed value of approx_n for now...
    # FIXME: if necessary make code more generic - choose n, if necessary...
    approx_n = 3

    wide_fxp = FixedDef(fxp.m * (approx_n + 1), fxp.f * (approx_n + 1))

    Taylor_series_coeff = [1.0, -1. / 3., 2. / 15., -17. / 315., 62. / 2835.]
    series_const = [Signal(intbv(wide_fxp.to_fixed(x))[wide_fxp.width:], constant=True) for x in Taylor_series_coeff]

    in_data_w = Signal(intbv()[wide_fxp.width:].signed())
    pow_sig = [Signal(intbv()[wide_fxp.width:]) for _ in range(4)]

    pow_sig_1 = [Signal(intbv()[wide_fxp.width:]) for _ in range(3)]
    pow_sig_2_c = [Signal(intbv()[wide_fxp.width:]) for _ in range(2)]
    pow_sig_2_s = [Signal(intbv()[wide_fxp.width:]) for _ in range(2)]
    pow_sig_3_c = [Signal(intbv()[wide_fxp.width:]) for _ in range(2)]
    pow_sig_3_s = [Signal(intbv()[wide_fxp.width:]) for _ in range(2)]
    pow_sig_5 = Signal(intbv()[wide_fxp.width:])

    mul_sig_c = [Signal(intbv()[wide_fxp.width:]) for _ in range(approx_n)]
    mul_sig_s = [Signal(intbv()[wide_fxp.width:]) for _ in range(approx_n)]
    accu_sig = Signal(intbv()[wide_fxp.width:].signed())
    tanh_w = Signal(intbv()[wide_fxp.width:].signed())
    out_data_signed = Signal(intbv()[wide_fxp.width:].signed())

    @always_comb
    def map_in():
        in_data_w.next = in_data.signed() << (wide_fxp.f - fxp.f)

    mul_i = []
    mul_i.append(wide_fxp.fx_mul(pow_sig_2_c[0], in_data_w, in_data_w))
    mul_i.append(wide_fxp.fx_mul(pow_sig_3_c[0], pow_sig_2_s[0], pow_sig_1[0]))
    mul_i.append(wide_fxp.fx_mul(pow_sig_5, pow_sig_2_s[1], pow_sig_3_s[0]))

    @always_seq(clk.posedge, reset)
    def pow_logic_0():
        pow_sig_2_s[0].next = pow_sig_2_c[0]
        pow_sig_1[0].next = in_data_w[wide_fxp.width:]
        pow_sig_2_s[1].next = pow_sig_2_s[0]
        pow_sig_3_s[0].next = pow_sig_3_c[0]
        pow_sig_1[1].next = pow_sig_1[0]
        pow_sig_3_s[1].next = pow_sig_3_s[0]

        pow_sig[0].next = pow_sig_1[1]
        pow_sig[1].next = pow_sig_3_s[1]
        pow_sig[2].next = pow_sig_5

    for i in range(approx_n):
        mul_i.append(wide_fxp.fx_mul(mul_sig_c[i], series_const[i], pow_sig[i]))

    @always_seq(clk.posedge, reset)
    def accy_map():
        for i in range(approx_n):
            mul_sig_s[i].next = mul_sig_c[i]

    @always_seq(clk.posedge, reset)
    def accu_logic():
        accu_sig.next = mul_sig_s[0].signed() + mul_sig_s[1].signed() + mul_sig_s[2].signed()

    monotonicity_step = 1 / 2 ** fxp.f

    @always_comb
    def sig_neg_true():
        if accu_sig.signed() < -1 * 2 ** wide_fxp.f:
            tanh_w.next = -1 * 2 ** wide_fxp.f
        elif accu_sig.signed() > 1 * 2 ** wide_fxp.f:
            tanh_w.next = 1 * 2 ** wide_fxp.f
        else:
            tanh_w.next = accu_sig

    @always_comb
    def signed_logic():
        out_data_signed.next = tanh_w

    @always_comb
    def return_logic():
        out_data.next = out_data_signed[fxp.f * (approx_n + 1) + fxp.m:fxp.f * approx_n]

    return instances()


@block
def tanh_single_linear(clk, reset, in_data, out_data, fxp=None):
    """
    Calculate tanh() for a single fixed point data signal using linear approximations
    :param clk: clock signal
    :param reset, Reset signal
    :param in_data: Port, input signal
    :param out_data: Port, output signal
    :param fxp: Parameter, fixed point definition
    :raises: Assertion errors
    """
    one_fourth_const = Signal(intbv(fxp.to_fixed(0.25))[fxp.width:].signed(), constant=True)
    minus_one_fourth_const = Signal(intbv(fxp.to_fixed(-0.25))[fxp.width:].signed(), constant=True)
    one_and_a_half_const = Signal(intbv(fxp.to_fixed(1.5))[fxp.width:].signed(), constant=True)
    minus_one_and_a_half_const = Signal(intbv(fxp.to_fixed(-1.5))[fxp.width:].signed(), constant=True)
    half_const = Signal(intbv(fxp.to_fixed(0.5))[fxp.width:].signed(), constant=True)
    minus_half_const = Signal(intbv(fxp.to_fixed(-0.5))[fxp.width:].signed(), constant=True)
    one_const = Signal(intbv(fxp.to_fixed(1.))[fxp.width:], constant=True)
    minus_one_const = Signal(intbv(fxp.to_fixed(-1.))[fxp.width:], constant=True)
    two_const = Signal(intbv(fxp.to_fixed(2.))[fxp.width:], constant=True)
    out_data_signed = Signal(intbv()[fxp.width:].signed())
    div_by_two = Signal(intbv()[fxp.width:])
    shifted_down = Signal(intbv()[fxp.width:])
    shifted_up = Signal(intbv()[fxp.width:])

    div_i = fxp.fx_div(div_by_two, in_data, two_const)
    add_i_0 = fxp.fx_add(shifted_down, div_by_two, minus_one_fourth_const)
    add_i_1 = fxp.fx_add(shifted_up, div_by_two, one_fourth_const)

    @always_seq(clk.posedge, reset)
    def logic():
        if in_data.signed() <= minus_one_and_a_half_const:
            out_data.next = minus_one_const
        elif in_data.signed() > minus_one_and_a_half_const and in_data.signed() <= minus_half_const:
            out_data.next = shifted_down
        elif in_data.signed() > minus_half_const and in_data.signed() <= half_const:
            out_data.next = in_data
        elif in_data.signed() > half_const and in_data.signed() <= one_and_a_half_const:
            out_data.next = shifted_up
        else:
            out_data.next = one_const

    # @always_comb
    # def return_logic():
    #     out_data.next = out_data_signed[fxp.width:]

    return instances()


@block
def tanh(clk, reset, in_fm, out_fm, implementation='maclaurin', fxp=None):
    """
    Performs combinational tanh() over single feature map
    :param clk: clock
    :param reset: reset signal
    :param in_fm: input feature map
    :type in_fm: Axis
    :param out_fm: output feature map
    :type out_fm: Axis
    :param implementation: type of single tanh implementation, can by changed to trade performance, accuracy, resources utilization.
    :type implementation: string
    :param fxp: fixed point definition
    :type fxp: FixedDef
    :raises AssertionErrors
    """
    assert fxp is not None, 'You have to provide fixed point definition'
    size = len(in_fm.tdata) // fxp.width

    # TODO: add LUT, linear and auto
    if implementation == 'series':
        tanh_single = tanh_single_series
    elif implementation == 'linear':
        tanh_single = tanh_single_linear
    else:
        raise NotImplementedError(f'{implementation} implementation of tanh is not supported')

    fm_sigs = [Signal(intbv()[fxp.width:]) for _ in range(size)]
    sig_val = [Signal(intbv()[fxp.width:]) for _ in range(size)]

    seq_levels = 7
    valid_buff = [Signal(bool(0)) for _ in range(seq_levels)]
    last_buff = [Signal(bool(0)) for _ in range(seq_levels)]

    @always_seq(clk.posedge, reset)
    def axi_ctrl():
        if out_fm.tready == 1:
            for i in range(size):
                if in_fm.tvalid == 1:
                    fm_sigs[i].next = in_fm.tdata[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width]
                    valid_buff[0].next = 1
                    last_buff[0].next = in_fm.tlast
                else:
                    valid_buff[0].next = 0
                    last_buff[0].next = 0
            for i in range(1, seq_levels):
                valid_buff[i].next = valid_buff[i - 1]
                last_buff[i].next = last_buff[i - 1]

    tanh_inst = list()
    for i in range(size):
        tanh_inst.append(tanh_single(clk, reset, in_data=fm_sigs[i], out_data=sig_val[i], fxp=fxp))

    sig_out_fm = ConcatSignal(*sig_val)

    @always_comb
    def axis_map():
        out_fm.tdata.next = sig_out_fm
        in_fm.tready.next = out_fm.tready
        out_fm.tvalid.next = valid_buff[seq_levels - 1]
        out_fm.tlast.next = last_buff[seq_levels - 1]

    return instances()
