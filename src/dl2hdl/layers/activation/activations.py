from myhdl import Signal, instances, block, always_seq, ConcatSignal
from myhdl import intbv, always_comb

from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.fixed_point import fixbv


@block
def product_n3_n5(in_data, out_p3, out_p5, fxp=None):
    """
    Generate in_data ** 3, in_data ** 5
    Assumes passed signals are of resolution that allows for multiplications with no precision loss
    :param in_data: Port, input signal
    :param out_p3: Port, output signal in_data**3
    :param out_p5: Port, output signal in_data**5
    :param fxp: Parameter, fixed point definition
    :return:
    """
    assert fxp is not None, 'You have to provide fixed point definition'

    rm = 'round'  # round mode
    in_format = (fxp.width, fxp.m - 1)

    x_p1_lp = Signal(fixbv(value=0, format=in_format, round_mode=rm))  # input precision in_data**1 value
    x_p1 = Signal(fixbv(value=0, format=in_format, round_mode=rm))
    x_p2 = Signal(fixbv(value=0, format=in_format, round_mode=rm))
    x_p3 = Signal(fixbv(value=0, format=in_format, round_mode=rm))
    x_p4 = Signal(fixbv(value=0, format=in_format, round_mode=rm))
    x_p5 = Signal(fixbv(value=0, format=in_format, round_mode=rm))

    @always_comb
    def in_logic():
        x_p1_lp.next = in_data.signed()

    @always_comb
    def x_1_logic():
        x_p1.next = x_p1_lp

    @always_comb
    def x_2_logic():
        x_p2.next = x_p1 * x_p1

    @always_comb
    def x_3_logic():
        x_p3.next = x_p2 * x_p1

    @always_comb
    def x_4_logic():
        x_p4.next = x_p3 * x_p1

    @always_comb
    def x_5_logic():
        x_p5.next = x_p4 * x_p1

    @always_comb
    def return_logic():
        out_p3.next = x_p3
        out_p5.next = x_p5

    return instances()


@block
def tanh_single_comb(in_data, out_data, fxp=None):
    """
        Calculate tanh() for a single fixed point data signal
        tanh(x) = x - (1 / 3 * x ** 3) + (2 / 15 * x ** 5)
        :param in_data: Port, input signal
        :param out_data: Port, output signal
        :param fxp: Parameter, fixed point definition

            Returns:
            AssertionErrors
        """
    assert fxp is not None, 'You have to provide fixed point definition'

    N = 1.2  # tanh approximation is correct on very limited range, outside it must be limited to -1, 1
    res_mul = 5
    rm = 'round'  # round mode

    fxp_calc = FixedDef(res_mul * fxp.m, res_mul * fxp.f)  # calculations fixed point format
    in_format = (fxp.width, fxp.m - 1)  # fixbv input format tuple
    calc_format = (fxp_calc.width, fxp_calc.m - 1)  # intermediate calculations fixbv format tuple

    upres_shift = int((fxp_calc.width - fxp.width) / 2)  # fixbv resolution increase bitshift
    downres_shift = upres_shift  # fixbv resolution decrease bitshift

    # function constant values
    const_1_by_3 = Signal(fixbv(value=(-1.0 / 3), format=calc_format, round_mode=rm))
    const_2_by_15 = Signal(fixbv(value=(2.0 / 15), format=calc_format, round_mode=rm))
    const_1_pos = Signal(fixbv(value=1.0, format=calc_format, round_mode=rm))
    const_1_neg = Signal(fixbv(value=-1.0, format=calc_format, round_mode=rm))

    const_min = Signal(fixbv(value=-N, format=in_format, round_mode=rm))
    const_max = Signal(fixbv(value=N, format=in_format, round_mode=rm))

    const_1_by_3.driven = True
    const_2_by_15.driven = True
    const_1_pos.driven = True
    const_1_neg.driven = True
    const_min.driven = True
    const_max.driven = True

    x_p1_lp = Signal(fixbv(value=0, format=in_format, round_mode=rm))  # input precision in_data**1 value
    x_p1 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    x_p3 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    x_p5 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    a_2 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    a_3 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    ss = Signal(fixbv(value=0, format=calc_format, round_mode=rm))  # expanded precision fixbv tanh() value

    # x**3, x**5 instance, must receive signals of expanded resolution to avoid range errors
    prod = product_n3_n5(x_p1, x_p3, x_p5, fxp=fxp_calc)

    @always_comb
    def in_logic():
        x_p1_lp.next = in_data.signed()

    @always_comb
    def x_1_upscale_logic():
        x_p1.next = x_p1_lp << upres_shift  # shift to initialize higher resolution fixbv

    @always_comb
    def intermediate_mul_logic():
        a_2.next = x_p3 * const_1_by_3  # x**3 * 1/3
        a_3.next = x_p5 * const_2_by_15  # x**5 * 2/15

    @always_comb
    def tanh_logic():
        # if not in [-N,N] approximation range, ss is limited to -1 or 1
        # otherwise ss = x - 1/3 * x**3 + 2/15 * x**5
        if x_p1_lp > const_max:
            ss.next = const_1_pos.signed()
        elif x_p1_lp < const_min:
            ss.next = const_1_neg.signed()
        else:
            ss.next = x_p1 + a_2 + a_3

    @always_comb
    def return_logic():
        out_data.next = ss.signed()[len(out_data) + downres_shift: downres_shift]

    return instances()


@block
def tanh_comb(in_fm, out_fm, size=4, fxp=None):
    """
    Performs combinational tanh() over single feature map

    Args:
        in_fm: Port, Concatenated list of signals comprising a complete feature map
        out_fm: Port, concatenated list of maximal values in feature map
        size: Parameter, number of signals in region
        fxp: Parameter, fixed point parameters definition

    Returns:
        AssertionErrors
    """
    assert fxp is not None, 'You have to provide fixed point definition'

    fm_sigs = [Signal(intbv(0)[fxp.width:]) for _ in range(size)]
    tanh_val = [Signal(intbv(0)[fxp.width:]) for _ in range(size)]
    tanh_out_fm = Signal(intbv()[fxp.width * size:])

    @always_comb
    def map_in():
        for ii in range(size):
            fm_sigs[ii].next = in_fm[((size - 1) - ii) * fxp.width + fxp.width:((size - 1) - ii) * fxp.width]

    tanh_inst = list()
    for i in range(size):
        tanh_inst.append(tanh_single_comb(in_data=fm_sigs[i], out_data=tanh_val[i], fxp=fxp))

    tanh_out_fm = ConcatSignal(*tanh_val)

    @always_comb
    def return_logic():
        out_fm.next = tanh_out_fm

    return instances()


@block
def tanh_seq(clk, reset, in_fm, out_fm, size=4, fxp=None):
    """
    Performs sequential tanh() over single feature map

    Args:
        clk: Port, clock signal
        reset: Port, reset signal
        in_fm: Port, concatenated list of signals comprising a complete feature map
        out_fm: Port, concatenated list of maximal values in feature map
        size: Parameter, number of signals in region
        fxp: Parameter, fixed point parameters definition

    Returns:
        AssertionErrors
    """
    assert fxp is not None, 'You have to provide fixed point definition'

    tanh_comb_sig = Signal(intbv()[len(out_fm):])
    tanh_comb_inst = tanh_comb(in_fm=in_fm, out_fm=tanh_comb_sig, size=size, fxp=fxp)

    @always_seq(clk.posedge, reset=reset)
    def tanh_proc():
        out_fm.next = tanh_comb_sig

    return instances()


@block
def sigmoid_single_comb(in_data, out_data, fxp=None):
    """
    Calculate sigmoid() for a single fixed point data signal
    sig(x) = (1 / 2.0) + (x / 4.0) - (x ** 3 / 48.0) + (x ** 5 / 480.0)
    :param in_data: Port, input signal
    :param out_data: Port, output signal
    :param fxp: Parameter, fixed point definition

        Returns:
        AssertionErrors
    """
    assert fxp is not None, 'You have to provide fixed point definition'

    N = 2.15  # sig approximation is correct in very limited range, outside it must be limited to 0, 1
    res_mul = 5
    rm = 'round'  # round mode

    fxp_calc = FixedDef(res_mul * fxp.m, res_mul * fxp.f)  # calculations fixed point format
    in_format = (fxp.width, fxp.m - 1)  # fixbv input format tuple
    calc_format = (fxp_calc.width, fxp_calc.m - 1)  # intermediate calculations fixbv format tuple

    upres_shift = int((fxp_calc.width - fxp.width) / 2)  # fixbv resolution increase bitshift
    downres_shift = upres_shift  # fixbv resolution decrease bitshift

    # function constant values
    const_1_by_2 = Signal(fixbv(value=(1.0 / 2), format=calc_format, round_mode=rm))
    const_1_by_4 = Signal(fixbv(value=(1.0 / 4), format=calc_format, round_mode=rm))
    const_1_by_48 = Signal(fixbv(value=(-1.0 / 48), format=calc_format, round_mode=rm))
    const_1_by_480 = Signal(fixbv(value=(1.0 / 480), format=calc_format, round_mode=rm))
    const_1_pos = Signal(fixbv(value=1.0, format=calc_format, round_mode=rm))

    const_min = Signal(fixbv(value=-N, format=in_format, round_mode=rm))
    const_max = Signal(fixbv(value=N, format=in_format, round_mode=rm))

    const_1_by_2.driven = True
    const_1_by_4.driven = True
    const_1_by_48.driven = True
    const_1_by_480.driven = True
    const_1_pos.driven = True
    const_min.driven = True
    const_max.driven = True

    x_p1_lp = Signal(fixbv(value=0, format=in_format, round_mode=rm))  # input precision in_data**1 value
    x_p1 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    x_p3 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    x_p5 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    a_2 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    a_3 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    a_4 = Signal(fixbv(value=0, format=calc_format, round_mode=rm))
    ss = Signal(fixbv(value=0, format=calc_format, round_mode=rm))  # expanded precision fixbv sig() value

    # x**3, x**5 instance, must receive signals of expanded resolution to avoid range errors
    prod = product_n3_n5(x_p1, x_p3, x_p5, fxp=fxp_calc)

    @always_comb
    def in_logic():
        x_p1_lp.next = in_data.signed()

    @always_comb
    def x_1_logic():
        x_p1.next = x_p1_lp << upres_shift  # shift to initialize higher resolution fixbv

    @always_comb
    def intermediate_mul_logic():
        a_2.next = x_p1 * const_1_by_4  # x * 1/4
        a_3.next = x_p3 * const_1_by_48  # x**3 * 1/48
        a_4.next = x_p5 * const_1_by_480  # x**5 * 1/480

    @always_comb
    def sigmoid_logic():
        # if not in [-N,N] approximation range, ss is limited to -1 or 0
        # otherwise ss = (1 / 2.0) + (x / 4.0) - (x ** 3 / 48.0) + (x ** 5 / 480.0)
        if x_p1_lp > const_max:
            ss.next = const_1_pos.signed()
        elif x_p1_lp < const_min:
            ss.next = 0
        else:
            ss.next = const_1_by_2 + a_2 + a_3 + a_4

    @always_comb
    def return_logic():
        out_data.next = ss[len(out_data) + downres_shift:downres_shift]

    return instances()


@block
def sigmoid_comb(in_fm, out_fm, size=4, fxp=None):
    """
    Performs combinational sigmoid() over single feature map

    Args:
        in_fm: Port, Concatenated list of signals comprising a complete feature map
        out_fm: Port, concatenated list of maximal values in feature map
        size: Parameter, number of signals in region
        fxp: Parameter, fixed point parameters definition

    Returns:
        AssertionErrors
    """
    assert fxp is not None, 'You have to provide fixed point definition'

    fm_sigs = [Signal(intbv(0)[fxp.width:]) for _ in range(size)]
    sig_val = [Signal(intbv(0)[fxp.width:]) for _ in range(size)]
    sig_out_fm = Signal(intbv()[fxp.width * size:])

    @always_comb
    def map_in():
        for ii in range(size):
            fm_sigs[ii].next = in_fm[((size - 1) - ii) * fxp.width + fxp.width:((size - 1) - ii) * fxp.width]

    sigmoid_inst = list()
    for i in range(size):
        sigmoid_inst.append(sigmoid_single_comb(in_data=fm_sigs[i], out_data=sig_val[i], fxp=fxp))

    sig_out_fm = ConcatSignal(*sig_val)

    @always_comb
    def return_logic():
        out_fm.next = sig_out_fm

    return instances()


@block
def sigmoid_seq(clk, reset, in_fm, out_fm, size=4, fxp=None):
    """
    Performs sequential sigmoid() over single feature map

    Args:
        clk: Port, clock signal
        reset: Port, reset signal
        in_fm: Port, concatenated list of signals comprising a complete feature map
        out_fm: Port, concatenated list of maximal values in feature map
        size: Parameter, number of signals in region
        fxp: Parameter, fixed point parameters definition

    Returns:
        AssertionErrors
    """
    assert fxp is not None, 'You have to provide fixed point definition'

    sigmoid_comb_sig = Signal(intbv()[len(out_fm):])
    sigmoid_comb_inst = sigmoid_comb(in_fm=in_fm, out_fm=sigmoid_comb_sig, size=size, fxp=fxp)

    @always_seq(clk.posedge, reset=reset)
    def sig_proc():
        out_fm.next = sigmoid_comb_sig

    return instances()


@block
def relu_single_comb(in_data, out_data):
    """
    Performs combinational ReLU over single data

    Args:
        in_data: Port, concatenated list of signals comprising a complete feature map
        out_data: Port, concatenated list of maximal values in feature map

    Returns:
        None

    """

    @always_comb
    def relu_logic():
        if in_data.signed() > 0:
            out_data.next = in_data
        else:
            out_data.next = 0

    return instances()


@block
def relu_comb(in_fm, out_fm, size=4, fxp=None):
    """
    Performs combinational ReLU over single feature map

    Args:
        in_fm: Port, Concatenated list of signals comprising a complete feature map
        out_fm: Port, concatenated list of maximal values in feature map
        size: Parameter, number of signals in region
        fxp: Parameter, fixed point parameters definition

    Returns:
        AssertionErrors

    """
    assert fxp is not None, 'You have to provide fixed point definition'

    fm_sigs = [Signal(intbv(0)[fxp.width:]) for _ in range(size)]
    relu_val = [Signal(intbv(0)[fxp.width:]) for _ in range(size)]
    relu_out_fm = Signal(intbv()[fxp.width * size:])

    @always_comb
    def map_in():
        for i in range(size):
            fm_sigs[i].next = in_fm[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width]

    relu_inst = list()
    for i in range(size):
        relu_inst.append(relu_single_comb(in_data=fm_sigs[i], out_data=relu_val[i]))

    relu_out_fm = ConcatSignal(*relu_val)

    @always_comb
    def return_logic():
        out_fm.next = relu_out_fm

    return instances()


@block
def relu_seq(clk, reset, in_fm, out_fm, size=4, fxp=None):
    """
    Performs sequential ReLU over single feature map

    Args:
        clk: Port, clock signal
        reset: Port, reset signal
        in_fm: Port, concatenated list of signals comprising a complete feature map
        out_fm: Port, concatenated list of maximal values in feature map
        bitwidth: Parameter, width of the single input data
        size: Parameter, number of signals in region
        fxp: Parameter, fixed point parameters definition

    Returns:
        AssertionErrors

    """
    relu_comb_sig = Signal(intbv()[len(out_fm):])
    relu_comb_inst = relu_comb(in_fm=in_fm, out_fm=relu_comb_sig, size=size, fxp=fxp)

    @always_seq(clk.posedge, reset=reset)
    def relu_proc():
        out_fm.next = relu_comb_sig

    return instances()
