import unittest
from math import exp
import numpy as np

import dl2hdl.layers.tests.layer_test_util as tutils

from myhdl import Signal, ResetSignal, ConcatSignal, StopSimulation
from myhdl import intbv, delay, block, instances, instance

from dl2hdl.layers.activation.activations import relu_seq, tanh_seq, tanh_single_comb, sigmoid_seq, sigmoid_single_comb
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from dl2hdl.basic.fixed_def import FixedDef

from config.config import config


def tanh(d):
    return (exp(d) - exp(-d)) / (exp(d) + exp(-d))


def tanh_approx(d, fxp=FixedDef(8, 8)):
    N = fxp.quantize_float(1.2)
    if d < -N:
        return -1.0
    if d > N:
        return 1.0
    return fxp.quantize_float(d - (1 / 3 * d ** 3) + (2 / 15 * d ** 5))


def sig(d):
    return exp(d) / (exp(d) + 1)


def sig_approx(d, fxp=FixedDef(8, 8)):
    N = fxp.quantize_float(2.15)
    if d < -N:
        return 0
    if d > N:
        return 1.0
    return fxp.quantize_float((1 / 2.0) + (d / 4.0) - (d ** 3 / 48.0) + (d ** 5 / 480.0))


class TestSig(unittest.TestCase):

    def testSigCombOverRange(self):

        assert_places = 1
        fxp = FixedDef(8, 8)
        # inputs
        test_vectors = [fxp.quantize_float(i/10.0) for i in range(-50, 50, 1)]
        # expected outputs
        precise = [fxp.quantize_float(sig(t)) for t in test_vectors]
        expected = [fxp.quantize_float(sig_approx(t, fxp)) for t in test_vectors]

        @block
        def tb():
            in_sig = Signal(intbv(0)[fxp.width:])
            out_sig = Signal(intbv(0)[fxp.width:])
            inputs = [Signal(intbv(fxp.to_fixed(tvec))[fxp.width:]) for tvec in test_vectors]
            dut_inst = sigmoid_single_comb(in_sig, out_sig, fxp)

            @instance
            def stimulus():
                for i, e, p in zip(inputs, expected, precise):
                    in_sig.next = i
                    yield delay(100)
                    ii = fxp.to_float(in_sig.signed())
                    oo = fxp.to_float(out_sig.signed())
                    print("%.8f -> %.8f exp %.8f diff_lsb %d (non-approx %.8f)" % (
                          ii, oo, e, tutils.diff_to_lsb(abs(e - oo), fxp=fxp), p))
                    assert tutils.diff_to_lsb(abs(e - oo), fxp=fxp) <= assert_places
                    # self.assertAlmostEqual(e, oo, places=assert_places)

            return instances()

        tb_inst = tb()
        tb_inst.config_sim(backend='myhld', trace=False)
        tb_inst.run_sim(quiet=1)

    def testSigSeqRandomData(self):
        """
        Random data test for sig_seq
        """

        def sig_flat(data):
            tmp = np.empty_like(data)
            for i in range(len(data)):
                tmp[i] = sig_approx(data[i], fxp)
            return tmp

        assert_places = 1
        fm_size = 30
        fxp = FixedDef(8, 8)
        # inputs
        test_vectors = []
        test_vector_1 = (np.random.rand(fm_size) - 0.5) * 5
        test_vector_1 = fxp.quantize_float(test_vector_1)
        test_vectors.append(test_vector_1)
        # expected outputs
        expected = [fxp.quantize_float(sig_flat(t).tolist()) for t in test_vectors]

        @block
        def tb():
            """
            Test instance for relu_seq
            """
            # signal definitions
            in_sig = Signal(intbv(0)[fxp.width * fm_size:])
            out_sig = Signal(intbv(0)[fxp.width * fm_size:])
            reset = ResetSignal(0, active=0, isasync=False)
            clk = Signal(bool(0))

            # test signals
            inputs = []
            for tvec in test_vectors:
                in_fm = [Signal(intbv(fxp.to_fixed(tvec[i]))[fxp.width:]) for i in range(int(fm_size))]
                for i in range(len(in_fm)):
                    in_fm[i].driven = True
                inputs.append(ConcatSignal(*in_fm))

            # instances
            clk_gen = clk_stim(clk, period=10)
            rst_gen = aresetn_stim(reset, delay_time=42)
            dut = sigmoid_seq(clk, reset, in_sig, out_sig, size=fm_size, fxp=fxp)
            # convert DUT
            dut.convert(hdl='VHDL', path=config.get_path_out_vhdl(), initial_values=True)

            @instance
            def stimulus():
                # wait for reset
                yield reset.posedge

                for i, e in zip(inputs, expected):
                    yield clk.negedge
                    in_sig.next = i  # set input
                    yield clk.posedge
                    yield clk.posedge
                    outputs = tutils.concatenated_signal_to_list(out_sig, size=fm_size, fxp=fxp)

                    # for se, so in zip(tutils.concatenated_signal_to_list(out_sig, size=fm_size, fxp=fxp), e):
                    #     self.assertAlmostEqual(se, so, places=assert_places, msg='SIGMOID NO work correctly')

                    tutils.assert_list_diff_below_n_lsb(outputs, e, assert_places, fxp=fxp,
                                                        verbose=True, msg_fail='SIGMOID NO work correctly')
                print("SIGMOID test passed")
                raise StopSimulation

            return instances()

        tb_inst = tb()
        tb_inst.config_sim(backend='myhld', trace=False)
        tb_inst.run_sim(quiet=1)


class TestTanh(unittest.TestCase):

    def testTanhCombOverRange(self):
        assert_places = 1
        fxp = FixedDef(8, 8)
        # inputs
        test_vectors = [fxp.quantize_float(i/10.0) for i in range(-50, 50, 1)]
        # expected outputs
        precise = [fxp.quantize_float(tanh(t)) for t in test_vectors]
        expected = [fxp.quantize_float(tanh_approx(t, fxp)) for t in test_vectors]

        @block
        def tb():
            in_sig = Signal(intbv(0)[fxp.width:])
            out_sig = Signal(intbv(0)[fxp.width:])
            inputs = [Signal(intbv(fxp.to_fixed(tvec))[fxp.width:]) for tvec in test_vectors]
            dut_inst = tanh_single_comb(in_sig, out_sig, fxp)

            @instance
            def stimulus():
                for i, e, p in zip(inputs, expected, precise):
                    in_sig.next = i
                    yield delay(10)
                    ii = fxp.to_float(in_sig.signed())
                    oo = fxp.to_float(out_sig.signed())
                    print("%.8f -> %.8f exp %.8f diff_lsb %d (non-approx %.8f)" % (
                          ii, oo, e, tutils.diff_to_lsb(abs(e - oo), fxp=fxp), p))
                    assert tutils.diff_to_lsb(abs(e - oo), fxp=fxp) <= assert_places
                    # self.assertAlmostEqual(e, oo, places=assert_places)

            return instances()

        tb_inst = tb()
        tb_inst.config_sim(backend='myhld', trace=False)
        tb_inst.run_sim(quiet=1)

    def testTanhSeqRandomData(self):
        """
        Random data test for tanh_seq
        """
        def tanh_flat(data):
            tmp = np.empty_like(data)
            for i in range(len(data)):
                tmp[i] = tanh_approx(data[i], fxp)
            return tmp

        assert_places = 1
        fm_size = 30
        fxp = FixedDef(8, 8)
        # inputs
        test_vectors = []
        test_vector_1 = (np.random.rand(fm_size) - 0.5) * 5
        test_vector_1 = fxp.quantize_float(test_vector_1)
        test_vectors.append(test_vector_1)
        # expected outputs
        expected = [fxp.quantize_float(tanh_flat(t).tolist()) for t in test_vectors]

        @block
        def tb():
            """
            Test instance for relu_seq
            """
            # signal definitions
            in_sig = Signal(intbv(0)[fxp.width * fm_size:])
            out_sig = Signal(intbv(0)[fxp.width * fm_size:])
            reset = ResetSignal(0, active=0, isasync=False)
            clk = Signal(bool(0))

            # test signals
            inputs = []
            for tvec in test_vectors:
                in_fm = [Signal(intbv(fxp.to_fixed(tvec[i]))[fxp.width:]) for i in range(int(fm_size))]
                for i in range(len(in_fm)):
                    in_fm[i].driven = True
                inputs.append(ConcatSignal(*in_fm))

            # instances
            clk_gen = clk_stim(clk, period=10)
            rst_gen = aresetn_stim(reset, delay_time=42)
            dut = tanh_seq(clk, reset, in_sig, out_sig, size=fm_size, fxp=fxp)
            # convert DUT
            dut.convert(hdl='VHDL', path=config.get_path_out_vhdl(), initial_values=True)

            @instance
            def stimulus():
                # wait for reset
                yield reset.posedge

                for i, e in zip(inputs, expected):
                    yield clk.negedge
                    in_sig.next = i  # set input
                    yield clk.posedge
                    yield clk.posedge
                    outputs = tutils.concatenated_signal_to_list(out_sig, size=fm_size, fxp=fxp)

                    # for se, so in zip(tutils.concatenated_signal_to_list(out_sig, size=fm_size, fxp=fxp), e):
                    #     self.assertAlmostEqual(se, so, places=assert_places, msg='TANH NO work correctly')

                    tutils.assert_list_diff_below_n_lsb(outputs, e, assert_places, fxp=fxp,
                                                        verbose=True, msg_fail='TANH NO work correctly')
                print("TANH test passed")
                raise StopSimulation

            return instances()

        tb_inst = tb()
        tb_inst.config_sim(backend='myhld', trace=False)
        tb_inst.run_sim(quiet=1)


class TestReLU(unittest.TestCase):

    def testBlockRandomData(self):
        """
        Random data test for relu_seq
        :return:
        """

        # TODO: use external relu implementation, if possible from pytorch
        def relu_flat(data):
            tmp = np.empty_like(data)
            for i in range(len(data)):
                if data[i] >= 0:
                    tmp[i] = data[i]
                else:
                    tmp[i] = 0
            return tmp

        fm_size = 30
        fxp = FixedDef(8, 8)
        # inputs
        test_vectors = []
        test_vector_1 = (np.random.rand(fm_size) - 0.5) * 20
        test_vector_1 = fxp.quantize_float(test_vector_1)
        test_vectors.append(test_vector_1)
        # expected outputs
        expected = [relu_flat(t).tolist() for t in test_vectors]

        @block
        def tb():
            """
            Test instance for relu_seq
            """

            # stimulation timing
            delay_time = 42
            period = 10

            # signal definitions
            in_sig = Signal(intbv(0)[fxp.width * fm_size:])
            out_sig = Signal(intbv(0)[fxp.width * fm_size:])
            reset = ResetSignal(0, active=0, isasync=False)
            clk = Signal(bool(0))

            # test signals
            inputs = []
            for tvec in test_vectors:
                in_fm = [Signal(intbv(fxp.to_fixed(tvec[i]))[fxp.width:]) for i in range(int(fm_size))]
                for i in range(len(in_fm)):
                    in_fm[i].driven = True
                inputs.append(ConcatSignal(*in_fm))

            # instances
            clk_gen = clk_stim(clk, period=period)
            rst_gen = aresetn_stim(reset, delay_time=delay_time)
            dut = relu_seq(clk, reset, in_sig, out_sig, size=fm_size, fxp=fxp)
            # convert DUT
            dut.convert(hdl='VHDL', path=config.get_path_out_vhdl(), initial_values=True)

            @instance
            def stimulus():
                # wait for reset
                yield reset.posedge

                for i, e in zip(inputs, expected):
                    yield clk.negedge
                    in_sig.next = i  # set input
                    yield clk.posedge
                    yield clk.posedge
                    self.assertListEqual(tutils.concatenated_signal_to_list(out_sig, size=fm_size, fxp=fxp), e,
                                         'ReLU NO work correctly')
                print("ReLU test passed")
                raise StopSimulation

            return instances()

        tb_inst = tb()
        tb_inst.config_sim(backend='myhld', trace=False)
        tb_inst.run_sim(quiet=1)


if __name__ == '__main__':
    unittest.main(verbosity=2)
