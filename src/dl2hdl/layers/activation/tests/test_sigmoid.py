from dl2hdl.tests.hdl_test import HdlTest
from myhdl import block, Signal, instances, ResetSignal, intbv, instance, delay, StopSimulation
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from dl2hdl.layers.activation.sigmoid import sigmoid
from dl2hdl.layers.analysis.sigmoid_test import sigmoid_series_vec, sigmoid_vec
from dl2hdl.basic.interface_map import receiver, transmitter
from dl2hdl.axi.axis import Axis
import numpy as np


class TestSigmoid(HdlTest):

    @staticmethod
    @block
    def sigmoid_net(clk, reset, in_data, out_data, fxp, size):

        in_fm = Axis(intbv()[fxp.width * size:])
        out_fm = Axis(intbv()[fxp.width * size:])

        rxd = receiver(clk, reset, in_data, in_fm, fxp)
        sigm = sigmoid(clk, reset, in_fm, out_fm, fxp)
        txd = transmitter(clk, reset, out_fm, out_data, fxp)

        return instances()

    def _single_run_seq(self, test_data, fxp, conv=False):
        @block
        def sigmoid_tb(data, out_data, conv=False, fxp=None):
            data_width = 32
            in_fm = Axis(intbv()[data_width:])
            out_fm = Axis(intbv()[data_width:])
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)
            rst_gen = aresetn_stim(reset, delay_time=24)

            stim_write = Axis.write(clk, reset, in_fm, data, fxp, verbose=True)

            uut = self.sigmoid_net(clk, reset, in_fm, out_fm, fxp, size=len(data[0]))

            pass

            @instance
            def stim_read():
                yield reset.posedge
                yield delay(24)
                yield clk.negedge
                out_fm.tready.next = 1
                for _ in range(len(data)):
                    out_row = []
                    while True:
                        yield clk.negedge
                        if out_fm.tvalid == 1:
                            out_row.append(out_fm.tdata.signed())
                            if out_fm.tlast == 1:
                                break
                    out_data.append(out_row)

                for i in range(10):
                    yield clk.negedge

                if conv:
                    uut.convert(hdl='VHDL', path=self.vhdl_output_path)

                raise StopSimulation()

            return instances()

        out_data = []
        tb = sigmoid_tb(data=test_data, out_data=out_data, conv=conv, fxp=fxp)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return out_data

    def test_sigmoid(self):
        fxp = FixedDef(4, 6)
        seq_len = 1
        in_f = 8
        min = -5
        max = 5
        x = np.linspace(min, max, num=in_f, endpoint=False)
        x = x.reshape(1, -1)
        y_true = sigmoid_vec(x)
        y_series = sigmoid_series_vec(x, n=4)
        out_data = self._single_run_seq(x, conv=True, fxp=fxp)
        out_data = [[fxp.to_float(y) for y in x] for x in out_data]
        import matplotlib.pyplot as plt
        ax = plt.subplot(111)
        ax.plot(x[0], y_true[0], 'r', label='true')
        ax.plot(x[0], y_series[0], 'g', label='series')
        ax.plot(x[0], out_data[0], 'b', label='hw')
        ax.legend()
        plt.show()
