from dl2hdl.tests.hdl_test import HdlTest
from unittest import skip
from myhdl import block, Signal, instances, ResetSignal, intbv, instance, delay, StopSimulation
from dl2hdl.basic.fixed_def import FixedDef
from config.config import config
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from dl2hdl.layers.activation.tanh import tanh
from dl2hdl.layers.analysis.tanh_series_test import tanh_series_vec
from dl2hdl.basic.interface_map import receiver, transmitter
from dl2hdl.axi.axis import Axis
import os
import numpy as np


class TestTanh(HdlTest):

    @staticmethod
    @block
    def tanh_net(clk, reset, in_data, out_data, fxp, size, implementation):

        in_fm = Axis(intbv()[fxp.width * size:])
        out_fm = Axis(intbv()[fxp.width * size:])

        rxd = receiver(clk, reset, in_data, in_fm, fxp)
        sigm = tanh(clk, reset, in_fm, out_fm, implementation=implementation, fxp=fxp)
        txd = transmitter(clk, reset, out_fm, out_data, fxp)

        return instances()

    def _single_run_seq(self, test_data, fxp, conv=False, implementation='series'):
        @block
        def tanh_tb(data, out_data, conv=False, fxp=None, implementation='series'):
            data_width = 32
            in_fm = Axis(intbv()[data_width:])
            out_fm = Axis(intbv()[data_width:])
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)
            rst_gen = aresetn_stim(reset, delay_time=24)

            stim_write = Axis.write(clk, reset, in_fm, data, fxp, verbose=True)

            uut = self.tanh_net(clk, reset, in_fm, out_fm, fxp, size=len(data[0]), implementation=implementation)

            pass

            @instance
            def stim_read():
                yield reset.posedge
                yield delay(24)
                yield clk.negedge
                out_fm.tready.next = 1
                for _ in range(len(data)):
                    out_row = []
                    while True:
                        yield clk.negedge
                        if out_fm.tvalid == 1:
                            out_row.append(out_fm.tdata.signed())
                            if out_fm.tlast == 1:
                                break
                    out_data.append(out_row)

                for i in range(10):
                    yield clk.negedge

                if conv:
                    uut.convert(hdl='VHDL', path=self.vhdl_output_path)

                raise StopSimulation()

            return instances()

        out_data = []
        tb = tanh_tb(data=test_data, out_data=out_data, conv=conv, fxp=fxp, implementation=implementation)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return out_data

    # def test_tanh_01(self):
        frac_width = 6
        fxp = FixedDef(4, frac_width)
        seq_len = 1
        min = -2
        max = 2
        # in_f = (max - min) * 2 ** frac_width
        in_f = 256
        x = np.linspace(min, max, num=in_f, endpoint=False)
        x = x.reshape(1, -1)
        y_true = np.tanh(x)
        y_series = tanh_series_vec(x, n=3)
        out_data_maclaurin = self._single_run_seq(x, conv=True, fxp=fxp, implementation='series')
        out_data_maclaurin = [[fxp.to_float(y) for y in x] for x in out_data_maclaurin]
        out_data_linear = self._single_run_seq(x, conv=True, fxp=fxp, implementation='linear')
        out_data_linear = [[fxp.to_float(y) for y in x] for x in out_data_linear]

        error_maclaurin = np.abs(np.asarray(out_data_maclaurin) - y_true)
        error_linear = np.abs(np.asarray(out_data_linear) - y_true)
        import matplotlib.pyplot as plt
        plt.figure(1)
        ax = plt.subplot(111)
        ax.plot(x[0], error_maclaurin[0], label='series')
        ax.plot(x[0], error_linear[0], label='linear')
        ax.set_yscale('log')
        ax.axis([0, 1.5, 10e-6, 1])

        def LSB_0_5(x, n):
            return 0.5 / (2 ** n)

        min_frac = np.vectorize(LSB_0_5)

        for i in range(1, 16):
            ax.plot(x[0], min_frac(x[0], i), 'r--')
            plt.text(0, LSB_0_5(0, i), str(i) + 'bit')

        plt.xlabel('Absolute value range')
        plt.ylabel('Error')
        ax.legend(loc='lower right')

        plt.figure(2)
        ax2 = plt.subplot(111)
        ax2.plot(x[0], y_true[0], 'r', label='true')
        ax2.plot(x[0], out_data_maclaurin[0], 'g', label='series')
        ax2.plot(x[0], out_data_linear[0], 'b', label='linear')
        ax2.legend()

        plt.show()

    def test_tanh_02_conv(self):
        multiplier=32
        int_width = 3
        frac_width = 29
        fxp = FixedDef(int_width, frac_width)
        in_fm = Axis(intbv()[fxp.width*multiplier:])
        out_fm = Axis(intbv()[fxp.width*multiplier:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)

        tanh_series = tanh(clk, reset, in_fm, out_fm, implementation='series', fxp=fxp)
        tanh_series.convert(hdl='VHDL', path=self.vhdl_output_path, name='tanh_series')
        tanh_linear = tanh(clk, reset, in_fm, out_fm, implementation='linear', fxp=fxp)
        tanh_linear.convert(hdl='VHDL', path=self.vhdl_output_path, name='tanh_linear')
