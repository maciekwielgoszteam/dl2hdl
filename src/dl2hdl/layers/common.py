from myhdl import block, Signal, always_comb, ConcatSignal

from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.fixed_point import fixbv


@block
def mul_elem_wise(in_data_a, in_data_b, out_data, fxp=None):
    """
    Performs element wise multiplication
    :param in_data_a: input array a
    :param in_data_b: input array b
    :param out_data: output array
    :param fxp: fixed point definition
    :return: None
    :raises: Assertions errors
    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'

    size = len(in_data_a.tdata) // fxp.width

    calc_elems_a = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    calc_elems_b = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    calc_results = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]

    @always_comb
    def map_in():
        for i in range(size):
            calc_elems_a[i].next = in_data_a.tdata[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width].signed()
            calc_elems_b[i].next = in_data_b.tdata[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width].signed()

    @always_comb
    def mul_logic():
        for i in range(size):
            calc_results[i].next = calc_elems_a[i] * calc_elems_b[i]

    @always_comb
    def axi_ctrl():
        out_data.tdata.next = ConcatSignal(*calc_results)
        in_data_a.tready = out_data.tready
        in_data_b.tready = out_data.tready
        if in_data_a.tvalid == 1 and in_data_b.tvalid == 1:
            out_data.tvalid.next = 1
            if in_data_a.tlast == 1 or in_data_b.tlast == 1:
                out_data.tlast.next = 1
            else:
                out_data.tlast.next = 0
        else:
            out_data.tvalid.next = 0

@block
def add_elem_wise(in_data_a, in_data_b, out_data, fxp=None):
    """
    Performs element wise addition
    :param in_data_a: input array a
    :param in_data_b: input array b
    :param out_data: output array
    :param fxp: fixed point definition
    :return: None
    :raises: Assertions errors
    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'

    size = len(in_data_a.tdata) // fxp.width

    calc_elems_a = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    calc_elems_b = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]
    calc_results = [Signal(fixbv(value=0, format=(fxp.width, fxp.m - 1))) for _ in range(size)]

    @always_comb
    def map_in():
        for i in range(size):
            calc_elems_a[i].next = in_data_a.tdata[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width].signed()
            calc_elems_b[i].next = in_data_b.tdata[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width].signed()

    @always_comb
    def mul_logic():
        for i in range(size):
            calc_results[i].next = calc_elems_a[i] + calc_elems_b[i]

    @always_comb
    def axi_ctrl():
        out_data.tdata.next = ConcatSignal(*calc_results)
        in_data_a.tready = out_data.tready
        in_data_b.tready = out_data.tready
        if in_data_a.tvalid == 1 and in_data_b.tvalid == 1:
            out_data.tvalid.next = 1
            if in_data_a.tlast == 1 or in_data_b.tlast == 1:
                out_data.tlast.next = 1
            else:
                out_data.tlast.next = 0
        else:
            out_data.tvalid.next = 0
