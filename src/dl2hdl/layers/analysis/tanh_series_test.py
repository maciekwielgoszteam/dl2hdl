import numpy as np
import matplotlib.pyplot as plt

'''
http://mathworld.wolfram.com/HyperbolicTangent.html
'''


def tnah_linear_v2_single(x):
    if x <= -1.5:
        return -1.
    elif -1.5 < x <= -0.75:
        return x / 2 - 0.25
    elif -0.75 < x <= -0.25:
        return 3 * x / 4 - 0.0625
    elif -0.25 < x <= 0.25:
        return x
    elif 0.25 < x <= 0.75:
        return 3 * x / 4 + 0.078125
    elif 0.75 < x <= 1.5:
        return x / 2 + 0.25
    else:
        return 1


tanh_linear_v2_vec = np.vectorize(tnah_linear_v2_single)


def tnah_linear_single(x):
    if x <= -1.5:
        return -1.
    elif -1.5 < x <= -0.5:
        return x / 2 - 0.25
    elif -0.5 < x <= 0.5:
        return x
    elif 0.5 < x <= 1.5:
        return x / 2 + 0.25
    else:
        return 1


tanh_linear_vec = np.vectorize(tnah_linear_single)


def tanh_series_single(x, n=1):
    series_coeff = [1.0, -1. / 3., 2. / 15., -17. / 315., 62. / 2835.]
    assert 0 <= n <= len(series_coeff)
    tmp = 0.
    for i in range(n):
        tmp += series_coeff[i] * (x ** (2 * i + 1))
    return tmp


def tanh_series_single_limited(x, n=1, monotonicity_step=0.001):
    tmp = tanh_series_single(x, n)
    if x < 0.:
        if abs(tmp - np.tanh(x)) > abs(-1. - np.tanh(x)):
            tmp = -1.
        if tanh_series_single(x + monotonicity_step, n) < tmp:
            tmp = -1.
    else:
        if abs(tmp - np.tanh(x)) > abs(1. - np.tanh(x)):
            tmp = 1.
        if tanh_series_single(x + monotonicity_step, n) < tmp:
            tmp = 1.
    if tmp < -1.:
        return -1.
    elif tmp > 1.:
        return 1.
    else:
        return tmp


tanh_series_vec = np.vectorize(tanh_series_single_limited)


def LSB_0_5(x, n):
    return 1 / (2 ** n)


min_frac = np.vectorize(LSB_0_5)

if __name__ == '__main__':
    step = 0.001
    limit = 3

    x = np.arange(-limit, limit, step)
    y_correct = np.tanh(x)
    y_test = []
    for n in range(0, 6):
        y = tanh_series_vec(x, n)
        y_test.append(y)
    y_test.append(tanh_linear_vec(x))
    # y_test.append(tanh_linear_v2_vec(x))

    error = []
    for y in y_test:
        error.append(np.abs(y - y_correct))
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.set_yscale('log')
    ax.axis([0, 1.5, 10e-6, 1])
    for i, e in enumerate(error):
        if i == 6:
            ax.plot(x, e, label='linear')
            print(f'for linear max error is {max(e)} for x = {abs(x[np.argmax(e)])}')
        # elif i == 7:
        #     ax.plot(x, e, label='linear v2')
        #     print(f'for linear v2 max error is {max(e)} for x = {abs(x[np.argmax(e)])}')
        else:
            ax.plot(x, e, label=i)
            print(f'for {i+1} max error is {max(e)} for x = {abs(x[np.argmax(e)])}')

    for i in range(1, 16):
        ax.plot(x, min_frac(x, i), 'r--')
        plt.text(0, LSB_0_5(0, i), str(i) + 'bit')

    plt.xlabel('Absolute value range')
    plt.ylabel('Error')
    ax.legend(loc='lower right')
    ax.yaxis.grid(True, which='both')
    # fig2 = plt.figure()
    # ax2 = plt.subplot(111)
    # for i, e in enumerate(y_test):
    #     ax2.plot(x, e, label=i)
    # ax2.plot(x, y_correct, label='true')
    # ax2.legend()
    plt.show()
