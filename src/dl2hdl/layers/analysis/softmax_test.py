import numpy as np
import matplotlib.pyplot as plt


def my_exp(x, n=3):
    result = 0.0
    for i in range(n + 1):
        result += (x ** i) / np.math.factorial(i)
    if result < 0.0:
        result = 0.0
    return result


my_exp_vec = np.vectorize(my_exp)


def true_softmax(x):
    exp_x = np.exp(x)
    sum_exp_x = sum(exp_x)
    return [x / sum_exp_x for x in exp_x]

def my_softmax(x, n=3):
    exp_x = my_exp_vec(x, n)
    sum_exp_x = sum(exp_x)
    return [x / sum_exp_x for x in exp_x]

def LSB_0_5(x, n):
    return 0.5 / (2 ** n)


min_frac = np.vectorize(LSB_0_5)

if __name__ == '__main__':
    step = 0.25
    limit = 2

    x = np.arange(-limit, limit, step)
    y_correct = true_softmax(x)
    y_test = []
    # for n in range(1, 7):
    #     y_test.append(my_exp_vec(x, n))
    # y_test.append(my_softmax(x, 1))
    y_test.append(my_softmax(x, 3))
    y_test.append(my_softmax(x, 5))
    # y_test.append(my_exp_vec(x, 7))
    # y_test.append(my_exp_vec(x, 9))
    # y_test.append(my_exp_vec(x, 11))
    # y_test.append(my_exp_vec(x, 13))


    fig2 = plt.figure()
    ax2 = plt.subplot(111)
    for i, e in enumerate(y_test):
        ax2.plot(x, e, label=i)
    ax2.plot(x, y_correct, label='true')
    ax2.legend()
    plt.show()
