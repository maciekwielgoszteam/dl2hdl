import numpy as np
import matplotlib.pyplot as plt

'''
http://mathworld.wolfram.com/SigmoidFunction.html
'''


def relu(x, limit=None):
    try:
        if x > limit:
            return limit
    except TypeError:
        pass
    if x < 0.:
        return 0.
    else:
        return x


relu_vec = np.vectorize(relu)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def sigmoid_series_single(x, n=1):
    series_coeff = [1.0 / 2.0, 1.0 / 4.0, -1.0 / 48.0, 1.0 / 480.0, -17.0 / 80640, 31.0 / 1451520]
    assert 1 <= n <= len(series_coeff) + 1
    tmp = series_coeff[0]
    for i in range(1, n):
        tmp += series_coeff[i] * (x ** (2 * i - 1))
    return tmp


def hard_sigmoid(x, scale_factor=0.2):
    return max(0., min(1., x * scale_factor + 0.5))


hard_sigmoid_vec = np.vectorize(hard_sigmoid)


def sigmoid_series_single_limited(x, n=1, monotonicity_step=0.001):
    tmp = sigmoid_series_single(x, n)
    if x < 0.:
        if abs(sigmoid(x) - tmp) > sigmoid(x):
            return 0.
        if sigmoid_series_single(x + monotonicity_step, n) < tmp:
            # print(f'for n = {n} monotonicity is lost for {x}')
            return 0.
    if x >= 0.:
        if abs(sigmoid(x) - tmp) > abs(sigmoid(x) - 1.):
            return 1.
        if sigmoid_series_single(x - monotonicity_step, n) > tmp:
            # print(f'for n = {n} monotonicity is lost for {x}')
            return 1.
    return tmp


sigmoid_series_vec = np.vectorize(sigmoid_series_single_limited)
sigmoid_vec = np.vectorize(sigmoid)


def LSB_0_5(x, n):
    return 0.5 / (2 ** n)


min_frac = np.vectorize(LSB_0_5)

if __name__ == '__main__':
    step = 0.01
    limit = 10

    x = np.arange(-limit, limit, step)
    y_correct = sigmoid(x)
    y_test = []
    # y_test.append(relu_vec(x, limit=6))
    # for n in range(1, 7):
    #     y = sigmoid_series_vec(x, n)
    #     y_test.append(y)
    y_test.append(sigmoid_series_vec(x, 4))
    y_test.append(hard_sigmoid_vec(x, 0.125))
    y_test.append(hard_sigmoid_vec(x, 0.2))
    y_test.append(hard_sigmoid_vec(x, 0.25))

    error = []
    for y in y_test:
        error.append(np.abs(y - y_correct))
    fig = plt.figure()
    ax = plt.subplot(111)
    for i, e in enumerate(error):
        ax.plot(x, e, label=i)
        print(f'for {i+1} max error is {max(e)} for x = {abs(x[np.argmax(e)])}')

    for i in range(1, 16):
        ax.plot(x, min_frac(x, i), 'r--')
        plt.text(0, LSB_0_5(0, i), str(i) + 'bit')
    ax.legend()
    fig2 = plt.figure()
    ax2 = plt.subplot(111)
    for i, e in enumerate(y_test):
        ax2.plot(x, e, label=i)
    ax2.plot(x, y_correct, label='true')
    ax2.legend()
    plt.show()
