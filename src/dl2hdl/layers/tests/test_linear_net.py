from unittest import skip
from dl2hdl.tests.hdl_test import HdlTest
import torch.nn as nn
from torch.utils.data import DataLoader
import torch
import os
import numpy as np
import warnings
import time

from myhdl import block, Signal, instances, ResetSignal, intbv, instance, delay, StopSimulation
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.axi.axis import Axis
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from dl2hdl.layers.linear import linear, LinearCoeffs
from dl2hdl.layers.tests.torch_dataset import RandomBin
from dl2hdl.basic.interface_map import receiver, transmitter
from dl2hdl.basic.cpp_if import torch_data_set_to_cpp_string, numpy_ndaray_to_cpp_str
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.model import MyHdlModel


class TestLinearBlocking(HdlTest):

    class _TestModel(nn.Module):

        def __init__(self, in_features, sizes):
            super().__init__()
            self.classifier = nn.Sequential()
            self.classifier.add_module('linear0', nn.Linear(in_features=in_features, out_features=sizes[0]))
            for i in range(1, len(sizes)):
                self.classifier.add_module(f'linear{i}', nn.Linear(in_features=sizes[i - 1], out_features=sizes[i]))

        def forward(self, x):
            x = self.classifier(x)
            return x

    @staticmethod
    def infer_net(model, data_set, device=torch.device('cpu')):
        test_loader = DataLoader(data_set, batch_size=1, shuffle=False)
        model.eval()
        out_true = []
        with torch.no_grad():
            for data, target in test_loader:
                data, target = data.to(device), target.to(device)
                out_true.append(model(data))
        return out_true

    def _single_run(self, test_data, model, conv=False, fxp=None):
        @block
        def linear_net_tb(data, out_data, model, conv=False, fxp=None):
            axis_width = 32
            in_fm = Axis(intbv()[axis_width:])
            out_fm = Axis(intbv()[axis_width:])
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)
            rst_gen = aresetn_stim(reset, delay_time=24)

            stim_write = Axis.write(clk, reset, in_fm, data.numpy(), fxp, verbose=True)

            hdl_model = MyHdlModel(model, fxp)
            uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)

            pass

            @instance
            def stim_read():
                yield reset.posedge
                yield delay(24)
                yield clk.negedge
                out_fm.tready.next = 1
                for _ in range(len(data)):
                    out_row = []
                    while True:
                        yield clk.negedge
                        if out_fm.tvalid == 1:
                            out_row.append(out_fm.tdata.signed())
                            if out_fm.tlast == 1:
                                break
                    out_data.append(out_row)

                for i in range(10):
                    yield clk.negedge

                # Print output for testbench
                # for sample in out_data:
                #     print(sample)

                if conv:
                    uut.convert(hdl='VHDL', path=self.vhdl_output_path)

                raise StopSimulation()

            return instances()

        out_data = []
        tb = linear_net_tb(data=test_data, out_data=out_data, model=model, conv=conv, fxp=fxp)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return out_data

    @skip
    def test_linear_net_sim(self):
        fxp = FixedDef(8, 8)
        seq_len = 6
        for in_f in range(2, 6):
            for hid_f in range(2, 6):
                for out_f in range(2, 6):
                    with self.subTest(in_f=in_f, hid_f=hid_f, out_f=out_f):
                        print(f'testing {in_f}-{hid_f}-{out_f}')
                        test_set = RandomBin(shape=(in_f,), num_of_samples=seq_len)
                        model = self._TestModel(in_f, (hid_f, out_f))
                        out_true = self.infer_net(model, test_set)
                        out_true = [x.tolist()[0] for x in out_true]
                        out_data = self._single_run(test_set.data, model, conv=False, fxp=fxp)
                        out_data = [[fxp.to_float(y) for y in x] for x in out_data]
                        decimal = 1
                        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
                        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    def test_linear_bigger_sim(self):
        fxp = FixedDef(16, 16)
        seq_len = 1
        in_f = 2
        hid_f = 8
        out_f = 2
        print(f'testing {in_f}-{hid_f}-{out_f}')
        test_set = RandomBin(shape=(in_f,), num_of_samples=seq_len, seed=7)
        model = self._TestModel(in_f, (hid_f, out_f))
        out_true = self.infer_net(model, test_set)
        out_true = [x.tolist()[0] for x in out_true]
        out_data = self._single_run(test_set.data, model, conv=True, fxp=fxp)
        out_data = [[fxp.to_float(y) for y in x] for x in out_data]
        decimal = 5
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    @skip
    def test_linear_for_hw(self):
        new = True
        fxp = FixedDef(8, 8)
        seq_len = 2
        in_f = 4
        out_f = 2
        print(f'testing {in_f}-{out_f}')
        test_set = RandomBin(shape=(in_f,), num_of_samples=seq_len, seed=2)
        model = self._TestModel(in_f, (out_f,))
        model_path = os.path.join(self.model_path, 'test_linear_net.pt')
        if new:
            torch.save(model.state_dict(), model_path)
        else:
            model.load_state_dict(torch.load(model_path))
        out_true = self.infer_net(model, test_set)
        out_true = [x.tolist()[0] for x in out_true]
        out_true_np = np.asarray(out_true)
        out_data = self._single_run(test_set.data, model, conv=True, fxp=fxp)
        out_data_np = np.asarray([[int(y) for y in x] for x in out_data])
        out_data = [[fxp.to_float(y) for y in x] for x in out_data]

        path = os.path.join(self.data_path, 'test_data')
        file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=fxp)
        file_str += numpy_ndaray_to_cpp_str(out_data_np, 'outputs')
        file_str += numpy_ndaray_to_cpp_str(fxp.to_fixed(out_true_np), 'outputs_true')
        os.makedirs(path, exist_ok=True)
        f = open(os.path.join(path, 'test_data.h'), 'w')
        print(file_str, file=f)
        f.close()

        numpy_array_to_txt_vec(fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(out_data_np, path, 'test_output')

        decimal = 2
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    @skip
    def test_linear_for_hw_one_by_one(self):
        new = True
        fxp = FixedDef(2, 6)
        seq_len = 4
        in_f = 32
        hid_f = 32
        out_f = 2
        print(f'testing {in_f}-{hid_f}-{out_f}')
        test_set = RandomBin(shape=(in_f,), num_of_samples=seq_len, seed=2)
        model = self._TestModel(in_f, (hid_f, out_f))
        model_path = os.path.join(self.model_path, 'test_linear_net_obos.pt')
        if new:
            torch.save(model.state_dict(), model_path)
        else:
            model.load_state_dict(torch.load(model_path))
        out_true = self.infer_net(model, test_set)
        out_true = [x.tolist()[0] for x in out_true]
        out_true_np = np.asarray(out_true)
        out_data = self._single_run(test_set.data, model, conv=True, fxp=fxp)
        out_data_np = np.asarray([[int(y) for y in x] for x in out_data])
        out_data = [[fxp.to_float(y) for y in x] for x in out_data]

        linear_0_coeff = LinearCoeffs(from_torch=list(model.modules())[2], fxp=fxp)
        linear_1_coeff = LinearCoeffs(from_torch=list(model.modules())[3], fxp=fxp)

        axis_width = 32
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)
        in_data_sig = Axis(intbv()[axis_width:])
        lin_0_in = Axis(intbv()[fxp.width * list(model.modules())[2].in_features:])
        lin_0_out = Axis(intbv()[fxp.width * list(model.modules())[2].out_features:])
        lin_1_out = Axis(intbv()[fxp.width * list(model.modules())[3].out_features:])
        out_data_sig = Axis(intbv()[axis_width:])

        rcv = receiver(clk, reset,
                       in_data=in_data_sig,
                       out_data=lin_0_in,
                       fxp=fxp)
        rcv.convert(hdl='VHDL', path=self.vhdl_output_path, name='axis_net_receiver')

        lin_0 = linear(clk, reset,
                       in_fm=lin_0_in,
                       out_fm=lin_0_out,
                       coeffs=linear_0_coeff,
                       fxp=fxp)
        lin_0.convert(hdl='VHDL', path=self.vhdl_output_path, name='axis_net_lin_0')

        lin_1 = linear(clk, reset,
                       in_fm=lin_0_out,
                       out_fm=lin_1_out,
                       coeffs=linear_1_coeff,
                       fxp=fxp)
        lin_1.convert(hdl='VHDL', path=self.vhdl_output_path, name='axis_net_lin_1')

        txd = transmitter(clk, reset,
                          in_data=lin_1_out,
                          out_data=out_data_sig,
                          fxp=fxp)
        txd.convert(hdl='VHDL', path=self.vhdl_output_path, name='axis_net_txd')

        path = os.path.join(self.data_path, 'test_data')
        file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=fxp)
        file_str += numpy_ndaray_to_cpp_str(out_data_np, 'outputs')
        file_str += numpy_ndaray_to_cpp_str(fxp.to_fixed(out_true_np), 'outputs_true')
        f = open(os.path.join(path, 'test_data.h'), 'w')
        print(file_str, file=f)
        f.close()

        numpy_array_to_txt_vec(fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(out_data_np, path, 'test_output')

        decimal = 1
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    @skip
    def test_linear_net_conv(self):
        fxp = FixedDef(8, 8)
        in_f = 5
        size_f = (5, 5)
        model = self._TestModel(in_f, size_f)
        hdl_model = MyHdlModel(model, fxp)
        hdl_model.convert(hdl='VHDL', path=self.vhdl_output_path, name='linear_net')
