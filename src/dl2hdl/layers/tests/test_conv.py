import unittest

import numpy as np

from myhdl import Signal, ResetSignal, ConcatSignal, intbv, block, instance, StopSimulation, instances
from dl2hdl.layers.tests.layer_test_util import concatenated_signal_to_list
from dl2hdl.layers.conv import conv_1d_seq, Conv1DCoeffsBiases
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from dl2hdl.basic.fixed_def import FixedDef
from ml.layers.conv import conv_1d_raw
from config.config import config
import torch


class TestConvolution(unittest.TestCase):

    def test1DOnRandomData(self):
        exhaustive = True
        if exhaustive:
            fm_dephs = range(1, 4)
            fm_heights = range(2, 9)
            kernel_sizes = range(1, 5)
            out_channels_sizes = range(2, 5)
            for fm_height in fm_heights:
                for kernel_size in kernel_sizes:
                    if kernel_size < fm_height:
                        for fm_deph in fm_dephs:
                            for out_channels in out_channels_sizes:
                                print('Test: in channels(' + str(fm_deph) + '), out channels(' + str(out_channels) + '), in height(' + str(
                                    fm_height) + '), kernel size(' + str(kernel_size) + '): ', end='', flush=True)
                                self.single_test(fm_deph, fm_height, kernel_size, out_channels)
        self.single_test(fm_deph=3, fm_height=8, kernel_size=3, out_channels=5, convert=True)

    def single_test(self, fm_deph, fm_height, kernel_size, out_channels, convert=False):

        torch.manual_seed(1)
        fxp = FixedDef(8, 8)

        in_fm_size = (fm_deph, fm_height)
        in_fm_len = np.asscalar(np.prod(in_fm_size))
        padding = 0
        stride = 1
        out_size = ((in_fm_size[1] - kernel_size + 2 * padding) // stride) + 1
        out_fm_len = out_size * out_channels
        input_data = torch.randn(1, in_fm_size[0], in_fm_size[1])
        test_data = input_data.data.numpy()[0]

        conv_0 = torch.nn.Conv1d(in_channels=in_fm_size[0], out_channels=out_channels, kernel_size=kernel_size)
        conv_0_coeffs = Conv1DCoeffsBiases(conv_0, fxp)
        true_output = conv_0(input_data)

        @block
        def tb():
            """
           Test instance for conv_1d_seq
           """

            # stimulation timing
            delay_time = 42
            period = 10

            # signal definitions
            # signal definitions
            in_sig = Signal(intbv(0)[fxp.width * in_fm_len:])
            out_sig = Signal(intbv(0)[fxp.width * out_fm_len:])
            in_fm = [Signal(intbv()[fxp.width:].signed()) for _ in range(in_fm_len)]
            out_fm = [Signal(intbv()[fxp.width:].signed()) for _ in range(out_fm_len)]
            reset = ResetSignal(0, active=0, isasync=False)
            clk = Signal(bool(0))

            # instances
            clk_gen = clk_stim(clk, period=period)
            rst_gen = aresetn_stim(reset, delay_time=delay_time)
            uut = conv_1d_seq(clk=clk,
                              reset=reset,
                              in_fm=in_sig,
                              out_fm=out_sig,
                              coeffs=conv_0_coeffs,
                              fxp=fxp)

            # # test signals
            # in_fm = [[Signal(intbv(fxp.to_fixed(test_data[d][h]))[fxp.width:]) for h in range(in_fm_size[1])] for d in range(in_fm_size[0])]
            # for d in range(len(in_fm)):
            #     for h in range(len(in_fm[0])):
            #         in_fm[d][h].driven = True

            @instance
            def stimulus():
                # wait for reset
                yield reset.posedge
                for d in range(in_fm_size[0]):
                    for h in range(in_fm_size[1]):
                        a = d * in_fm_size[1] + h
                        in_fm[a].next = fxp.to_fixed(input_data.data.numpy()[0][d][h])
                yield clk.negedge
                if len(in_fm) > 1:
                    in_sig.next = ConcatSignal(*in_fm)
                else:
                    in_sig.next = in_fm[0].unsigned()
                yield clk.negedge

                test_out = concatenated_signal_to_list(out_sig, size=(out_channels, out_size), fxp=fxp)
                true_out_fxp = conv_1d_raw(in_fm=input_data.data.numpy()[0], coeffs=conv_0_coeffs, fxp=fxp).tolist()
                self.assertListEqual(test_out, true_out_fxp, 'Linear test failed')
                yield clk.negedge

                print("Linear test passed")

                raise StopSimulation

            # @instance
            # def stimulus():
            #     # wait for reset
            #     yield reset.posedge
            #
            #     yield clk.negedge
            #     if in_fm_size[0] == 1:
            #         in_sig.next = ConcatSignal(*in_fm[0])
            #     else:
            #         in_sig.next = ConcatSignal(*[ConcatSignal(*x) for x in in_fm])
            #     yield clk.posedge
            #     yield clk.posedge
            #     for d in range(out_channels):
            #         tmp_list = list()
            #         for h in range(out_size):
            #             tmp_list.append(fxp.to_float(out_sig[((out_channels - 1) - d) * out_size * fxp.width + ((out_size - 1) - h) * fxp.width + fxp.width:
            #                                                  ((out_channels - 1) - d) * out_size * fxp.width + ((out_size - 1) - h) * fxp.width].signed()))
            #         test_output.append(tmp_list)
            #         del tmp_list
            #
            #     yield clk.posedge
            #
            #     if true_output_fxp != test_output:
            #         print('test data:')
            #         print(test_data)
            #         print('test weights:')
            #         print(test_weights)
            #         print('test biases:')
            #         print(test_bias)
            #         print('true:')
            #         print(true_output_fxp)
            #         print('test:')
            #         print(test_output)
            #
            #         print('test data fxp:')
            #         print(fxp.to_fixed(test_data))
            #         print('test weights fxp:')
            #         print(fxp.to_fixed(test_weights))
            #         print('test biases fxp:')
            #         print(fxp.to_fixed(test_bias))
            #         print('true fxp:')
            #         print(fxp.to_fixed(np.asarray(true_output_fxp)))
            #         print('test fxp:')
            #         print(fxp.to_fixed(np.asarray(test_output)))
            #
            #     self.assertListEqual(true_output_fxp, test_output, 'Convolution 1D doesn\'t work correctly')
            #     print("conv 1d test passed")
            #     raise StopSimulation()

            # convert DUT
            if convert:
                uut.convert(hdl='VHDL', path=config.get_path_out_vhdl(), initial_values=True)

            return instances()

        tb_inst = tb()
        tb_inst.config_sim(backend='myhld', trace=True, directory=config.get_path_out_testbench(), name='conv1d_tb')
        tb_inst.run_sim(quiet=1)


if __name__ == '__main__':
    unittest.main(verbosity=2)
