from unittest import skip
from dl2hdl.tests.hdl_test import HdlTest
import torch.nn as nn
from torch.utils.data import DataLoader
import torch
import os
import numpy as np
import warnings
import time

from myhdl import block, Signal, instances, ResetSignal, intbv, instance, delay, StopSimulation
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.axi.axis import Axis
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from dl2hdl.layers.tests.torch_dataset import RandomBin
from dl2hdl.basic.cpp_if import torch_data_set_to_cpp_string, numpy_ndaray_to_cpp_str
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.model import MyHdlModel


class TestLinearBlocking(HdlTest):
    class _TestModel(nn.Module):

        def __init__(self, in_features, sizes):
            super().__init__()
            self.classifier = nn.Sequential()
            self.classifier.add_module('linear0', nn.Linear(in_features=in_features, out_features=sizes[0]))
            for i in range(1, len(sizes)):
                self.classifier.add_module(f'linear{i}', nn.Linear(in_features=sizes[i - 1], out_features=sizes[i]))

        def forward(self, x):
            x = self.classifier(x)
            return x

    @staticmethod
    def infer_net(model, data_set, device=torch.device('cpu')):
        test_loader = DataLoader(data_set, batch_size=1, shuffle=False)
        model.eval()
        out_true = []
        with torch.no_grad():
            for data, target in test_loader:
                data, target = data.to(device), target.to(device)
                out_true.append(model(data))
        return out_true

    def _single_run(self, test_data, model, conv=False, fxp=None):
        @block
        def linear_prun_net_tb(data, out_data, model, conv=False, fxp=None):
            axis_width = 32
            in_fm = Axis(intbv()[axis_width:])
            out_fm = Axis(intbv()[axis_width:])
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)
            rst_gen = aresetn_stim(reset, delay_time=24)

            stim_write = Axis.write(clk, reset, in_fm, data.numpy(), fxp, verbose=True)

            hdl_model = MyHdlModel(torch_model=model, fxp=fxp)
            uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)

            pass

            @instance
            def stim_read():
                yield reset.posedge
                yield delay(24)
                yield clk.negedge
                out_fm.tready.next = 1
                for _ in range(len(data)):
                    out_row = []
                    while True:
                        yield clk.negedge
                        if out_fm.tvalid == 1:
                            out_row.append(out_fm.tdata.signed())
                            if out_fm.tlast == 1:
                                break
                    out_data.append(out_row)

                for i in range(10):
                    yield clk.negedge

                # Print output for testbench
                # for sample in out_data:
                #     print(sample)

                if conv:
                    uut.convert(hdl='VHDL', path=self.vhdl_output_path)

                raise StopSimulation()

            return instances()

        out_data = []
        tb = linear_prun_net_tb(data=test_data, out_data=out_data, model=model, conv=conv, fxp=fxp)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return out_data

    # @skip
    def test_linear_for_hw(self):
        new = False
        fxp = FixedDef(8, 16)
        seq_len = 1
        in_f = 4
        out_f = 2
        print(f'testing {in_f}-{out_f}')
        test_set = RandomBin(shape=(in_f,), num_of_samples=seq_len, seed=2)
        model = self._TestModel(in_f, (out_f,))
        model_path = os.path.join(self.model_path, 'test_linear_prun_net.pt')
        if new:
            torch.save(model.state_dict(), model_path)
        else:
            model.load_state_dict(torch.load(model_path))
        out_true = self.infer_net(model, test_set)
        out_true = [x.tolist()[0] for x in out_true]
        out_true_np = np.asarray(out_true)

        model.classifier.linear0.weight.data[0][0] = 0.0
        model.classifier.linear0.weight.data[0][3] = 0.0
        model.classifier.linear0.weight.data[1][2] = 0.0

        out_data = self._single_run(test_set.data, model, conv=True, fxp=fxp)
        out_data_np = np.asarray([[int(y) for y in x] for x in out_data])
        out_data = [[fxp.to_float(y) for y in x] for x in out_data]

        path = os.path.join(self.data_path, 'linear_prun')
        file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=fxp)
        file_str += numpy_ndaray_to_cpp_str(out_data_np, 'outputs')
        file_str += numpy_ndaray_to_cpp_str(fxp.to_fixed(out_true_np), 'outputs_true')
        os.makedirs(path, exist_ok=True)
        f = open(os.path.join(path, 'test_data.h'), 'w')
        print(file_str, file=f)
        f.close()

        numpy_array_to_txt_vec(fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(out_data_np, path, 'test_output')

        decimal = 6
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)
