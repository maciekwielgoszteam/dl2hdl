

def concatenated_signal_to_list(concat_sig, size=4, fxp=None):
    """
    :param concat_sig: DUT output signal (concatenated fixed point signals)
    :param size: output data size TODO: parameter outdated, left for backward compatibility, check if can be safely removed
    :param fxp: fixed point definition
    :return: deflatened list of output signal values converted to float
    """
    assert fxp is not None, 'Please provide fixed point definition'
    size = len(concat_sig) // fxp.width
    assert size * fxp.width == len(concat_sig)
    out_data = []
    for i in range(size):
        f = fxp.to_float(concat_sig[((size - 1) - i) * fxp.width + fxp.width:((size - 1) - i) * fxp.width].signed())
        out_data.append(f)
    return out_data


def diff_to_lsb(diff, fxp=None):
    """
    Value to fixed point resolution LSB count
    :param diff: Value
    :param fxp: Fixed point definition
    :return:
    """
    assert fxp is not None, 'Please provide fixed point definition'
    return 0 if diff == 0 else diff/(1/(2**fxp.f))


def assert_list_diff_below_n_lsb(a, b, max_lsb_error=1,
                                 fxp=None, verbose=False, msg_ok=None, msg_fail='Failed', msg_start=None):
    """
    Compare two lists of fixed point values,
    asserting the difference for each element pair to be below max_lsb_error fxp lsb
    :param a: list a
    :param b: list b
    :param max_lsb_error: maximum error in fixed point fraction lsb
    :param fxp: fixed point definition
    :param verbose: verbose output
    :param msg_ok: success message
    :param msg_fail: fail message
    :param msg_start: start message
    :return:
    """
    assert fxp is not None, 'Please provide fixed point definition'
    if msg_start is not None:
        print(msg_start)
    for i, e in zip(a, b):
        diff = abs(i-e)
        if verbose:
            print(f'exp {e:2.8f} got {i:2.8f} diff {diff:2.8f} '
                  f'(error_lsb {diff_to_lsb(diff, fxp=fxp):.4f} perc {(diff / e)*100:.2f}%)')
        assert diff_to_lsb(abs(e - i), fxp=fxp) <= max_lsb_error, msg_fail
    if msg_ok is not None:
        print(msg_ok)
