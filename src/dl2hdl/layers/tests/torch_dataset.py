import numpy as np
import torch
from torch.utils.data import Dataset


class RandomBin(Dataset):

    """
    Creates a random dataset with binary labels
    """

    def __init__(self, shape, num_of_samples=1, range_min_max=(-1, 1), seed=None):
        assert range_min_max[1] > range_min_max[0]
        if seed is not None:
            np.random.seed(seed)
        range_val = range_min_max[1] - range_min_max[0]
        data_pos = (np.random.rand(num_of_samples // 2, *shape) * range_val) + range_min_max[0]
        labels_pos = np.ones(num_of_samples // 2)
        data_neg = (np.random.rand(num_of_samples - num_of_samples // 2, *shape) * range_val) + range_min_max[0]
        labels_neg = np.zeros(num_of_samples - num_of_samples // 2)
        self.data = torch.from_numpy(np.concatenate((data_pos, data_neg), axis=0)).float()
        self.labels = torch.from_numpy(np.concatenate((labels_pos, labels_neg), axis=0)).float()

    def __getitem__(self, item):
        return self.data[item], self.labels[item]

    def __len__(self):
        return self.data.shape[0]


class RandomSeries(Dataset):

    """
    Creates a random series dataset
    """

    def __init__(self, shape, num_of_samples=1, range_min_max=(-1, 1), seed=None):
        assert range_min_max[1] > range_min_max[0]
        if seed is not None:
            np.random.seed(seed)
        range_val = range_min_max[1] - range_min_max[0]
        self.data = torch.from_numpy((np.random.rand(num_of_samples, *shape) * range_val) + range_min_max[0]).float()
        self.labels = (np.random.rand(num_of_samples) * range_val) + range_min_max[0]

    def __getitem__(self, item):
        return self.data[item], self.labels[item]

    def __len__(self):
        return self.data.shape[0]
