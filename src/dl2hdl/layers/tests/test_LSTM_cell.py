from dl2hdl.tests.hdl_test import HdlTest
import os
import warnings
import torch
import torch.nn as nn
import numpy as np
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.axi.axis import Axis
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from myhdl import block, instances, StopSimulation, intbv, instance, Signal, delay, ResetSignal
from dl2hdl.layers.tests.torch_dataset import RandomBin
from dl2hdl.basic.interface_map import receiver, transmitter
from dl2hdl.layers.lstm import LSTMCoeffs
from dl2hdl.layers.lstm import LSTM_cell


class TestLSTM_cell(HdlTest):

    @staticmethod
    @block
    def lstm_axis(clk, reset, in_data, in_cell, in_hidden, out_cell, out_hidden, layer, fxp=None):
        assert isinstance(fxp, FixedDef)

        lstm_coeff = LSTMCoeffs(from_torch=layer, fxp=fxp)
        in_data_sig = Axis(intbv()[fxp.width * lstm_coeff.layer.input_size:])
        in_cell_sig = Axis(intbv()[fxp.width * lstm_coeff.layer.hidden_size:])
        in_hidden_sig = Axis(intbv()[fxp.width * lstm_coeff.layer.hidden_size:])
        out_cell_sig = Axis(intbv()[fxp.width * lstm_coeff.layer.hidden_size:])
        out_hidden_sig = Axis(intbv()[fxp.width * lstm_coeff.layer.hidden_size:])

        layers = []

        layers.append(receiver(clk, reset,
                               in_data=in_data,
                               out_data=in_data_sig,
                               fxp=fxp))
        layers.append(receiver(clk, reset,
                               in_data=in_cell,
                               out_data=in_cell_sig,
                               fxp=fxp))
        layers.append(receiver(clk, reset,
                               in_data=in_hidden,
                               out_data=in_hidden_sig,
                               fxp=fxp))

        layers.append(LSTM_cell(clk, reset,
                                in_data=in_data_sig,
                                in_cell=in_cell_sig,
                                in_hidden=in_hidden_sig,
                                out_cell=out_cell_sig,
                                out_hidden=out_hidden_sig,
                                coeffs=lstm_coeff, fxp=fxp))

        layers.append(transmitter(clk, reset,
                                  in_data=out_cell_sig,
                                  out_data=out_cell,
                                  fxp=fxp))
        layers.append(transmitter(clk, reset,
                                  in_data=out_hidden_sig,
                                  out_data=out_hidden,
                                  fxp=fxp))

        return instances()

    def _single_run(self, in_data, in_cell, in_hidden, layer, conv=False, fxp=None):
        @block
        def lstm_cell_tb(in_data, in_cell, in_hidden, out_cell, out_hidden, layer, conv=False, fxp=None):
            axis_width = 32
            in_data_sig = Axis(intbv()[axis_width:])
            in_cell_sig = Axis(intbv()[axis_width:])
            in_hidden_sig = Axis(intbv()[axis_width:])
            out_cell_sig = Axis(intbv()[axis_width:])
            out_hidden_sig = Axis(intbv()[axis_width:])
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)
            rst_gen = aresetn_stim(reset, delay_time=24)


            stim_write_data = Axis.write(clk, reset, in_data_sig, in_data.numpy(), fxp, verbose=True)
            stim_write_cell = Axis.write(clk, reset, in_cell_sig, in_cell.numpy(), fxp, verbose=True)
            stim_write_hidden = Axis.write(clk, reset, in_hidden_sig, in_hidden.numpy(), fxp, verbose=True)

            coeffs = LSTMCoeffs(layer, fxp)
            uut = self.lstm_axis(clk, reset,
                                 in_data=in_data_sig,
                                 in_cell=in_cell_sig,
                                 in_hidden=in_hidden_sig,
                                 out_cell=out_cell_sig,
                                 out_hidden=out_hidden_sig,
                                 layer=layer,
                                 fxp=fxp)

            @instance
            def stim_read_cell():
                yield reset.posedge
                yield delay(24)
                yield clk.negedge
                out_cell_sig.tready.next = 1
                out_row = []
                while True:
                    yield clk.negedge
                    if out_cell_sig.tvalid == 1:
                        out_row.append(out_cell_sig.tdata.signed())
                        if out_cell_sig.tlast == 1:
                            break
                out_cell.append(out_row)

            @instance
            def stim_read_hidden():
                yield reset.posedge
                yield delay(24)
                yield clk.negedge
                out_hidden_sig.tready.next = 1
                out_row = []
                while True:
                    yield clk.negedge
                    if out_hidden_sig.tvalid == 1:
                        out_row.append(out_hidden_sig.tdata.signed())
                        if out_hidden_sig.tlast == 1:
                            break
                out_hidden.append(out_row)

                for i in range(10):
                    yield clk.negedge

                if conv:
                    uut.convert(hdl='VHDL', path=self.vhdl_output_path)

                raise StopSimulation()

            return instances()

        out_cell = []
        out_hidden = []
        tb = lstm_cell_tb(in_data, in_cell, in_hidden, out_cell, out_hidden, layer, conv=conv, fxp=fxp)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return out_hidden, out_cell

    def test_LSTM_cell(self):
        new = True
        data_seed = 8
        torch_seed = 13
        fxp = FixedDef(4, 6)
        test_sequences = 1
        seq_len = 1
        in_f = 4
        hid_f = 16
        print(f'testing {in_f}-{hid_f}')
        test_data = RandomBin(shape=(in_f,), num_of_samples=test_sequences, seed=data_seed)
        lstm_l = nn.LSTM(input_size=in_f, hidden_size=hid_f, batch_first=True)
        model_path = os.path.join(self.model_path, 'LSTM_cell_test.pt')
        if new:
            torch.save(lstm_l.state_dict(), model_path)
        else:
            lstm_l.load_state_dict(torch.load(model_path))
        torch.manual_seed(torch_seed)
        hidden = (torch.randn(1, 1, hid_f), torch.randn(1, 1, hid_f))

        # print('weights_ih:')
        # print(lstm_l.weight_ih_l0)
        # print('bias_ih:')
        # print(lstm_l.bias_ih_l0)
        # print('weights_hh:')
        # print(lstm_l.weight_hh_l0)
        # print('bias_hh:')
        # print(lstm_l.bias_hh_l0)
        # print('previous out:')
        # print(hidden[0])
        # print('previous hidden:')
        # print(hidden[1])
        # print('input:')
        # print(test_data.data)

        out_true, hidden_true = lstm_l(test_data.data.view(1, 1, -1), hidden)
        out_true = [x.tolist()[0] for x in out_true]
        hidden_true = [x.tolist()[0] for x in hidden_true[1]]

        out_data, out_hidden= self._single_run(test_data.data, hidden[1], hidden[0], lstm_l, conv=True, fxp=fxp)
        out_data = [[fxp.to_float(y) for y in x] for x in out_data]
        out_hidden = [[fxp.to_float(y) for y in x] for x in out_hidden]
        decimal = 2
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        np.testing.assert_array_almost_equal([out_data, out_hidden], [out_true, hidden_true], decimal=decimal)
