import unittest
import numpy as np

from myhdl import Signal, ResetSignal, ConcatSignal, intbv, delay, Simulation

from dl2hdl.layers.maxpool import maxpool_flat_seq, maxpool_1d_seq
from dl2hdl.basic.fixed_def import FixedDef
from ml.layers.maxpool import maxpool1d_raw

from config.config import config


class TestPooling(unittest.TestCase):
    # TODO: combine tests for flat and 1D into single run function (similar to MyHDL unittest example)

    def testFlatOnRandomData(self):
        fm_size = 30
        pool_size = 3
        fxp = FixedDef(8, 8)
        test_data = (np.random.rand(fm_size) - 0.75) * 20
        test_data = [fxp.to_float(fxp.to_fixed(x)) for x in test_data]

        # TODO: use external relu implementation, if possible from pytorch
        def maxpool_flat(data, pool):
            max_vals = list()
            for i in range(int(len(data) / pool)):
                max_vals.append(max(data[i * pool:i * pool + pool]))
            return max_vals

        true_output = maxpool_flat(test_data, pool_size)

        test_output = []

        def stimulus(clk, reset, in_sig, out_sig, out_data, in_size=4, pool_size=2, fxp=None):
            assert fxp is not None, 'Please provide fixed point definition'
            out_size = fm_size // pool_size
            in_fm = [Signal(intbv(fxp.to_fixed(test_data[i]))[fxp.width:]) for i in range(int(in_size))]
            for i in range(len(in_fm)):
                in_fm[i].driven = True

            delay_time = 42
            period = 10
            low_time = int(period / 2)
            high_time = period - low_time

            # TODO: Find a way to use ready blocks instead of plain clock and reset control
            # clk_gen = clk_stim(clk, period=period)
            # rst_gen = aresetn_stim(reset, delay_time=delay_time)

            in_sig.next = ConcatSignal(*in_fm)

            reset.next = 0
            yield delay(delay_time)
            reset.next = 1
            yield delay(low_time)
            clk.next = 1
            yield delay(high_time)
            clk.next = 0
            yield delay(low_time)
            clk.next = 1
            yield delay(high_time)
            clk.next = 0

            for i in range(in_size // pool_size):
                out_data.append(fxp.to_float(out_sig[((out_size - 1) - i) * fxp.width + fxp.width:((out_size - 1) - i) * fxp.width].signed()))
            yield delay(1)

        del test_output[:]
        in_sig = Signal(intbv(0)[fxp.width * fm_size:])
        out_sig = Signal(intbv(0)[fxp.width * (fm_size / pool_size):])
        reset = ResetSignal(0, active=0, isasync=False)
        clk = Signal(bool(0))
        uut = maxpool_flat_seq(clk, reset, in_sig, out_sig, in_size=fm_size, pool_size=pool_size, fxp=fxp)
        stim = stimulus(clk, reset, in_sig, out_sig, test_output, in_size=fm_size, pool_size=pool_size, fxp=fxp)
        sim = Simulation(uut, stim)
        sim.run(quiet=1)
        uut.convert(hdl='VHDL', path=config.get_path_out_vhdl(), initial_values=True)
        self.assertListEqual(true_output, test_output, 'MaxPool flat doesn\'t work correctly')

    def test1DOnRandomData(self):
        fm_size = (5, 18)
        fm_len = np.prod(fm_size)
        pool_size = 3
        fxp = FixedDef(8, 8)
        test_data = (np.random.rand(*fm_size) - 0.75) * 20
        test_data = fxp.to_float(fxp.to_fixed(test_data))

        # TODO: consider replacing by pytorch implementation
        true_output = maxpool1d_raw(test_data, pool_size).tolist()

        test_output = []

        def stimulus(clk, reset, in_sig, out_sig, out_data, in_size=(4, 8), pool_size=2, fxp=None):
            assert fxp is not None, 'Please provide fixed point definition'
            if type(pool_size) is not int:
                assert np.ndim(pool_size) == 1, 'Can only accept tuples with one dimension'
                assert len(pool_size) == 1, 'pool size can oly have 2 parameters in 2D pooling'
                pool_size = pool_size[0]
            out_fms = fm_size[0]
            out_size = fm_size[1] // pool_size
            in_fm = [[Signal(intbv(fxp.to_fixed(test_data[d][h]))[fxp.width:]) for h in range(in_size[1])] for d in range(in_size[0])]
            for d in range(len(in_fm)):
                for h in range(len(in_fm[0])):
                    in_fm[d][h].driven = True

            delay_time = 42
            period = 10
            low_time = int(period / 2)
            high_time = period - low_time

            # TODO: Find a way to use ready blocks instead of plain clock and reset control
            # clk_gen = clk_stim(clk, period=period)
            # rst_gen = aresetn_stim(reset, delay_time=delay_time)

            in_sig.next = ConcatSignal(*[ConcatSignal(*x) for x in in_fm])

            reset.next = 0
            yield delay(delay_time)
            reset.next = 1
            clk.next = 0
            yield delay(low_time)
            clk.next = 1
            yield delay(high_time)
            clk.next = 0
            yield delay(low_time)
            clk.next = 1
            yield delay(high_time)
            clk.next = 0

            for d in range(out_fms):
                tmp_list = list()
                for h in range(out_size):
                    tmp_list.append(fxp.to_float(out_sig[((out_fms - 1) - d) * out_size * fxp.width + ((out_size - 1) - h) * fxp.width + fxp.width
                                                         : ((out_fms - 1) - d) * out_size * fxp.width + ((out_size - 1) - h) * fxp.width].signed()))
                out_data.append(tmp_list)
                del tmp_list

            yield delay(1)

        del test_output[:]
        in_sig = Signal(intbv(0)[fxp.width * fm_len:])
        out_sig = Signal(intbv(0)[fxp.width * (fm_len / np.prod(pool_size)):])
        reset = ResetSignal(0, active=0, isasync=False)
        clk = Signal(bool(0))
        uut = maxpool_1d_seq(clk, reset, in_sig, out_sig, in_size=fm_size, pool_size=pool_size, fxp=fxp)
        stim = stimulus(clk, reset, in_sig, out_sig, test_output, in_size=fm_size, pool_size=pool_size, fxp=fxp)
        sim = Simulation(uut, stim)
        sim.run(quiet=1)
        uut.convert(hdl='VHDL', path=config.get_path_out_vhdl(), initial_values=True)
        self.assertListEqual(true_output, test_output, 'MaxPool 1D doesn\'t work correctly')


if __name__ == '__main__':
    unittest.main(verbosity=2)
