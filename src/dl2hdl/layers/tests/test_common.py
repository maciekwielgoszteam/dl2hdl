import unittest
import numpy as np
import dl2hdl.layers.tests.layer_test_util as tutils

from myhdl import *

from dl2hdl.axi.common import elementwise_sum_comb, elementwise_mul_comb

from dl2hdl.basic.fixed_def import FixedDef
from config.config import config


class TestCommon(unittest.TestCase):

    def testElementwiseSumAndMulComb(self):
        assert_prec_sum = 6  # places
        assert_prec_mul = 6
        fm_size = 9
        fxp = FixedDef(8, 8)

        in_a = (np.random.rand(fm_size) - 0.5) * 2
        in_a = fxp.quantize_float(in_a)

        in_b = (np.random.rand(fm_size) - 0.5) * 2
        in_b = fxp.quantize_float(in_b)

        np_sum_fix = np.vectorize(fxp.emul_add)
        expected_sum_fxp = np_sum_fix(in_a, in_b)
        expected_sum_fxp = fxp.quantize_float(expected_sum_fxp)

        expected_sum_precise = in_a + in_b

        np_mul_fix = np.vectorize(fxp.emul_mul)
        expected_mul_fxp = np_mul_fix(in_a, in_b)
        expected_mul_fxp = fxp.quantize_float(expected_mul_fxp)

        expected_mul_precise = np.multiply(in_a, in_b)

        @block
        def tb():
            in_fm_a = [Signal(intbv(fxp.to_fixed(in_a[i]))[fxp.width:]) for i in range(int(fm_size))]
            for i in range(len(in_fm_a)):
                in_fm_a[i].driven = True
            in_fm_b = [Signal(intbv(fxp.to_fixed(in_b[i]))[fxp.width:]) for i in range(int(fm_size))]
            for i in range(len(in_fm_b)):
                in_fm_b[i].driven = True
            in_a_sig = Signal(intbv(0)[fxp.width * fm_size:])
            in_b_sig = Signal(intbv(0)[fxp.width * fm_size:])
            out_sum_sig = Signal(intbv(0)[fxp.width * fm_size:])
            out_mul_sig = Signal(intbv(0)[fxp.width * fm_size:])

            dut1_inst = elementwise_sum_comb(in_a_sig, in_b_sig, out_sum_sig, size=fm_size, fxp=fxp)
            dut1_inst.convert(hdl='VHDL', path=config.get_path_out_vhdl(), initial_values=True)

            dut2_inst = elementwise_mul_comb(in_a_sig, in_b_sig, out_mul_sig, size=fm_size, fxp=fxp)
            dut2_inst.convert(hdl='VHDL', path=config.get_path_out_vhdl(), initial_values=True)

            @instance
            def stimulus():
                in_a_sig.next = ConcatSignal(*in_fm_a)
                in_b_sig.next = ConcatSignal(*in_fm_b)
                yield delay(10)
                i_a = tutils.concatenated_signal_to_list(ConcatSignal(*in_fm_a), size=fm_size, fxp=fxp)
                i_b = tutils.concatenated_signal_to_list(ConcatSignal(*in_fm_b), size=fm_size, fxp=fxp)
                o_sum = tutils.concatenated_signal_to_list(out_sum_sig, size=fm_size, fxp=fxp)
                o_mul = tutils.concatenated_signal_to_list(out_mul_sig, size=fm_size, fxp=fxp)
                print('--- OUT SUM ---')
                for oo, i1, i2, e, p in zip(o_sum, i_a, i_b,
                                            expected_sum_fxp.tolist(), expected_sum_precise):
                    print(f'i1={i1:.8f} i2={i2:.8f} exp={e:.8f} got={oo:.8f} '
                          f'(precise={p:.8f} err_lsb={round(tutils.diff_to_lsb(abs(oo - p), fxp=fxp))})')
                    self.assertAlmostEqual(oo, e, places=assert_prec_sum)

                print('--- OUT MUL ---')
                for oo, i1, i2, e, p in zip(o_mul, i_a, i_b,
                                            expected_mul_fxp.tolist(), expected_mul_precise):
                    print(f'i1={i1:.8f} i2={i2:.8f} exp={e:.8f} got={oo:.8f} '
                          f'(precise={p:.8f} err_lsb={round(tutils.diff_to_lsb(abs(oo - p), fxp=fxp))})')
                    self.assertAlmostEqual(oo, e, places=assert_prec_mul)

            return instances()

        tb_inst = tb()
        tb_inst.config_sim(backend='myhld', trace=False)
        tb_inst.run_sim(quiet=1)
