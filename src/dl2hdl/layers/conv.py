from myhdl import Signal, instances, block, always_seq, ConcatSignal, intbv, always_comb
import numpy as np
from dl2hdl.basic.fixed_def import FixedDef
import torch
import math


class Conv1DCoeffsBiases:
    """
    Wrapper class for 1D conv layer coefficients and biases
    Holds both floating and fixed point representation
    """

    def __init__(self, from_torch=None, fxp=None):
        """
        Conv1D cell weights and biases wrapper ctor
        :param from_torch: torch.nn.Conv1d object of Conv1d layer
        :param fxp: fixed point representation
        """
        if fxp is not None:
            self._fxp = fxp

        if from_torch is not None:
            self._layer = from_torch

    @property
    def fxp(self):
        return self._fxp

    @fxp.setter
    def fxp(self, value):
        assert isinstance(value, FixedDef)
        self._fxp = value

    @property
    def layer(self):
        return self._layer

    @layer.setter
    def layer(self, value):
        assert isinstance(value, torch.nn.Conv1d)
        self._layer = value

    @property
    def weight(self):
        return self._layer.weight.data.numpy()

    @weight.setter
    def weight(self, value):
        if isinstance(value, list):
            value = np.asarray(value)
        assert isinstance(value, np.ndarray)
        self._layer.weight.data = torch.from_numpy(value)

    @property
    def bias(self):
        return self._layer.bias.data.numpy()

    @bias.setter
    def bias(self, value):
        if isinstance(value, list):
            value = np.asarray(value)
        assert isinstance(value, np.ndarray)
        self._layer.bias.data = torch.from_numpy(value)


    @property
    def weight_fxp(self):
        assert isinstance(self._fxp, FixedDef)
        return self._fxp.to_fixed(self.weight)

    @property
    def bias_fxp(self):
        assert isinstance(self._fxp, FixedDef)
        return self._fxp.to_fixed(self.bias)


@block
def conv_1d_single_comb(in_fm, out_data, weights, bias, in_channels=4, kernel_size=3, stride=1, padding=0, fxp=None):
    """
    Performs combinational convolution operation

    Args:
        in_fm: Port, concatenated list of signals comprising a region of input feature map required for output data point
        out_data: Port, concatenated list of maximal values in feature map
        weights: Parameter, list of weights for layer
        bias : Parameter, output biases
        in_channels: Parameter, number of input channels
        kernel_size: Parameter, shape of the kernel
        stride: Parameter, convolution stride, currently only stride == 1 is supported
        padding: Parameter, feature map zero padding, not yet supported
        fxp: Parameter, fixed point parameters definition

    Returns:
        None

    Rises:
        AssertionErrors

    """
    assert fxp is not None, 'You have to provide fixed point definition'
    assert type(in_channels) is int, 'Number of input channels must be integer'
    if type(kernel_size) is not int:
        assert np.ndim(kernel_size) == 1, 'Can only accept tuples with one dimension'
        assert len(kernel_size) == 1, 'kernel size in 1d convolution can only have 1 parameter'
        kernel_size = kernel_size[0]
    assert stride == 1, 'Stride different than 1 is not yet supported'
    assert padding == 0, 'Padding different than 0 is not yet supported'

    size = kernel_size * in_channels

    bias_sig = Signal(intbv(int(bias))[fxp.width:])
    bias_sig.driven = True

    weights_sig = [[Signal(intbv(int(weights[d][kh]))[fxp.width:]) for kh in range(kernel_size)] for d in range(in_channels)]
    for d in range(in_channels):
        for kh in range(kernel_size):
            weights_sig[d][kh].driven = True

    in_fm_sigs = [Signal(intbv()[fxp.width:]) for _ in range(in_channels * kernel_size)]
    out_data_sig = Signal(intbv()[fxp.width:])
    mul_sigs_flatten = [Signal(intbv()[fxp.width:]) for _ in range(kernel_size * in_channels)]
    sum_sigs = [Signal(intbv()[fxp.width:]) for _ in range(size - 1 + (size % 2))]

    @always_comb
    def map_in():
        for d in range(in_channels):
            for kh in range(kernel_size):
                in_fm_sigs[d * kernel_size + kh].next = in_fm[
                                                        ((in_channels - 1) - d) * kernel_size * fxp.width + ((kernel_size - 1) - kh) * fxp.width + fxp.width:
                                                        ((in_channels - 1) - d) * kernel_size * fxp.width + ((kernel_size - 1) - kh) * fxp.width]

    mul_inst = list()
    for d in range(in_channels):
        for kh in range(kernel_size):
            mul_inst.append(fxp.fx_mul(mul_sigs_flatten[d * kernel_size + kh], in_fm_sigs[d * kernel_size + kh], weights_sig[d][kh]))

    sum_inst = list()
    for i in range(size - 1 + (size % 2)):
        if i < math.floor(size / 2):
            sum_inst.append(fxp.fx_add(sum_sigs[i], mul_sigs_flatten[2 * i], mul_sigs_flatten[2 * i + 1]))
        elif i < math.ceil(size / 2):
            sum_sigs[i] = mul_sigs_flatten[2 * i]
        else:
            sum_inst.append(fxp.fx_add(sum_sigs[i], sum_sigs[2 * i - size - (size % 2)], sum_sigs[2 * i - size + 1 - (size % 2)]))

    sum_inst.append(fxp.fx_add(out_data_sig, sum_sigs[size - 2 + (size % 2)], bias_sig))

    @always_comb
    def return_logic():
        out_data.next = out_data_sig

    return instances()


@block
def conv_1d_comb(in_fm, out_fm, coeffs=None, fxp=None):
    """
    Performs combinational convolution operation

    Args:
        in_fm: Port, concatenated list of signals comprising a complete feature map
        out_fm: Port, concatenated list of maximal values in feature map
        coeffs: Parameter, Conv1DCoeffsBiases object with weights, biases, pytorch layer and fixed point
        fxp: Parameter, fixed point parameters definition

    Returns:
        None

    Rises:
        AssertionErrors

    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
    assert isinstance(coeffs, Conv1DCoeffsBiases), 'You have to provide valid coefficients for conv1d'

    kernel_size = coeffs.layer.kernel_size[0]
    stride = coeffs.layer.stride[0]
    padding = coeffs.layer.padding[0]
    dilation = coeffs.layer.dilation[0]
    in_channels = coeffs.layer.in_channels
    in_height = len(in_fm) // in_channels // fxp.width
    out_channels = coeffs.layer.out_channels
    out_height = int(math.floor((in_height + 2 * padding - dilation * (kernel_size - 1) - 1) / stride + 1))
    out_fm_c = Signal(intbv()[fxp.width * out_height * out_channels:])
    in_fm_sig = [Signal(intbv()[fxp.width * kernel_size:]) for _ in range(in_channels * out_height)]
    out_fm_sig = [Signal(intbv()[fxp.width:]) for _ in range(out_channels * out_height)]

    @always_comb
    def map_to_kernels():
        for d in range(in_channels):
            for h in range(out_height):
                in_fm_sig[d * out_height + h].next = in_fm[
                                                     ((in_channels - 1) - d) * in_height * fxp.width + ((in_height - kernel_size) - h) * fxp.width + fxp.width *
                                                     kernel_size: ((in_channels - 1) - d) * in_height * fxp.width + ((in_height - kernel_size) - h) * fxp.width]

    single_conv_inst = list()
    for d in range(out_channels):
        for h in range(out_height):
            if in_channels == 1:
                single_conv_inst.append(
                    conv_1d_single_comb(in_fm=in_fm_sig[h],
                                        out_data=out_fm_sig[d * out_height + h],
                                        weights=coeffs.weight_fxp[d],
                                        bias=coeffs.bias_fxp[d],
                                        in_channels=in_channels,
                                        kernel_size=kernel_size,
                                        fxp=fxp))
            else:
                single_conv_inst.append(
                    conv_1d_single_comb(in_fm=ConcatSignal(*[in_fm_sig[d * out_height + h] for d in range(coeffs.layer.in_channels)]),
                                        out_data=out_fm_sig[d * out_height + h],
                                        weights=coeffs.weight_fxp[d],
                                        bias=coeffs.bias_fxp[d],
                                        in_channels=in_channels,
                                        kernel_size=kernel_size,
                                        fxp=fxp))

    out_fm_c = ConcatSignal(*out_fm_sig)

    @always_comb
    def return_logic():
        out_fm.next = out_fm_c

    return instances()


@block
def conv_1d_seq(clk, reset, in_fm, out_fm, coeffs=None, fxp=None):
    """
    Performs sequential convolution operation

    Args:
        clk: Port, clock signal
        reset: Port, reset signal
        in_fm: Port, concatenated list of signals comprising a complete feature map
        out_fm: Port, concatenated list of maximal values in feature map
        coeffs: Parameter, Conv1DCoeffsBiases object with weights, biases, pytorch layer and fixed point
        fxp: Parameter, fixed point parameters definition

    Returns:
        None

    Raises:
        None

    """

    conv_comb = Signal(intbv()[len(out_fm):])

    conv_1d_comb_inst = conv_1d_comb(in_fm=in_fm,
                                     out_fm=conv_comb,
                                     coeffs=coeffs,
                                     fxp=fxp)

    @always_seq(clk.posedge, reset=reset)
    def seq_proc():
        out_fm.next = conv_comb

    return instances()


def main():
    from ml.layers.conv import conv_1d_raw
    torch.manual_seed(1)
    fxp = FixedDef(8, 8)

    in_size = (4, 8)
    out_channels = 5
    kernel_size = 3

    conv_0 = torch.nn.Conv1d(in_channels=in_size[0], out_channels=out_channels, kernel_size=kernel_size)
    input_data = torch.randn(1, in_size[0], in_size[1])
    true_output = conv_0(input_data)

    conv_0_coeffs = Conv1DCoeffsBiases(conv_0, fxp)

    test_output = conv_1d_raw(input_data.data.numpy()[0], coeffs=conv_0_coeffs, fxp=fxp)

    conv_0_weight = conv_0_coeffs.weight / 2
    conv_0_coeffs.layer.weight.data = torch.from_numpy(conv_0_weight)

    print('bye')


if __name__ == '__main__':
    main()
