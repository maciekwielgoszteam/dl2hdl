from myhdl import block, Signal, intbv, always_comb, always_seq, instances
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.fixed_point import fixbv
import math


@block
def exponent(clk, reset, in_data, out_data, param=3, fxp=None):
    """
    Performs exponential function approximated with series
    exp(x) = sum(x^n / n!) for n = [0, 1, 2, ..., N], where N = param
    exp(x) = 1 + x + x^2 / 2 + x^3 / 6 + x^4 / 24 + ... + n^N / N!
    :param clk: clock
    :param reset: reset signal
    :param in_data: input data
    :param out_data: exponentiation result
    :param param: number of series elements used for approximation
    :param fxp: fixed point definition
    :return: None
    :raises AssertionErrors
    """
    assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
    assert param == 3, 'Currently only param == 3 is supported'

    fxp_wide = FixedDef(fxp.m * param, fxp.f * param)

    in_wide = Signal(intbv()[fxp_wide.width:].signed())
    x_2 = Signal(intbv()[fxp_wide.width:])
    x_2_seq = Signal(intbv()[fxp_wide.width:])
    x_3 = Signal(intbv()[fxp_wide.width:])
    x_3_seq = Signal(intbv()[fxp_wide.width:])
    const_one_sixth = Signal(intbv(fxp_wide.to_fixed(1. / 6.))[fxp_wide.width:], constant=True)
    const_one = Signal(intbv(fxp.to_fixed(1.))[fxp.width:], constant=True)
    x_2_div_2 = Signal(intbv()[fxp.width:])
    x_3_div_6 = Signal(intbv()[fxp_wide.width:])
    x_3_div_6_seq = Signal(intbv()[fxp.width:])
    out_data_sum = Signal(intbv()[fxp.width:].signed())

    mul_i = []

    @always_comb
    def map_in():
        in_wide.next = in_data.signed() << (fxp_wide.f - fxp.f)

    mul_i.append(fxp_wide.fx_mul(x_2, in_wide, in_wide))
    mul_i.append(fxp_wide.fx_mul(x_3, x_2, in_wide))

    mul_i.append(fxp_wide.fx_mul(x_3_div_6, x_3_seq, const_one_sixth))

    @always_seq(clk.posedge, reset)
    def pow_logic():
        x_2_seq.next = x_2
        x_2_div_2.next = (x_2_seq >> 1)[fxp.m + fxp.f * param:fxp.f * (param - 1)]
        x_3_seq.next = x_3
        x_3_div_6_seq.next = x_3_div_6[fxp.m + fxp.f * param:fxp.f * (param - 1)]
        out_data_sum.next = const_one.signed() + in_data.signed() + x_2_div_2.signed() + x_3_div_6_seq.signed()

    @always_comb
    def map_out():
        out_data.next = out_data_sum[fxp.width:]

    return instances()


@block
def exponent_lut(value, result, fxp):
    max_val_flp = math.log(2 ** (fxp.m - 1))
    min_val_flp = math.log(1 / (2 ** fxp.f))
    max_val_fxp = Signal(intbv(fxp.to_fixed(max_val_flp))[fxp.width:].signed(), constant=True)
    min_val_fxp = Signal(intbv(fxp.to_fixed(min_val_flp))[fxp.width:].signed(), constant=True)
    min_val_fxp_u = Signal(intbv(fxp.to_fixed(min_val_flp))[fxp.width:], constant=True)
    max_out_val = Signal(intbv((value.max - 1) >> 1)[fxp.width:], constant=True)

    lut_pos = []
    for i in range(max_val_fxp):
        lut_pos.append(math.exp(fxp.to_float(i)))
    lut_pos_sig = [Signal(fixbv(value=w, format=(fxp.width, fxp.m - 1)), constant=True) for w in lut_pos]

    lut_neg = []
    for i in range(intbv(-1)[fxp.width:] - min_val_fxp.unsigned() + 1):
        lut_neg.append(math.exp(fxp.to_float(min_val_fxp + i)))
    lut_neg_sig = [Signal(fixbv(value=w, format=(fxp.width, fxp.m - 1)), constant=True) for w in lut_neg]

    @always_comb
    def logic():
        if value.signed() < min_val_fxp:
            result.next = 0
        elif value.signed() >= min_val_fxp and value.signed() < 0:
            result.next = lut_neg_sig[value.val - min_val_fxp_u]
        elif value.signed() >= 0 and value.signed() < max_val_fxp:
            result.next = lut_pos_sig[value.val]
        else:
            result.next = max_out_val

    return instances()


if __name__ == '__main__':
    from myhdl import delay, instance, StopSimulation
    from config.config import config


    @block
    def exp_tb(in_data, out_data, fxp=None):
        in_fm = Signal(intbv()[fxp.width:])
        out_fm = Signal(intbv()[fxp.width:])

        uut = exponent_lut(in_fm, out_fm, fxp)

        @instance
        def stim_read():
            yield delay(24)
            for data in in_data:
                in_fm.next = intbv(fxp.to_fixed(data))[fxp.width:].unsigned()
                yield delay(1)
                out_data.append(fxp.to_float(out_fm.signed()))
                yield delay(9)
            raise StopSimulation()

        return instances()

    i = 12
    print(f'i = {i}')
    fxp = FixedDef(4, i)

    in_data = [-7.99, -7.69, -5.20, -1.23, -0.44, 0.1, 0.97, 1.74, 2.54, 3.67, 5.78]
    out_data = []

    tb = exp_tb(in_data, out_data, fxp)
    tb.config_sim(trace=True, directory=config.get_path_out_testbench())
    tb.run_sim()
    import math
    for i in range(len(in_data)):
        print(f'exp({in_data[i]}) = {out_data[i]}, true = {math.exp(in_data[i])}, diff = {math.fabs(out_data[i]-math.exp(in_data[i]))}')
