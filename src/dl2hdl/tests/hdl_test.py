import os
from unittest import TestCase

import numpy as np
import torch
from myhdl import block, instances, StopSimulation, intbv, instance, Signal, delay, ResetSignal
from torch.utils.data import DataLoader

from config.config import config
from dl2hdl.axi.axis import Axis
from dl2hdl.myhdl_sim.clk_stim import clk_stim


class HdlTest(TestCase):
    def setUp(self):
        self.trace_save_path = config.get_path_out_testbench()
        self.vhdl_output_path = config.get_path_out_vhdl()
        self.data_path = config.get_path_out_data()
        self.model_path = config.get_path_out_model()
        os.makedirs(os.path.dirname(self.trace_save_path), exist_ok=True)
        os.makedirs(os.path.dirname(self.vhdl_output_path), exist_ok=True)

    @staticmethod
    def infer_net(model, data_set, device=torch.device('cpu')):
        test_loader = DataLoader(data_set, batch_size=1, shuffle=False)
        model.eval()
        out_true = []
        with torch.no_grad():
            for data, target in test_loader:
                data, target = data.to(device), target.to(device)
                out_true.append(model(data))
        return out_true

    @staticmethod
    def prun_weight(weight, prun):
        weight_flat = weight.flatten()
        indices = np.random.choice(np.arange(weight_flat.shape[0]), replace=False,
                                   size=int(weight_flat.shape[0] * prun))
        weight_flat[indices] = 0.
        weight.data = weight_flat.reshape(weight.shape).data

    def _single_run(self, test_data, hdl_model, fxp=None):
        @block
        def model_tb(data, out_data, hdl_model, fxp=None):
            axis_width = 32
            in_fm = Axis(intbv()[axis_width:])
            out_fm = Axis(intbv()[axis_width:])
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)

            stim_write = Axis.write(clk, reset, in_fm, data.numpy(), fxp, verbose=True)

            uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)

            @instance
            def stim_read():
                reset.next = 0
                yield delay(24)
                reset.next = 1
                yield delay(24)
                yield clk.negedge
                out_fm.tready.next = 1
                for _ in range(data.shape[0]):
                    out_row = []
                    while True:
                        yield clk.negedge
                        if out_fm.tvalid == 1:
                            out_row.append(out_fm.tdata.signed())
                            if out_fm.tlast == 1:
                                break
                    out_data.append(out_row)

                for i in range(10):
                    yield clk.negedge

                reset.next = 0

                for i in range(2):
                    yield clk.negedge

                raise StopSimulation()

            return instances()

        out_data = []
        tb = model_tb(data=test_data, out_data=out_data, hdl_model=hdl_model, fxp=fxp)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return out_data
