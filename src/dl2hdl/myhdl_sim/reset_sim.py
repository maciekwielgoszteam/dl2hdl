from myhdl import block, delay, instance


@block
def aresetn_stim(reset, delay_time=10):
    @instance
    def reset_gen():
        reset.next = 0
        yield delay(delay_time)
        reset.next = 1

    return reset_gen
