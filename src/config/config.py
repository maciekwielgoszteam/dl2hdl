import json
import os


class Config(object):
    """

    """

    config_path = os.path.join(os.path.dirname(__file__), 'config.json')

    def __init__(self, config_file_path=config_path):
        self._config = self.__load_config(config_file_path)
        # dirpath = os.path.dirname(__file__)
        # self._project_root = os.path.join(dirpath[:dirpath.index(self._config['project_name'])], self._config['project_name'])

    def __load_config(self, path):
        """

        :param path:
        :return:
        """
        with open(path, 'r') as f:
            config = json.load(f)

        return config

    # fixbv section

    def get_fixbv_wl(self):
        return self._config['fixbv']['wl']

    def get_fixbv_iwl(self):
        return self._config['fixbv']['iwl']

    def get_fixbv_fwl(self):
        return self._config['fixbv']['fwl']

    def get_path_out_vhdl(self):
        return self._config['paths']['out_vhdl']

    def get_path_out_verilog(self):
        return self._config['paths']['out_verilog']

    def get_path_out_testbench(self):
        return self._config['paths']['out_testbench']

    def get_path_out_model(self):
        return self._config['paths']['out_model']

    def get_path_out_data(self):
        return self._config['paths']['out_data']

    def get_path_data_cern_quench(self):
        return self._config['paths']['data_cern_quench']


config = Config()

if __name__ == '__main__':
    config = Config()
    print(config.get_path_out_vhdl())
    print(config.get_path_out_verilog())
    print(config.get_path_out_testbench())
    print(config.get_path_out_model())
    print(config.get_path_out_data())
    print(config.get_path_data_cern_quench())
