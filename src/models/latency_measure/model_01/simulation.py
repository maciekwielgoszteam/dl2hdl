import os
import time
import warnings
from unittest import skip

import numpy as np
import torch
from myhdl import intbv, Signal, ResetSignal
from torch.utils.data import DataLoader

from dl2hdl.axi.axis import Axis
from dl2hdl.basic.cpp_if import torch_data_set_to_cpp_string, numpy_ndaray_to_cpp_str
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.model import MyHdlModel
from dl2hdl.tests.hdl_test import HdlTest
from models.cen_01_model.data import CernData
from models.cen_01_model.model import CernModel_01


class TestLatencyNet(HdlTest):

    @staticmethod
    def infer_net(model, data_set, device=torch.device('cpu')):
        test_loader = DataLoader(data_set, batch_size=1, shuffle=False)
        model.eval()
        out_true = []
        with torch.no_grad():
            for data, target in test_loader:
                data, target = data.to(device), target.to(device)
                out_true.append(model(data))
        return out_true

    def setUp(self):
        super().setUp()
        self.net_name = 'latency_net_v1'
        self.fxp = FixedDef(4, 12)
        # self.test_sequences = 1s
        self.seq_len = 8
        self.in_f = 4
        self.hid_f = (64, 32)
        self.out_dim = 2
        self.pruning = 0.0

    def prun(self, model, prun=None):
        # TODO: fix bias pruning
        prun = prun if prun is not None else self.pruning
        self.prun_weight(model.lstm_0.weight_ih_l0, prun)
        self.prun_weight(model.lstm_0.weight_hh_l0, prun)
        # self.prun_weight(model.lstm_0.bias_ih_l0)
        # self.prun_weight(model.lstm_0.bias_ih_l0)
        self.prun_weight(model.lstm_1.weight_ih_l0, prun)
        self.prun_weight(model.lstm_1.weight_hh_l0, prun)
        # self.prun_weight(model.lstm_1.bias_ih_l0)
        # self.prun_weight(model.lstm_1.bias_ih_l0)
        self.prun_weight(model.linear.weight, prun)
        # self.prun_weight(model.linear.bias)
        return model

    def test_latency_01_net_sim(self):
        new = True
        settings = f'{self.seq_len}-{self.in_f}-{self.hid_f[0]}-{self.hid_f[1]}-{self.out_dim}'
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: testing {settings}')
        test_data_path = '/home/michal/datasets/pm-hardware/subsets/grided_test_100_set.npy'
        test_set = CernData(data_path=test_data_path, out_len=1)
        test_set.data = test_set.data[:, :self.seq_len, :]
        model = CernModel_01(self.in_f, self.hid_f, self.out_dim)
        model_path = os.path.join(self.model_path, self.net_name + '.pt')
        if new:
            model = self.prun(model)
            torch.save(model.state_dict(), model_path)
        else:
            model.load_state_dict(torch.load(model_path))

        out_true = self.infer_net(model, test_set)
        out_true = [each.tolist() for each in out_true[0]]
        hdl_model = MyHdlModel(model, seq_len=self.seq_len, fxp=self.fxp)
        out_data = self._single_run(test_set.data, hdl_model, fxp=self.fxp)
        out_data = [[self.fxp.to_float(y) for y in x] for x in out_data]

        test_set.data = test_set.data.view(*test_set.data.shape[1:])
        out_true_np = np.asarray(out_true)
        out_data_np = np.asarray(out_data)
        path = os.path.join(self.data_path, self.net_name)
        file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=self.fxp)
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_data_np), 'outputs')
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_true_np), 'outputs_true')
        os.makedirs(path, exist_ok=True)
        f = open(os.path.join(path, 'test_data.h'), 'w')
        print(file_str, file=f)
        f.close()

        numpy_array_to_txt_vec(self.fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(self.fxp.to_fixed(out_data_np), path, 'test_output')

        decimal = 3
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        print(f'out_data:\n{out_data}')
        print(f'out_true:\n{out_true}')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    def test_latency_02_conv(self):
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Creating model...')
        model = CernModel_01(self.in_f, self.hid_f, self.out_dim)
        model_path = os.path.join(self.model_path, self.net_name + '.pt')
        model.load_state_dict(torch.load(model_path))
        hdl_model = MyHdlModel(model, seq_len=self.seq_len, fxp=self.fxp)
        hdl_model.convert(path=self.vhdl_output_path, name=self.net_name)
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')

    @skip
    def test_latency_03_conv_one_by_one(self):
        """
        This function is for testing and debug
        """
        from dl2hdl.basic.interface_map import receiver, transmitter
        from dl2hdl.layers.linear import linear, LinearCoeffs
        from dl2hdl.layers.lstm import LSTM, LSTM_cell, LSTMCoeffs
        from dl2hdl.axi.common import packet_block
        from dl2hdl.layers.activation.softmax import softmax
        import dl2hdl.axi.common as axis
        from dl2hdl.layers.activation.sigmoid import hard_sigmoid
        from dl2hdl.layers.activation.tanh import tanh
        import torch.nn as nn

        model = CernModel_01(self.in_f, self.hid_f, self.out_dim)
        model_path = os.path.join(self.model_path, self.net_name + '.pt')
        model.load_state_dict(torch.load(model_path))

        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Converting...')

        axis_width = 32
        in_fm = Axis(intbv()[axis_width:])
        out_fm = Axis(intbv()[axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)

        lstm_0_coeff = LSTMCoeffs(from_torch=list(model.modules())[1], fxp=self.fxp)
        lstm_1_coeff = LSTMCoeffs(from_torch=list(model.modules())[2], fxp=self.fxp)
        linear_0_coeff = LinearCoeffs(from_torch=list(model.modules())[3], fxp=self.fxp)

        # RECEIVER
        connection_0 = Axis(intbv()[self.fxp.width * lstm_0_coeff.layer.input_size:])
        rcv = receiver(clk, reset,
                       in_data=in_fm,
                       out_data=connection_0,
                       fxp=self.fxp)
        rcv.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_receiver')

        # LSTM 0
        connection_1 = Axis(intbv()[self.fxp.width * lstm_0_coeff.layer.hidden_size:])

        lstm_0_hid_size = lstm_0_coeff.layer.hidden_size
        lstm_0_cell = Axis(intbv()[lstm_0_hid_size * self.fxp.width:])
        lstm_0_cell_reg = Axis(intbv()[lstm_0_hid_size * self.fxp.width:])
        lstm_0_cell_next = Axis(intbv()[lstm_0_hid_size * self.fxp.width:])

        lstm_0_hidden = Axis(intbv()[lstm_0_hid_size * self.fxp.width:])
        lstm_0_hidden_next = Axis(intbv()[lstm_0_hid_size * self.fxp.width:])
        lstm_0_hidden_reg_0 = Axis(intbv()[lstm_0_hid_size * self.fxp.width:])
        lstm_0_hidden_reg_1 = Axis(intbv()[lstm_0_hid_size * self.fxp.width:])

        lstm_0_clear_cell_0 = Signal(bool(0))
        lstm_0_set_cell_1 = Signal(bool(0))
        lstm_0_clear_hidden_0 = Signal(bool(0))
        lstm_0_set_hidden_1 = Signal(bool(0))

        # LSTM 0 cell
        lstm_0_cell_linear_ih_result = Axis(intbv()[lstm_0_coeff.layer.hidden_size * 4 * self.fxp.width:])
        lstm_0_cell_linear_hh_result = Axis(intbv()[lstm_0_coeff.layer.hidden_size * 4 * self.fxp.width:])
        lstm_0_cell_linear_sum_result = Axis(intbv()[lstm_0_coeff.layer.hidden_size * 4 * self.fxp.width:])
        lstm_0_cell_gate_i = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_gate_f = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_gate_g = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_gate_o = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_gate_i_act = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_gate_f_act = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_gate_g_act = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_gate_o_act = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_mul_ig = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_mul_fc_prev = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_c_next = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_c_next_raw = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])
        lstm_0_cell_c_next_act = Axis(intbv()[lstm_0_coeff.layer.hidden_size * self.fxp.width:])

        lstm_0_cell_sum_i = []
        lstm_0_cell_mul_i = []
        lstm_0_cell_axis_i = []

        lstm_0_cell_linear_ih = nn.Linear(in_features=lstm_0_coeff.layer.input_size,
                                          out_features=lstm_0_coeff.layer.hidden_size * 4)
        lstm_0_cell_coeffs_ih = LinearCoeffs(from_torch=lstm_0_cell_linear_ih, fxp=self.fxp)
        lstm_0_cell_coeffs_ih.weight = lstm_0_coeff.weight_ih
        lstm_0_cell_coeffs_ih.bias = lstm_0_coeff.bias_ih
        lstm_0_cell_linear_hh = nn.Linear(in_features=lstm_0_coeff.layer.hidden_size,
                                          out_features=lstm_0_coeff.layer.hidden_size * 4)
        lstm_0_cell_coeffs_hh = LinearCoeffs(from_torch=lstm_0_cell_linear_hh, fxp=self.fxp)
        lstm_0_cell_coeffs_hh.weight = lstm_0_coeff.weight_hh
        lstm_0_cell_coeffs_hh.bias = lstm_0_coeff.bias_hh

        # W_{i} x_t + b_{i} + W_{h} h_{(t-1)} + b_{h}
        lstm_0_cell_lin_ih = linear(clk, reset, in_fm=connection_0, out_fm=lstm_0_cell_linear_ih_result,
                                    coeffs=lstm_0_cell_coeffs_ih, fxp=self.fxp)
        lstm_0_cell_lin_ih.convert(hdl='VHDL', path=self.vhdl_output_path,
                                   name=self.net_name + '_split_lstm_0_lstm_cell_lin_ih')
        lstm_0_cell_lin_hh = linear(clk, reset, in_fm=lstm_0_hidden, out_fm=lstm_0_cell_linear_hh_result,
                                    coeffs=lstm_0_cell_coeffs_hh, fxp=self.fxp)
        lstm_0_cell_lin_hh.convert(hdl='VHDL', path=self.vhdl_output_path,
                                   name=self.net_name + '_split_lstm_0_lstm_cell_lin_hh')
        lstm_0_cell_sum_i.append(
            axis.elementwise_sum_comb(in_a=lstm_0_cell_linear_ih_result, in_b=lstm_0_cell_linear_hh_result,
                                      out_fm=lstm_0_cell_linear_sum_result, fxp=self.fxp))
        lstm_0_cell_sum_i[0].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_0_lstm_cell_sum_i_0')
        lstm_0_cell_axis_i.append(
            axis.split_4_comb(bus_a=lstm_0_cell_gate_i, bus_b=lstm_0_cell_gate_f, bus_c=lstm_0_cell_gate_g,
                              bus_d=lstm_0_cell_gate_o, bus_in=lstm_0_cell_linear_sum_result))
        lstm_0_cell_axis_i[0].convert(hdl='VHDL', path=self.vhdl_output_path,
                                      name=self.net_name + '_split_lstm_0_lstm_cell_axis_i_0')

        # i_t = \sigma(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi})
        lstm_0_cell_act_i = hard_sigmoid(clk, reset, in_fm=lstm_0_cell_gate_i, out_fm=lstm_0_cell_gate_i_act,
                                         fxp=self.fxp)
        lstm_0_cell_act_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                  name=self.net_name + '_split_lstm_0_lstm_cell_act_i')
        # f_t = \sigma(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf})
        lstm_0_cell_act_f = hard_sigmoid(clk, reset, in_fm=lstm_0_cell_gate_f, out_fm=lstm_0_cell_gate_f_act,
                                         fxp=self.fxp)
        lstm_0_cell_act_f.convert(hdl='VHDL', path=self.vhdl_output_path,
                                  name=self.net_name + '_split_lstm_0_lstm_cell_act_f')
        # g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hg} h_{(t-1)} + b_{hg})
        lstm_0_cell_act_g = tanh(clk, reset, in_fm=lstm_0_cell_gate_g, out_fm=lstm_0_cell_gate_g_act,
                                 implementation='linear', fxp=self.fxp)
        lstm_0_cell_act_g.convert(hdl='VHDL', path=self.vhdl_output_path,
                                  name=self.net_name + '_split_lstm_0_lstm_cell_act_g')
        # o_t = \sigma(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho})
        lstm_0_cell_act_o = hard_sigmoid(clk, reset, in_fm=lstm_0_cell_gate_o, out_fm=lstm_0_cell_gate_o_act,
                                         fxp=self.fxp)
        lstm_0_cell_act_o.convert(hdl='VHDL', path=self.vhdl_output_path,
                                  name=self.net_name + '_split_lstm_0_lstm_cell_act_o')

        # c_t = f_t c_{(t-1)} + i_t g_t
        lstm_0_cell_mul_i.append(axis.elementwise_mul_comb(in_a=lstm_0_cell_gate_i_act, in_b=lstm_0_cell_gate_g_act,
                                                           out_fm=lstm_0_cell_mul_ig, fxp=self.fxp))
        lstm_0_cell_mul_i[0].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_0_lstm_cell_mul_i_0')
        lstm_0_cell_mul_i.append(
            axis.elementwise_mul_comb(in_a=lstm_0_cell_gate_f_act, in_b=lstm_0_cell, out_fm=lstm_0_cell_mul_fc_prev,
                                      fxp=self.fxp))
        lstm_0_cell_mul_i[1].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_0_lstm_cell_mul_i_1')
        lstm_0_cell_sum_i.append(
            axis.elementwise_sum_comb(in_a=lstm_0_cell_mul_ig, in_b=lstm_0_cell_mul_fc_prev, out_fm=lstm_0_cell_c_next,
                                      fxp=self.fxp))
        lstm_0_cell_sum_i[1].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_0_lstm_cell_sum_i_1')

        lstm_0_cell_axis_i.append(
            axis.broadcast_2_comb(bus_a=lstm_0_cell_c_next_raw, bus_b=lstm_0_cell_next, bus_in=lstm_0_cell_c_next))
        lstm_0_cell_axis_i[1].convert(hdl='VHDL', path=self.vhdl_output_path,
                                      name=self.net_name + '_split_lstm_0_lstm_cell_axis_i_1')

        # h_t = o_t \tanh(c_t)
        lstm_0_cell_act_out = tanh(clk, reset, in_fm=lstm_0_cell_c_next_raw, out_fm=lstm_0_cell_c_next_act,
                                   implementation='linear', fxp=self.fxp)
        lstm_0_cell_act_out.convert(hdl='VHDL', path=self.vhdl_output_path,
                                    name=self.net_name + '_split_lstm_0_lstm_cell_act_out')
        lstm_0_cell_mul_i.append(axis.elementwise_mul_comb(in_a=lstm_0_cell_c_next_act, in_b=lstm_0_cell_gate_o_act,
                                                           out_fm=lstm_0_hidden_next, fxp=self.fxp))
        lstm_0_cell_mul_i[2].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_0_lstm_cell_mul_i_2')

        lstm_0_lstm_cell_i = LSTM_cell(clk, reset,
                                       in_data=connection_0,
                                       in_cell=lstm_0_cell,
                                       in_hidden=lstm_0_hidden,
                                       out_cell=lstm_0_cell_next,
                                       out_hidden=lstm_0_hidden_next,
                                       coeffs=lstm_0_coeff,
                                       sigmoid_type='hard',
                                       fxp=self.fxp)
        lstm_0_lstm_cell_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                   name=self.net_name + '_split_lstm_0_lstm_cell')

        lstm_0_cell_reg_0_i = axis.axis_reg(clk, reset,
                                            bus_m=lstm_0_cell_reg,
                                            bus_s=lstm_0_cell_next,
                                            clear_reg=lstm_0_clear_cell_0,
                                            set_reg=0,
                                            value=0,
                                            last=1)
        lstm_0_cell_reg_0_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                    name=self.net_name + '_split_lstm_0_cell_reg_0')

        lstm_0_cell_reg_1_i = axis.axis_reg(clk, reset,
                                            bus_m=lstm_0_cell,
                                            bus_s=lstm_0_cell_reg,
                                            clear_reg=0,
                                            set_reg=lstm_0_set_cell_1,
                                            value=0,
                                            last=1)
        lstm_0_cell_reg_1_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                    name=self.net_name + '_split_lstm_0_cell_reg_1')

        lstm_0_hidden_broadcast_i = axis.broadcast_2_comb(bus_a=connection_1,
                                                          bus_b=lstm_0_hidden_reg_0,
                                                          bus_in=lstm_0_hidden_next)
        lstm_0_hidden_broadcast_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                          name=self.net_name + '_split_lstm_0_hidden_broadcast')

        lstm_0_hidden_reg_0_i = axis.axis_reg(clk, reset,
                                              bus_m=lstm_0_hidden_reg_1,
                                              bus_s=lstm_0_hidden_reg_0,
                                              clear_reg=lstm_0_clear_hidden_0,
                                              set_reg=0,
                                              value=0,
                                              last=1)
        lstm_0_hidden_reg_0_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                      name=self.net_name + '_split_lstm_0_hidden_reg_0')

        lstm_0_hidden_reg_1_i = axis.axis_reg(clk, reset,
                                              bus_m=lstm_0_hidden,
                                              bus_s=lstm_0_hidden_reg_1,
                                              clear_reg=0,
                                              set_reg=lstm_0_set_hidden_1,
                                              value=0,
                                              last=1)
        lstm_0_hidden_reg_1_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                      name=self.net_name + '_split_lstm_0_hidden_reg_1')

        lstm_0 = LSTM(clk, reset,
                      in_data=connection_0,
                      out_data=connection_1,
                      coeffs=lstm_0_coeff,
                      sequence_length=self.seq_len,
                      sigmoid_type='hard',
                      fxp=self.fxp)
        lstm_0.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_lstm_0')

        # LSTM 1
        connection_2 = Axis(intbv()[self.fxp.width * lstm_1_coeff.layer.hidden_size:])

        lstm_1_hid_size = lstm_1_coeff.layer.hidden_size
        lstm_1_cell = Axis(intbv()[lstm_1_hid_size * self.fxp.width:])
        lstm_1_cell_reg = Axis(intbv()[lstm_1_hid_size * self.fxp.width:])
        lstm_1_cell_next = Axis(intbv()[lstm_1_hid_size * self.fxp.width:])

        lstm_1_hidden = Axis(intbv()[lstm_1_hid_size * self.fxp.width:])
        lstm_1_hidden_next = Axis(intbv()[lstm_1_hid_size * self.fxp.width:])
        lstm_1_hidden_reg_0 = Axis(intbv()[lstm_1_hid_size * self.fxp.width:])
        lstm_1_hidden_reg_1 = Axis(intbv()[lstm_1_hid_size * self.fxp.width:])

        lstm_1_clear_cell_0 = Signal(bool(0))
        lstm_1_set_cell_1 = Signal(bool(0))
        lstm_1_clear_hidden_0 = Signal(bool(0))
        lstm_1_set_hidden_1 = Signal(bool(0))

        # LSTM 0 cell
        lstm_1_cell_linear_ih_result = Axis(intbv()[lstm_1_coeff.layer.hidden_size * 4 * self.fxp.width:])
        lstm_1_cell_linear_hh_result = Axis(intbv()[lstm_1_coeff.layer.hidden_size * 4 * self.fxp.width:])
        lstm_1_cell_linear_sum_result = Axis(intbv()[lstm_1_coeff.layer.hidden_size * 4 * self.fxp.width:])
        lstm_1_cell_gate_i = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_gate_f = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_gate_g = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_gate_o = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_gate_i_act = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_gate_f_act = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_gate_g_act = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_gate_o_act = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_mul_ig = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_mul_fc_prev = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_c_next = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_c_next_raw = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])
        lstm_1_cell_c_next_act = Axis(intbv()[lstm_1_coeff.layer.hidden_size * self.fxp.width:])

        lstm_1_cell_sum_i = []
        lstm_1_cell_mul_i = []
        lstm_1_cell_axis_i = []

        lstm_1_cell_linear_ih = nn.Linear(in_features=lstm_1_coeff.layer.input_size,
                                          out_features=lstm_1_coeff.layer.hidden_size * 4)
        lstm_1_cell_coeffs_ih = LinearCoeffs(from_torch=lstm_1_cell_linear_ih, fxp=self.fxp)
        lstm_1_cell_coeffs_ih.weight = lstm_1_coeff.weight_ih
        lstm_1_cell_coeffs_ih.bias = lstm_1_coeff.bias_ih
        lstm_1_cell_linear_hh = nn.Linear(in_features=lstm_1_coeff.layer.hidden_size,
                                          out_features=lstm_1_coeff.layer.hidden_size * 4)
        lstm_1_cell_coeffs_hh = LinearCoeffs(from_torch=lstm_1_cell_linear_hh, fxp=self.fxp)
        lstm_1_cell_coeffs_hh.weight = lstm_1_coeff.weight_hh
        lstm_1_cell_coeffs_hh.bias = lstm_1_coeff.bias_hh

        # W_{i} x_t + b_{i} + W_{h} h_{(t-1)} + b_{h}
        lstm_1_cell_lin_ih = linear(clk, reset, in_fm=connection_1, out_fm=lstm_1_cell_linear_ih_result,
                                    coeffs=lstm_1_cell_coeffs_ih, fxp=self.fxp)
        lstm_1_cell_lin_ih.convert(hdl='VHDL', path=self.vhdl_output_path,
                                   name=self.net_name + '_split_lstm_1_lstm_cell_lin_ih')
        lstm_1_cell_lin_hh = linear(clk, reset, in_fm=lstm_1_hidden, out_fm=lstm_1_cell_linear_hh_result,
                                    coeffs=lstm_1_cell_coeffs_hh, fxp=self.fxp)
        lstm_1_cell_lin_hh.convert(hdl='VHDL', path=self.vhdl_output_path,
                                   name=self.net_name + '_split_lstm_1_lstm_cell_lin_hh')
        lstm_1_cell_sum_i.append(
            axis.elementwise_sum_comb(in_a=lstm_1_cell_linear_ih_result, in_b=lstm_1_cell_linear_hh_result,
                                      out_fm=lstm_1_cell_linear_sum_result, fxp=self.fxp))
        lstm_1_cell_sum_i[0].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_1_lstm_cell_sum_i_0')

        lstm_1_cell_axis_i.append(
            axis.split_4_comb(bus_a=lstm_1_cell_gate_i, bus_b=lstm_1_cell_gate_f, bus_c=lstm_1_cell_gate_g,
                              bus_d=lstm_1_cell_gate_o, bus_in=lstm_1_cell_linear_sum_result))
        lstm_1_cell_axis_i[0].convert(hdl='VHDL', path=self.vhdl_output_path,
                                      name=self.net_name + '_split_lstm_1_lstm_cell_axis_i_0')

        # i_t = \sigma(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi})
        lstm_1_cell_act_i = hard_sigmoid(clk, reset, in_fm=lstm_1_cell_gate_i, out_fm=lstm_1_cell_gate_i_act,
                                         fxp=self.fxp)
        lstm_1_cell_act_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                  name=self.net_name + '_split_lstm_1_lstm_cell_act_i')
        # f_t = \sigma(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf})
        lstm_1_cell_act_f = hard_sigmoid(clk, reset, in_fm=lstm_1_cell_gate_f, out_fm=lstm_1_cell_gate_f_act,
                                         fxp=self.fxp)
        lstm_1_cell_act_f.convert(hdl='VHDL', path=self.vhdl_output_path,
                                  name=self.net_name + '_split_lstm_1_lstm_cell_act_f')
        # g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hg} h_{(t-1)} + b_{hg})
        lstm_1_cell_act_g = tanh(clk, reset, in_fm=lstm_1_cell_gate_g, out_fm=lstm_1_cell_gate_g_act,
                                 implementation='linear', fxp=self.fxp)
        lstm_1_cell_act_g.convert(hdl='VHDL', path=self.vhdl_output_path,
                                  name=self.net_name + '_split_lstm_1_lstm_cell_act_g')
        # o_t = \sigma(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho})
        lstm_1_cell_act_o = hard_sigmoid(clk, reset, in_fm=lstm_1_cell_gate_o, out_fm=lstm_1_cell_gate_o_act,
                                         fxp=self.fxp)
        lstm_1_cell_act_o.convert(hdl='VHDL', path=self.vhdl_output_path,
                                  name=self.net_name + '_split_lstm_1_lstm_cell_act_o')

        # c_t = f_t c_{(t-1)} + i_t g_t
        lstm_1_cell_mul_i.append(axis.elementwise_mul_comb(in_a=lstm_1_cell_gate_i_act, in_b=lstm_1_cell_gate_g_act,
                                                           out_fm=lstm_1_cell_mul_ig, fxp=self.fxp))
        lstm_1_cell_mul_i[0].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_1_lstm_cell_mul_i_0')
        lstm_1_cell_mul_i.append(
            axis.elementwise_mul_comb(in_a=lstm_1_cell_gate_f_act, in_b=lstm_1_cell, out_fm=lstm_1_cell_mul_fc_prev,
                                      fxp=self.fxp))
        lstm_1_cell_mul_i[1].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_1_lstm_cell_mul_i_1')
        lstm_1_cell_sum_i.append(
            axis.elementwise_sum_comb(in_a=lstm_1_cell_mul_ig, in_b=lstm_1_cell_mul_fc_prev, out_fm=lstm_1_cell_c_next,
                                      fxp=self.fxp))
        lstm_1_cell_sum_i[1].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_1_lstm_cell_sum_i_1')

        lstm_1_cell_axis_i.append(
            axis.broadcast_2_comb(bus_a=lstm_1_cell_c_next_raw, bus_b=lstm_1_cell_next, bus_in=lstm_1_cell_c_next))
        lstm_1_cell_axis_i[1].convert(hdl='VHDL', path=self.vhdl_output_path,
                                      name=self.net_name + '_split_lstm_1_lstm_cell_axis_i_1')

        # h_t = o_t \tanh(c_t)
        lstm_1_cell_act_out = tanh(clk, reset, in_fm=lstm_1_cell_c_next_raw, out_fm=lstm_1_cell_c_next_act,
                                   implementation='linear', fxp=self.fxp)
        lstm_1_cell_act_out.convert(hdl='VHDL', path=self.vhdl_output_path,
                                    name=self.net_name + '_split_lstm_1_lstm_cell_act_out')
        lstm_1_cell_mul_i.append(axis.elementwise_mul_comb(in_a=lstm_1_cell_c_next_act, in_b=lstm_1_cell_gate_o_act,
                                                           out_fm=lstm_1_hidden_next, fxp=self.fxp))
        lstm_1_cell_mul_i[2].convert(hdl='VHDL', path=self.vhdl_output_path,
                                     name=self.net_name + '_split_lstm_1_lstm_cell_mul_i_2')

        lstm_1_lstm_cell_i = LSTM_cell(clk, reset,
                                       in_data=connection_1,
                                       in_cell=lstm_1_cell,
                                       in_hidden=lstm_1_hidden,
                                       out_cell=lstm_1_cell_next,
                                       out_hidden=lstm_1_hidden_next,
                                       coeffs=lstm_1_coeff,
                                       sigmoid_type='hard',
                                       fxp=self.fxp)
        lstm_1_lstm_cell_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                   name=self.net_name + '_split_lstm_1_lstm_cell')

        lstm_1_cell_reg_0_i = axis.axis_reg(clk, reset,
                                            bus_m=lstm_1_cell_reg,
                                            bus_s=lstm_1_cell_next,
                                            clear_reg=lstm_1_clear_cell_0,
                                            set_reg=0,
                                            value=0,
                                            last=1)
        lstm_1_cell_reg_0_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                    name=self.net_name + '_split_lstm_1_cell_reg_0')

        lstm_1_cell_reg_1_i = axis.axis_reg(clk, reset,
                                            bus_m=lstm_1_cell,
                                            bus_s=lstm_1_cell_reg,
                                            clear_reg=0,
                                            set_reg=lstm_1_set_cell_1,
                                            value=0,
                                            last=1)
        lstm_1_cell_reg_1_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                    name=self.net_name + '_split_lstm_1_cell_reg_1')

        lstm_1_hidden_broadcast_i = axis.broadcast_2_comb(bus_a=connection_2,
                                                          bus_b=lstm_1_hidden_reg_0,
                                                          bus_in=lstm_1_hidden_next)
        lstm_1_hidden_broadcast_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                          name=self.net_name + '_split_lstm_1_hidden_broadcast')

        lstm_1_hidden_reg_0_i = axis.axis_reg(clk, reset,
                                              bus_m=lstm_1_hidden_reg_1,
                                              bus_s=lstm_1_hidden_reg_0,
                                              clear_reg=lstm_1_clear_hidden_0,
                                              set_reg=0,
                                              value=0,
                                              last=1)
        lstm_1_hidden_reg_0_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                      name=self.net_name + '_split_lstm_1_hidden_reg_0')

        lstm_1_hidden_reg_1_i = axis.axis_reg(clk, reset,
                                              bus_m=lstm_1_hidden,
                                              bus_s=lstm_1_hidden_reg_1,
                                              clear_reg=0,
                                              set_reg=lstm_1_set_hidden_1,
                                              value=0,
                                              last=1)
        lstm_1_hidden_reg_1_i.convert(hdl='VHDL', path=self.vhdl_output_path,
                                      name=self.net_name + '_split_lstm_1_hidden_reg_1')

        lstm_1 = LSTM(clk, reset,
                      in_data=connection_1,
                      out_data=connection_2,
                      coeffs=lstm_1_coeff,
                      sequence_length=self.seq_len,
                      sigmoid_type='hard',
                      fxp=self.fxp)
        lstm_1.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_lstm_1')

        # PKT BLOCK
        connection_3 = Axis(intbv()[self.fxp.width * linear_0_coeff.layer.in_features:])
        pkt_block = packet_block(clk, reset,
                                 bus_in=connection_2,
                                 bus_out=connection_3,
                                 pass_cntr=self.seq_len)
        pkt_block.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_pkt_block')

        # LINEAR
        connection_4 = Axis(intbv()[self.fxp.width * linear_0_coeff.layer.out_features:])
        lin_0 = linear(clk, reset,
                       in_fm=connection_3,
                       out_fm=connection_4,
                       coeffs=linear_0_coeff,
                       fxp=self.fxp)
        lin_0.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_linear_0')

        # SOFTMAX
        connection_5 = Axis(intbv()[len(connection_4.tdata):])
        smax = softmax(clk, reset,
                       in_fm=connection_4,
                       out_fm=connection_5,
                       fxp=self.fxp)
        smax.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_softmax')

        # TRANSMITTER
        txd = transmitter(clk, reset,
                          in_data=connection_5,
                          out_data=out_fm,
                          fxp=self.fxp)
        txd.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_transmitter')

        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')
