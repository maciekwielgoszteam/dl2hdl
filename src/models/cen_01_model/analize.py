from models.cen_01_model.transfer_weights import CernData
from models.cen_01_model.cern_model_net import TestCernModelNet
from dl2hdl.basic.fixed_def import FixedDef
from torch.utils.data import DataLoader
from config.config import config
import torch
import os
from matplotlib import pyplot as plt
import numpy as np
from models.cen_01_model.log import gate_log, cell_log, hidden_log


def weights_histogram():
    fxp = FixedDef(8, 6)
    model_path = config.get_path_out_model()
    test_sequences = 1
    seq_len = 256
    in_f = 4
    hid_f = (64, 32)
    out_dim = 2
    model = TestCernModelNet.TestModel(in_f, hid_f, out_dim)
    model_path = os.path.join(model_path, 'cern_01_model_net.pt')
    model.load_state_dict(torch.load(model_path))
    weights = {}
    weights['lstm_0_weights_hh'] = model.lstm_0.weight_hh_l0.data.numpy().flatten()
    weights['lstm_0_weights_ih'] = model.lstm_0.weight_ih_l0.data.numpy().flatten()
    weights['lstm_0_bias_hh'] = model.lstm_0.bias_hh_l0.data.numpy().flatten()
    weights['lstm_0_bias_ih'] = model.lstm_0.bias_ih_l0.data.numpy().flatten()
    weights['lstm_1_weights_hh'] = model.lstm_1.weight_hh_l0.data.numpy().flatten()
    weights['lstm_1_weights_ih'] = model.lstm_1.weight_ih_l0.data.numpy().flatten()
    weights['lstm_1_bias_hh'] = model.lstm_1.bias_hh_l0.data.numpy().flatten()
    weights['lstm_1_bias_ih'] = model.lstm_1.bias_ih_l0.data.numpy().flatten()
    weights['linear_weight'] = model.linear.weight.data.numpy().flatten()
    weights['linear_bias'] = model.linear.bias.data.numpy().flatten()

    bins = 'auto'

    plt.figure(1)

    plt.subplot(221)
    plt.hist(weights['lstm_0_weights_hh'], bins=bins)
    plt.title(f"lstm_0_weights_hh ({len(weights['lstm_0_weights_hh'])})")

    plt.subplot(222)
    plt.hist(weights['lstm_0_weights_ih'], bins=bins)
    plt.title(f"lstm_0_weights_ih ({len(weights['lstm_0_weights_ih'])})")

    plt.subplot(223)
    plt.hist(weights['lstm_0_bias_hh'], bins=bins)
    plt.title(f"lstm_0_bias_hh ({len(weights['lstm_0_bias_hh'])})")

    plt.subplot(224)
    plt.hist(weights['lstm_0_bias_ih'], bins=bins)
    plt.title(f"lstm_0_bias_ih ({len(weights['lstm_0_bias_ih'])})")

    plt.figure(2)

    plt.subplot(221)
    plt.hist(weights['lstm_1_weights_hh'], bins=bins)
    plt.title(f"lstm_1_weights_hh ({len(weights['lstm_1_weights_hh'])})")

    plt.subplot(222)
    plt.hist(weights['lstm_1_weights_ih'], bins=bins)
    plt.title(f"lstm_1_weights_ih ({len(weights['lstm_1_weights_ih'])})")

    plt.subplot(223)
    plt.hist(weights['lstm_1_bias_hh'], bins=bins)
    plt.title(f"lstm_1_bias_hh ({len(weights['lstm_1_bias_hh'])})")

    plt.subplot(224)
    plt.hist(weights['lstm_1_bias_ih'], bins=bins)
    plt.title(f"lstm_1_bias_ih ({len(weights['lstm_1_bias_ih'])})")

    plt.figure(3)

    plt.subplot(211)
    plt.hist(weights['linear_weight'], bins=bins)
    plt.title(f"linear_weight ({len(weights['linear_weight'])})")

    plt.subplot(212)
    plt.hist(weights['linear_bias'], bins=bins)
    plt.title(f"linear_bias ({len(weights['linear_bias'])})")

    plt.show()


def gates_log_analize():
    fxp = FixedDef(8, 6)
    model_path = config.get_path_out_model()
    test_sequences = 1
    seq_len = 256
    in_f = 4
    hid_f = (64, 32)
    out_dim = 2
    print('loading model')
    model = TestCernModelNet.TestModel(in_f, hid_f, out_dim)
    model_path = os.path.join(model_path, 'cern_01_model_net.pt')
    model.load_state_dict(torch.load(model_path))
    print('loading data')
    test_data_path = '/home/michal/agh/workspace/ml/pm-hardware/subsets/grided_test_100_set.npy'
    test_set = CernData(data_path=test_data_path)
    test_loader = DataLoader(test_set, batch_size=1, shuffle=False)
    print('running inference')
    model.eval()
    out_true = []
    with torch.no_grad():
        for data, target in test_loader:
            out_true.append(model(data))
    print('printing stats')
    print(f'len of test_set = {len(test_set)}')

    bins = 256
    np_gate_log = [x.numpy() for x in gate_log]
    np_gate_log_0 = []
    np_gate_log_1 = []
    for i in range(len(test_set)):
        for j in range(256):
            if j < 128:
                np_gate_log_0.append(np_gate_log[(i * 512) + (2 * j)])
            else:
                np_gate_log_1.append(np_gate_log[(i * 512) + (2 * j + 1)])
    plt.figure(1)
    gates_0_vals = np.stack(np_gate_log_0, axis=0).flatten()
    print(f'gates_0_vals ({len(gates_0_vals)}, nim = {np.min(gates_0_vals)}, max = {np.max(gates_0_vals)})')
    plt.hist(gates_0_vals, bins=bins, weights=np.ones(len(gates_0_vals)) / len(gates_0_vals))
    plt.title(f"gates_0_vals ({len(gates_0_vals)})")
    plt.figure(2)
    gates_1_vals = np.stack(np_gate_log_1, axis=0).flatten()
    print(f'gates_1_vals ({len(gates_1_vals)}, nim = {np.min(gates_1_vals)}, max = {np.max(gates_1_vals)})')
    plt.hist(gates_1_vals, bins=bins, weights=np.ones(len(gates_1_vals)) / len(gates_1_vals))
    plt.title(f"gates_1_vals ({len(gates_1_vals)})")

    np_cell_log = [x.numpy() for x in cell_log]
    np_cell_log_0 = []
    np_cell_log_1 = []
    for i in range(len(test_set)):
        for j in range(256):
            if j < 128:
                np_cell_log_0.append(np_cell_log[(i * 512) + (2 * j)])
            else:
                np_cell_log_1.append(np_cell_log[(i * 512) + (2 * j + 1)])
    plt.figure(3)
    cell_0_vals = np.stack(np_cell_log_0, axis=0).flatten()
    print(f'cell_0_vals ({len(cell_0_vals)}, nim = {np.min(cell_0_vals)}, max = {np.max(cell_0_vals)})')
    plt.hist(cell_0_vals, bins=bins, weights=np.ones(len(cell_0_vals)) / len(cell_0_vals))
    plt.title(f"cell_0_vals ({len(cell_0_vals)})")
    plt.figure(4)
    cell_1_vals = np.stack(np_cell_log_1, axis=0).flatten()
    print(f'cell_1_vals ({len(cell_1_vals)}, nim = {np.min(cell_1_vals)}, max = {np.max(cell_1_vals)})')
    plt.hist(cell_1_vals, bins=bins, weights=np.ones(len(cell_1_vals)) / len(cell_1_vals))
    plt.title(f"cell_1_vals ({len(cell_1_vals)})")

    np_hidden_log = [x.numpy() for x in hidden_log]
    np_hidden_log_0 = []
    np_hidden_log_1 = []
    for i in range(len(test_set)):
        for j in range(256):
            if j < 128:
                np_hidden_log_0.append(np_hidden_log[(i * 512) + (2 * j)])
            else:
                np_hidden_log_1.append(np_hidden_log[(i * 512) + (2 * j + 1)])
    plt.figure(5)
    hidden_0_vals = np.stack(np_hidden_log_0, axis=0).flatten()
    print(f'hidden_0_vals ({len(hidden_0_vals)}, nim = {np.min(hidden_0_vals)}, max = {np.max(hidden_0_vals)})')
    plt.hist(hidden_0_vals, bins=bins, weights=np.ones(len(hidden_0_vals)) / len(hidden_0_vals))
    plt.title(f"hidden_0_vals ({len(hidden_0_vals)})")
    plt.figure(6)
    hidden_1_vals = np.stack(np_hidden_log_1, axis=0).flatten()
    print(f'hidden_1_vals ({len(hidden_1_vals)}, nim = {np.min(hidden_1_vals)}, max = {np.max(hidden_1_vals)})')
    plt.hist(hidden_1_vals, bins=bins, weights=np.ones(len(hidden_1_vals)) / len(hidden_1_vals))
    plt.title(f"hidden_1_vals ({len(hidden_1_vals)})")

    plt.show()


if __name__ == '__main__':
    # weights_histogram()
    gates_log_analize()
