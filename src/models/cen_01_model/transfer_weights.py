from keras.models import load_model
from config.config import config
import os
import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader
import numpy as np
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from models.cen_01_model.model import CernModel_01
from models.cen_01_model.data import CernData


def transfer_keras_to_pytorch():
    model_path = config.get_path_out_model()
    keras_model_path = os.path.join(model_path, 'model-441f0bf2-31d7-11e9-bec5-001e67e41b91.h5')
    test_data_path = '/media/michal/storage/datasets/pm-hardware/subsets/grided_test_set.npy'
    input_channels = [0, 1, 2, 3]
    output_channel = 4
    keras_model = load_model(keras_model_path)
    keras_model.summary()
    keras_weights = keras_model.get_weights()
    torch_model = CernModel_01(4, (64, 32), 2)
    torch_model.lstm_0.weight_ih_l0.data = torch.from_numpy(np.transpose(keras_weights[0]).astype(np.float32))
    torch_model.lstm_0.weight_hh_l0.data = torch.from_numpy(np.transpose(keras_weights[1]).astype(np.float32))
    torch_model.lstm_0.bias_ih_l0.data = torch.from_numpy(np.transpose(keras_weights[2]).astype(np.float32))
    torch_model.lstm_0.bias_hh_l0.data = torch.from_numpy(np.transpose(np.zeros(keras_weights[2].shape)).astype(np.float32))
    torch_model.lstm_1.weight_ih_l0.data = torch.from_numpy(np.transpose(keras_weights[3]).astype(np.float32))
    torch_model.lstm_1.weight_hh_l0.data = torch.from_numpy(np.transpose(keras_weights[4]).astype(np.float32))
    torch_model.lstm_1.bias_ih_l0.data = torch.from_numpy(np.transpose(keras_weights[5]).astype(np.float32))
    torch_model.lstm_1.bias_hh_l0.data = torch.from_numpy(np.transpose(np.zeros(keras_weights[5].shape)).astype(np.float32))
    torch_model.linear.weight.data = torch.from_numpy(np.transpose(keras_weights[6]).astype(np.float32))
    torch_model.linear.bias.data = torch.from_numpy(np.transpose(keras_weights[7]).astype(np.float32))

    test_data = np.load(test_data_path)[0:10]

    keras_encoded_predicted_y = keras_model.predict(test_data[:, :-1, input_channels], batch_size=1, verbose=1)
    print('Keras output:')
    print(keras_encoded_predicted_y)
    keras_predicted_y = np.argmax(keras_encoded_predicted_y, axis=2).reshape((-1, 1))

    keras_real_y = test_data[:, -1, output_channel]

    keras_acc = accuracy_score(keras_real_y, keras_predicted_y)
    keras_precision, keras_recall, keras_fscore, _ = precision_recall_fscore_support(keras_real_y, keras_predicted_y, average='binary')

    print("Keras Accuracy: {}".format(keras_acc))
    print("Keras Precision: {}".format(keras_precision))
    print("Keras Recall: {}".format(keras_recall))
    print("Keras F1 score: {}".format(keras_fscore))

    torch_test_data = CernData(dataset=test_data)
    torch_test_data_loader = DataLoader(torch_test_data, batch_size=1)
    torch_model.eval()
    torch_test_loss = 0
    torch_correct = 0
    torch_real_y = np.ndarray(shape=(0,))
    torch_encoded_predicted_y = []
    torch_predicted_y = np.ndarray(shape=(0,))
    with torch.no_grad():
        for data, target in torch_test_data_loader:
            output = torch_model(data)
            torch_encoded_predicted_y.append(output.numpy())
            torch_test_loss += F.cross_entropy(output, target, reduction='sum').item()  # sum up batch loss
            pred = output.max(1, keepdim=True)[1]  # get the index of the max log-probability
            torch_correct += pred.eq(target.view_as(pred)).sum().item()
            torch_real_y = np.append(torch_real_y, target.cpu().numpy())
            torch_predicted_y = np.append(torch_predicted_y, pred.cpu().numpy())

    torch_encoded_predicted_y = np.stack(torch_encoded_predicted_y, axis=0)
    print('Torch output:')
    print(torch_encoded_predicted_y)

    torch_acc = accuracy_score(torch_real_y, torch_predicted_y)
    torch_precision, torch_recall, torch_fscore, _ = precision_recall_fscore_support(torch_real_y, torch_predicted_y, average='binary')

    print("Torch Accuracy: {}".format(torch_acc))
    print("Torch Precision: {}".format(torch_precision))
    print("Torch Recall: {}".format(torch_recall))
    print("Torch F1 score: {}".format(torch_fscore))

    print('Diff array:')
    diff = np.abs(keras_encoded_predicted_y - torch_encoded_predicted_y)
    print(diff)
    print(f'Average diff = {np.mean(diff)}')

    torch_model_path = os.path.join(model_path, 'cern_01_model_net.pt')
    torch.save(torch_model.state_dict(), torch_model_path)


if __name__ == '__main__':
    transfer_keras_to_pytorch()
