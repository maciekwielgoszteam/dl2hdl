import torch
import torch.nn as nn
from ml.layers.lstm_hard import LSTMHardSigmoid


class CernModel_01(nn.Module):

    def __init__(self, in_dim, hidden_dim, out_dim):
        super().__init__()
        self.hidden_dim = hidden_dim
        self.lstm_0 = LSTMHardSigmoid(input_size=in_dim, hidden_size=hidden_dim[0], batch_first=True)
        self.lstm_1 = LSTMHardSigmoid(input_size=hidden_dim[0], hidden_size=hidden_dim[1], batch_first=True)
        self.linear = nn.Linear(in_features=hidden_dim[1], out_features=out_dim)
        self.softmax = nn.Softmax(dim=1)

    def _init_hidden(self, device, batch_size=1):
        return ((torch.zeros(1, batch_size, self.hidden_dim[0]).to(device), torch.zeros(1, batch_size, self.hidden_dim[0]).to(device)),
                (torch.zeros(1, batch_size, self.hidden_dim[1]).to(device), torch.zeros(1, batch_size, self.hidden_dim[1]).to(device)))

    def forward(self, input_seq):
        hidden_init = self._init_hidden(input_seq.device, input_seq.shape[0])
        self.lstm_0.flatten_parameters()
        self.lstm_1.flatten_parameters()
        outputs, hidden = self.lstm_0(input_seq, hidden_init[0])
        outputs, hidden = self.lstm_1(outputs, hidden_init[1])
        outputs = self.linear(outputs[:, -1, :])
        outputs = self.softmax(outputs)
        return outputs