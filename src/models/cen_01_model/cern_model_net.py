from dl2hdl.tests.hdl_test import HdlTest
from unittest import skip
import warnings
import torch
import numpy as np
import os
import time
from torch.utils.data import DataLoader
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.axi.axis import Axis
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from myhdl import block, instances, StopSimulation, intbv, instance, Signal, delay, ResetSignal
from dl2hdl.basic.cpp_if import torch_data_set_to_cpp_string, numpy_ndaray_to_cpp_str
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.model import MyHdlModel
from models.cen_01_model.data import CernData
from models.cen_01_model.model import CernModel_01


class TestCernModelNet(HdlTest):

    @staticmethod
    def infer_net(model, data_set, device=torch.device('cpu')):
        test_loader = DataLoader(data_set, batch_size=1, shuffle=False)
        model.eval()
        out_true = []
        with torch.no_grad():
            for data, target in test_loader:
                data, target = data.to(device), target.to(device)
                out_true.append(model(data))
        return out_true

    def _single_run(self, test_data, model, fxp=None):
        @block
        def model_tb(data, out_data, model, fxp=None):
            axis_width = 32
            in_fm = Axis(intbv()[axis_width:])
            out_fm = Axis(intbv()[axis_width:])
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)

            stim_write = Axis.write(clk, reset, in_fm, data.numpy(), fxp, verbose=True)

            hdl_model = MyHdlModel(model, seq_len=data.shape[1], fxp=fxp)
            uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)

            @instance
            def stim_read():
                reset.next = 0
                yield delay(24)
                reset.next = 1
                yield delay(24)
                yield clk.negedge
                out_fm.tready.next = 1
                for _ in range(data.shape[0]):
                    out_row = []
                    while True:
                        yield clk.negedge
                        if out_fm.tvalid == 1:
                            out_row.append(out_fm.tdata.signed())
                            if out_fm.tlast == 1:
                                break
                    out_data.append(out_row)

                for i in range(10):
                    yield clk.negedge

                reset.next = 0

                for i in range(2):
                    yield clk.negedge

                raise StopSimulation()

            return instances()

        out_data = []
        tb = model_tb(data=test_data, out_data=out_data, model=model, fxp=fxp)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return out_data

    def setUp(self):
        super().setUp()
        self.fxp = FixedDef(8, 6)
        # self.test_sequences = 1
        self.seq_len = 256
        self.in_f = 4
        self.hid_f = (64, 32)
        self.out_dim = 2

    @skip
    def test_01_net_sim(self):
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: testing {self.seq_len}-{self.in_f}-{self.hid_f[0]}-{self.hid_f[1]}-{self.out_dim}')
        test_data_path = '/media/michal/storage/datasets/pm-hardware/subsets/grided_test_set.npy'
        test_set = CernData(data_path=test_data_path, out_len=1)
        # test_set = RandomBin(shape=(self.seq_len, self.in_f), num_of_samples=self.test_sequences, seed=42)
        model = CernModel_01(self.in_f, self.hid_f, self.out_dim)
        model_path = os.path.join(self.model_path, 'cern_01_model_net.pt')
        model.load_state_dict(torch.load(model_path))
        out_true = self.infer_net(model, test_set)
        out_true = [each.tolist() for each in out_true[0]]
        out_data = self._single_run(test_set.data, model, fxp=self.fxp)
        out_data = [[self.fxp.to_float(y) for y in x] for x in out_data]

        test_set.data = test_set.data.view(*test_set.data.shape[1:])
        out_true_np = np.asarray(out_true)
        out_data_np = np.asarray(out_data)
        path = os.path.join(self.data_path, 'cern_01_model_net_data')
        file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=self.fxp)
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_data_np), 'outputs')
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_true_np), 'outputs_true')
        os.makedirs(path, exist_ok=True)
        f = open(os.path.join(path, 'test_data.h'), 'w')
        print(file_str, file=f)
        f.close()

        numpy_array_to_txt_vec(self.fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(self.fxp.to_fixed(out_data_np), path, 'test_output')

        decimal = 5
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        print(f'out_data:\n{out_data}')
        print(f'out_true:\n{out_true}')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    # @skip
    def test_02_net_conv(self):
        model = CernModel_01(self.in_f, self.hid_f, self.out_dim)
        model_path = os.path.join(self.model_path, 'cern_01_model_net.pt')
        model.load_state_dict(torch.load(model_path))
        axis_width = 32
        in_fm = Axis(intbv()[axis_width:])
        out_fm = Axis(intbv()[axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)
        hdl_model = MyHdlModel(model, seq_len=self.seq_len, fxp=self.fxp)
        uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Converting...')
        uut.convert(hdl='VHDL', path=self.vhdl_output_path, name='cern_01_model_net')
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')
