import torch
from torch.utils.data import Dataset
import numpy as np


class CernData(Dataset):
    channels = ["U_RES", "U_DIFF", "I_DCCT", "I_DIDT", "quench_marks"]
    input_channels = [0, 1, 2, 3]
    output_channel = 4

    def __init__(self, dataset=None, data_path=None, out_len=None, ratio=1.0):
        if data_path is not None:
            data = np.load(data_path)
        else:
            data = dataset
        data = data[:, :-1, :]
        if out_len is None:
            assert 0.0 < ratio <= 1.0
            out_len = int(len(data) * ratio)
        else:
            assert isinstance(out_len, int)
        self.data = torch.from_numpy(data[:out_len, :, self.input_channels]).float()
        self.labels = data[:out_len, -1, self.output_channel].astype('int')

    def __getitem__(self, item):
        return self.data[item], self.labels[item]

    def __len__(self):
        return self.data.shape[0]
