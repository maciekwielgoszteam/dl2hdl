from dl2hdl.tests.hdl_test import HdlTest
from unittest import skip
import warnings
import torch
import numpy as np
import os
import time
import torch.nn as nn
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.axi.axis import Axis
from myhdl import intbv, Signal, ResetSignal
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.model import MyHdlModel
from dl2hdl.layers.tests.torch_dataset import RandomSeries


class LeFlowComparison(HdlTest):
    class _TestModel(nn.Module):

        def __init__(self, in_features, out_features):
            super().__init__()
            self.classifier = nn.Sequential()
            self.classifier.add_module('linear', nn.Linear(in_features=in_features, out_features=out_features))
            self.classifier.add_module(f'relu', nn.ReLU())

        def forward(self, x):
            x = self.classifier(x)
            return x

    class _SoftmaxModel(nn.Module):
        def __init__(self):
            super().__init__()
            self.classifier = nn.Sequential()
            self.classifier.add_module('softmax', nn.Softmax(dim=-1))

        def forward(self, x):
            x = self.classifier(x)
            return x

    def setUp(self):
        super().setUp()
        self.fxp = FixedDef(8, 24)
        self.in_features = 1
        self.out_features = 64

    # @skip
    def test_01_net_sim(self):
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: testing {self.in_features}-{self.out_features}')
        test_set = RandomSeries(shape=(self.in_features,), num_of_samples=1)
        model = self._TestModel(self.in_features, self.out_features)
        model_path = os.path.join(self.model_path, 'leflow_model_net.pt')
        torch.save(model.state_dict(), model_path)
        hdl_model = MyHdlModel(model, fxp=self.fxp)
        out_true = self.infer_net(model, test_set)
        out_true = [each[0].tolist() for each in out_true]
        out_data = self._single_run(test_set.data, hdl_model, fxp=self.fxp)
        out_data = [[self.fxp.to_float(y) for y in x] for x in out_data]

        out_true_np = np.asarray(out_true)
        out_data_np = np.asarray(out_data)
        path = os.path.join(self.data_path, 'leflow_model_net_data')
        os.makedirs(path, exist_ok=True)
        # file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        # file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        # file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        # file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        # file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=self.fxp)
        # file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_data_np), 'outputs')
        # file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_true_np), 'outputs_true')
        # f = open(os.path.join(path, 'test_data.h'), 'w')
        # print(file_str, file=f)
        # f.close()

        numpy_array_to_txt_vec(self.fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(self.fxp.to_fixed(out_data_np), path, 'test_output')

        decimal = 5
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        print(f'out_data:\n{out_data}')
        print(f'out_true:\n{out_true}')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    # @skip
    def test_02_net_conv(self):
        model = self._TestModel(self.in_features, self.out_features)
        model_path = os.path.join(self.model_path, 'leflow_model_net.pt')
        model.load_state_dict(torch.load(model_path))
        axis_width = 32
        in_fm = Axis(intbv()[axis_width:])
        out_fm = Axis(intbv()[axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)
        hdl_model = MyHdlModel(model, fxp=self.fxp)
        uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Converting...')
        uut.convert(hdl='VHDL', path=self.vhdl_output_path, name='leflow_model_net')
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')

    # @skip
    def test_03_softmax_sim(self):
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: testing {self.in_features}-{self.out_features}')
        test_set = RandomSeries(shape=(self.out_features,), num_of_samples=1)
        model = self._SoftmaxModel()
        model_path = os.path.join(self.model_path, 'leflow_model_net.pt')
        torch.save(model.state_dict(), model_path)
        hdl_model = MyHdlModel(model, fxp=self.fxp)
        out_true = self.infer_net(model, test_set)
        out_true = [each[0].tolist() for each in out_true]
        out_data = self._single_run(test_set.data, hdl_model, fxp=self.fxp)
        out_data = [[self.fxp.to_float(y) for y in x] for x in out_data]

        out_true_np = np.asarray(out_true)
        out_data_np = np.asarray(out_data)
        path = os.path.join(self.data_path, 'leflow_model_net_data')
        os.makedirs(path, exist_ok=True)
        # file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        # file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        # file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        # file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        # file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=self.fxp)
        # file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_data_np), 'outputs')
        # file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_true_np), 'outputs_true')
        # f = open(os.path.join(path, 'test_data.h'), 'w')
        # print(file_str, file=f)
        # f.close()

        numpy_array_to_txt_vec(self.fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(self.fxp.to_fixed(out_data_np), path, 'test_output')

        decimal = 5
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        print(f'out_data:\n{out_data}')
        print(f'out_true:\n{out_true}')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    # @skip
    def test_04_softmax_conv(self):
        model = self._TestModel(self.in_features, self.out_features)
        model_path = os.path.join(self.model_path, 'leflow_model_net.pt')
        model.load_state_dict(torch.load(model_path))
        axis_width = 32
        in_fm = Axis(intbv()[axis_width:])
        out_fm = Axis(intbv()[axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)
        hdl_model = MyHdlModel(model, fxp=self.fxp)
        uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Converting...')
        uut.convert(hdl='VHDL', path=self.vhdl_output_path, name='leflow_model_net')
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')
