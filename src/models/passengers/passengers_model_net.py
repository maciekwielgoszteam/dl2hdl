from dl2hdl.tests.hdl_test import HdlTest
from unittest import skip
import warnings
import torch
import numpy as np
import os
import time
from torch.utils.data import DataLoader
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.axi.axis import Axis
from dl2hdl.myhdl_sim.clk_stim import clk_stim
from dl2hdl.myhdl_sim.reset_sim import aresetn_stim
from myhdl import block, instances, StopSimulation, intbv, instance, Signal, delay, ResetSignal
from dl2hdl.basic.cpp_if import torch_data_set_to_cpp_string, numpy_ndaray_to_cpp_str
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.model import MyHdlModel
from models.passengers.data import PassengersData
from models.passengers.model import PassengersModel
from dl2hdl.layers.tests.torch_dataset import RandomSeries
from myhdl._Signal import _Signal


class TestPassengersModelNet(HdlTest):

    def _single_run(self, test_data, hdl_model, fxp=None):
        @block
        def model_tb(data, out_data, hdl_model, fxp=None):
            axis_width = 32
            in_fm = Axis(intbv()[axis_width:])
            out_fm = Axis(intbv()[axis_width:])
            clk = Signal(bool(0))
            reset = ResetSignal(0, active=0, isasync=False)
            clk_gen = clk_stim(clk, period=10)

            @instance
            def sim():
                assert isinstance(fxp, FixedDef), 'You have to provide valid fixed point definition'
                yield reset.posedge
                yield clk.negedge

                in_fm.tvalid.next = 1
                in_fm.tlast.next = 0
                data_f = data.numpy().reshape(-1, data.shape[-1])
                for sample_idx, samples_nested in enumerate(data_f, 1):
                    if True:
                        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Sending sample {sample_idx}/{len(data_f)}')
                    idx = 0
                    if isinstance(samples_nested, _Signal):  # FIXME: do not use protected member
                        sample = [samples_nested]
                    elif isinstance(samples_nested, np.ndarray):
                        sample = np.asarray(samples_nested).flatten()
                        sample = [intbv(fxp.to_fixed(x))[fxp.width:].unsigned() for x in sample]
                    else:
                        raise TypeError

                    in_fm.tdata.next = sample[idx]
                    sent = False
                    while not sent:
                        yield clk.negedge
                        if in_fm.tready == 1:
                            if idx == len(sample) - 1:
                                sent = True
                            else:
                                idx += 1
                        if idx == len(sample) - 1:
                            in_fm.tlast.next = 1
                        if sent:
                            if len(sample) == 1:
                                in_fm.tlast.next = 1
                            else:
                                in_fm.tlast.next = 0
                        in_fm.tdata.next = sample[idx]
                in_fm.tvalid.next = 0

            uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)

            @instance
            def stim_read():
                reset.next = 0
                yield delay(24)
                reset.next = 1
                yield delay(24)
                yield clk.negedge
                out_fm.tready.next = 1
                for _ in range(data.shape[0]):
                    out_row = []
                    while True:
                        yield clk.negedge
                        if out_fm.tvalid == 1:
                            out_row.append(out_fm.tdata.signed())
                            if out_fm.tlast == 1:
                                break
                    out_data.append(out_row)

                for i in range(10):
                    yield clk.negedge

                reset.next = 0

                for i in range(2):
                    yield clk.negedge

                raise StopSimulation()

            return instances()

        out_data = []
        tb = model_tb(data=test_data, out_data=out_data, hdl_model=hdl_model, fxp=fxp)
        tb.config_sim(trace=True, directory=self.trace_save_path)
        tb.run_sim()
        return out_data

    def prun(self, model, prun=None):
        # TODO: fix bias pruning
        prun = prun if prun is not None else self.pruning
        self.prun_weight(model.lstm.weight_ih_l0, prun)
        self.prun_weight(model.lstm.weight_hh_l0, prun)
        # self.prun_weight(model.lstm_0.bias_ih_l0, prun)
        # self.prun_weight(model.lstm_0.bias_ih_l0, prun)
        self.prun_weight(model.linear.weight, prun)
        # self.prun_weight(model.linear.bias, prun)
        return model

    def setUp(self):
        super().setUp()
        self.fxp = FixedDef(3, 10)
        self.test_sequences = 1
        self.seq_len = 5
        self.in_f = 1
        self.hid_f = 4
        self.out_dim = 1
        self.pruning = 0.2764

    # @skip
    def test_01_net_sim(self):
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: testing {self.seq_len}-{self.in_f}-{self.hid_f}-{self.out_dim}')
        test_data_path = "/home/michal/datasets/kaggle_airline_passengers/international-airline-passengers.csv"
        # test_set = PassengersData(data_path=test_data_path, lookback=self.seq_len, out_len=self.test_sequences)
        test_set = RandomSeries(shape=(self.seq_len, self.in_f), num_of_samples=self.test_sequences)
        trained_model = PassengersModel(self.in_f, self.hid_f, self.out_dim)
        model_path = os.path.join(self.model_path, 'passengers_model_net.pt')
        trained_model.load_state_dict(torch.load(model_path))
        model = self.prun(trained_model)
        model_path = os.path.join(self.model_path, 'passengers_model_net_v0.pt')
        torch.save(model.state_dict(), model_path)
        hdl_model = MyHdlModel(model, seq_len=self.seq_len, fxp=self.fxp)
        out_true = self.infer_net(model, test_set)
        out_true = [each[0].tolist() for each in out_true]
        out_data = self._single_run(test_set.data, hdl_model, fxp=self.fxp)
        out_data = [[self.fxp.to_float(y) for y in x] for x in out_data]

        out_true_np = np.asarray(out_true)
        out_data_np = np.asarray(out_data)
        path = os.path.join(self.data_path, 'passengers_model_net_data')
        os.makedirs(path, exist_ok=True)
        # file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        # file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        # file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        # file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        # file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=self.fxp)
        # file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_data_np), 'outputs')
        # file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_true_np), 'outputs_true')
        # f = open(os.path.join(path, 'test_data.h'), 'w')
        # print(file_str, file=f)
        # f.close()

        numpy_array_to_txt_vec(self.fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(self.fxp.to_fixed(out_data_np), path, 'test_output')

        decimal = 5
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        print(f'out_data:\n{out_data}')
        print(f'out_true:\n{out_true}')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    # @skip
    def test_02_net_conv(self):
        model = PassengersModel(self.in_f, self.hid_f, self.out_dim)
        model_path = os.path.join(self.model_path, 'passengers_model_net_v0.pt')
        model.load_state_dict(torch.load(model_path))
        axis_width = 32
        in_fm = Axis(intbv()[axis_width:])
        out_fm = Axis(intbv()[axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)
        hdl_model = MyHdlModel(model, seq_len=self.seq_len, fxp=self.fxp)
        uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Converting...')
        uut.convert(hdl='VHDL', path=self.vhdl_output_path, name='passengers_model_net')
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')
