import torch
import pandas as pd
from torch.utils.data import Dataset, DataLoader
from sklearn.preprocessing import MinMaxScaler
import numpy as np


class PassengersData(Dataset):

    def create_dataset(self, lookback=5):
        dataX = []
        for i in range(len(self.data) - lookback):
            dataX.append(self.data[i:i+lookback])
        return np.array(dataX)

    def __init__(self, dataset=None, data_path=None, lookback=5, out_len=None):
        if data_path is not None:
            df = pd.read_csv(data_path, usecols=[1])
            df = df.dropna()
            self.data = df.values
            self.data = self.data.astype("float32")
            scaler = MinMaxScaler(feature_range=(0, 1))
            self.data = scaler.fit_transform(self.data)
        else:
            self.data = dataset
        dataX = self.create_dataset(lookback)
        self.labels = self.data[lookback:].flatten()
        self.data = torch.from_numpy(dataX).float()
        if out_len is not None:
            self.labels = self.labels[:out_len]
            self.data = self.data[:out_len]
        pass

    def __getitem__(self, item):
        return self.data[item], self.labels[item]

    def __len__(self):
        return self.data.shape[0]

    @property
    def data_np(self):
        return self.data.numpy()


if __name__ == '__main__':
    test_data_path = "/home/michal/datasets/kaggle_airline_passengers/international-airline-passengers.csv"
    test_set = PassengersData(data_path=test_data_path, lookback=5)
    test_data_loader = DataLoader(test_set, batch_size=1)
    test_set_np_data = test_set.data_np
    with torch.no_grad():
        for data, target in test_data_loader:
            print(data)
