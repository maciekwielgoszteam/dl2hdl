import os
import time

from unittest import skip
import numpy as np
import torch
import torch.nn as nn
from myhdl import intbv, Signal, ResetSignal

from dl2hdl.axi.axis import Axis
from dl2hdl.basic.cpp_if import torch_data_set_to_cpp_string, numpy_ndaray_to_cpp_str
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.layers.tests.torch_dataset import RandomBin
from dl2hdl.model import MyHdlModel
from dl2hdl.tests.hdl_test import HdlTest


class TestSingleLayerNet(HdlTest):
    class _TestModel(nn.Module):

        def __init__(self, in_features, sizes):
            super().__init__()
            self.classifier = nn.Sequential()
            self.classifier.add_module('linear0', nn.Linear(in_features=in_features, out_features=sizes[0]))
            for i in range(1, len(sizes)):
                self.classifier.add_module(f'linear{i}', nn.Linear(in_features=sizes[i - 1], out_features=sizes[i]))

        def forward(self, x):
            x = self.classifier(x)
            return x

    def setUp(self):
        super().setUp()
        self.raw_name = 'linear_layer_perf_v0'
        self.fxp = FixedDef(2, 4)
        self.seq_len = 4
        self.in_f = 2
        self.out_f = 2
        self.pruning = 0.0
        full_name = f'in{self.in_f}_out{self.out_f}_fxp{self.fxp.m}_{self.fxp.f}_prun{int(10000 * self.pruning):04d}'
        self.net_name = os.path.join(self.raw_name, full_name)
        self.in_f_range = [4]  # , 16, 64, 128]
        self.out_f_range = [2]  # , 4, 8]
        self.fxp_range = [FixedDef(2, 4), FixedDef(2, 6), FixedDef(2, 8)]
        self.prun_range = [0., 0.25, 0.5, 0.75]

    def prun(self, model, prun=None):
        # TODO: fix bias pruning
        prun = prun if prun is not None else self.pruning
        self.prun_weight(model.classifier.linear0.weight, prun)
        # self.prun_weight(model.linear.bias)
        return model

    @staticmethod
    def gen_single_one_by_one(model, fxp, out_path, name):
        from dl2hdl.layers.linear import linear, LinearCoeffs
        from dl2hdl.basic.interface_map import receiver, transmitter

        os.makedirs(out_path, exist_ok=True)

        axis_width = 32
        in_fm = Axis(intbv()[axis_width:])
        out_fm = Axis(intbv()[axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)

        linear_0_coeff = LinearCoeffs(from_torch=list(model.modules())[2], fxp=fxp)

        # RECEIVER
        connection_0 = Axis(intbv()[fxp.width * linear_0_coeff.layer.in_features:])
        rcv = receiver(clk, reset,
                       in_data=in_fm,
                       out_data=connection_0,
                       fxp=fxp)
        rcv.convert(hdl='VHDL', path=out_path, name=name + '_split_receiver')

        # LINEAR
        connection_1 = Axis(intbv()[fxp.width * linear_0_coeff.layer.out_features:])
        lin_0 = linear(clk, reset,
                       in_fm=connection_0,
                       out_fm=connection_1,
                       coeffs=linear_0_coeff,
                       fxp=fxp)
        lin_0.convert(hdl='VHDL', path=out_path, name=name + '_split_linear_0')

        # TRANSMITTER
        txd = transmitter(clk, reset,
                          in_data=connection_1,
                          out_data=out_fm,
                          fxp=fxp)
        txd.convert(hdl='VHDL', path=out_path, name=name + '_split_transmitter')

    @skip('Not running test')
    def test_01_net_sim(self):
        new = True
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: testing {self.seq_len}-{self.in_f}-{self.out_f}')
        test_set = RandomBin(shape=(self.in_f,), num_of_samples=self.seq_len)
        model = self._TestModel(self.in_f, (self.out_f,))
        model_path = os.path.join(self.model_path, self.net_name + '.pt')
        if new:
            model = self.prun(model)
            torch.save(model.state_dict(), model_path)
        else:
            model.load_state_dict(torch.load(model_path))

        out_true = self.infer_net(model, test_set)
        out_true = [each.view(each.shape[1:]).tolist() for each in out_true]
        hdl_model = MyHdlModel(model, fxp=self.fxp)
        out_data = self._single_run(test_set.data, hdl_model, fxp=self.fxp)
        out_data = [[self.fxp.to_float(y) for y in x] for x in out_data]

        out_true_np = np.asarray(out_true)
        out_data_np = np.asarray(out_data)
        path = os.path.join(self.data_path, self.net_name)
        file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=self.fxp)
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_data_np), 'outputs')
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_true_np), 'outputs_true')
        os.makedirs(path, exist_ok=True)
        f = open(os.path.join(path, 'test_data.h'), 'w')
        print(file_str, file=f)
        f.close()

        numpy_array_to_txt_vec(self.fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(self.fxp.to_fixed(out_data_np), path, 'test_output')

        max_diff_allowed = 1. / 2 ** 3
        print(f'out_data:\n{out_data}')
        print(f'out_true:\n{out_true}')
        out_diff = np.abs(out_data_np - out_true_np)
        max_diff = np.amax(out_diff)
        mean_diff = np.mean(out_diff)
        print(f'max_diff_allowed = {max_diff_allowed}')
        print(f'max_diff = {max_diff}')
        print(f'mean_diff = {mean_diff}')
        self.assertTrue(max_diff < max_diff_allowed)

    def test_02_net_gen(self):
        import subprocess
        for in_f in self.in_f_range:
            for out_f in self.out_f_range:
                for fxp in self.fxp_range:
                    for prun in self.prun_range:
                        with self.subTest(in_f=in_f, out_f=out_f, fxp=fxp, prun=prun):
                            curr_time = time.strftime("%Y-%m-%d %H:%M:%S")
                            print(f'{curr_time}: {in_f}-{out_f}-{fxp}-{prun}')
                            # model = self._TestModel(in_f, (out_f,))
                            # model = self.prun(model, prun=prun)
                            full_name = f'in{in_f}_out{out_f}_fxp{fxp.m}_{fxp.f}_prun{int(10000 * prun):04d}'
                            net_path = os.path.join(self.vhdl_output_path, self.raw_name, full_name)
                            cmd = ['python', 'generate_model.py']
                            cmd += ['--in_f', str(in_f)]
                            cmd += ['--out_f', str(out_f)]
                            cmd += ['--fxp_i', str(fxp.m)]
                            cmd += ['--fxp_f', str(fxp.f)]
                            cmd += ['--prun', str(prun)]
                            cmd += ['--path', net_path]
                            cmd += ['--name', self.raw_name]
                            subprocess.call(cmd)
                            # subprocess.call(['touch', os.path.join(net_path, self.raw_name + '.txt')])
                            # self.gen_single_one_by_one(model, fxp, net_path, self.raw_name)

    @skip('Using one by one approach instead')
    def test_03_conv_all(self):
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Loading model...')
        model = self._TestModel(self.in_f, (self.out_f,))
        model_path = os.path.join(self.model_path, self.net_name + '.pt')
        model.load_state_dict(torch.load(model_path))
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Creating HDL model...')
        axis_width = 32
        in_fm = Axis(intbv()[axis_width:])
        out_fm = Axis(intbv()[axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)
        hdl_model = MyHdlModel(model, fxp=self.fxp)
        uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Converting...')
        uut.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name)
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')

    @skip('Not used')
    def test_04_conv_one_by_one(self):
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Loading model...')
        model = self._TestModel(self.in_f, (self.out_f,))
        model_path = os.path.join(self.model_path, self.net_name + '.pt')
        model.load_state_dict(torch.load(model_path))
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Creating HDL model blocs and converting them...')

        from dl2hdl.layers.linear import linear, LinearCoeffs
        from dl2hdl.basic.interface_map import receiver, transmitter

        axis_width = 32
        in_fm = Axis(intbv()[axis_width:])
        out_fm = Axis(intbv()[axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)

        linear_0_coeff = LinearCoeffs(from_torch=list(model.modules())[2], fxp=self.fxp)

        # RECEIVER
        connection_0 = Axis(intbv()[self.fxp.width * linear_0_coeff.layer.in_features:])
        rcv = receiver(clk, reset,
                       in_data=in_fm,
                       out_data=connection_0,
                       fxp=self.fxp)
        rcv.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_receiver')

        # LINEAR
        connection_1 = Axis(intbv()[self.fxp.width * linear_0_coeff.layer.out_features:])
        lin_0 = linear(clk, reset,
                       in_fm=connection_0,
                       out_fm=connection_1,
                       coeffs=linear_0_coeff,
                       fxp=self.fxp)
        lin_0.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_linear_0')

        # TRANSMITTER
        txd = transmitter(clk, reset,
                          in_data=connection_1,
                          out_data=out_fm,
                          fxp=self.fxp)
        txd.convert(hdl='VHDL', path=self.vhdl_output_path, name=self.net_name + '_split_transmitter')

        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')
