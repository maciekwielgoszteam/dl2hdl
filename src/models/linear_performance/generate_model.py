import os
import argparse
import time

import numpy as np
import torch.nn as nn
from myhdl import intbv, Signal, ResetSignal

from dl2hdl.axi.axis import Axis
from dl2hdl.basic.fixed_def import FixedDef


class TestModel(nn.Module):

    def __init__(self, in_features, sizes):
        super().__init__()
        self.classifier = nn.Sequential()
        self.classifier.add_module('linear0', nn.Linear(in_features=in_features, out_features=sizes[0]))
        for i in range(1, len(sizes)):
            self.classifier.add_module(f'linear{i}', nn.Linear(in_features=sizes[i - 1], out_features=sizes[i]))

    def forward(self, x):
        x = self.classifier(x)
        return x


def gen_single_one_by_one(model, fxp, out_path, name):
    from dl2hdl.layers.linear import linear, LinearCoeffs
    from dl2hdl.basic.interface_map import receiver, transmitter

    os.makedirs(out_path, exist_ok=True)

    axis_width = 32
    in_fm = Axis(intbv()[axis_width:])
    out_fm = Axis(intbv()[axis_width:])
    clk = Signal(bool(0))
    reset = ResetSignal(0, active=0, isasync=False)

    linear_0_coeff = LinearCoeffs(from_torch=list(model.modules())[2], fxp=fxp)

    # RECEIVER
    connection_0 = Axis(intbv()[fxp.width * linear_0_coeff.layer.in_features:])
    rcv = receiver(clk, reset,
                   in_data=in_fm,
                   out_data=connection_0,
                   fxp=fxp)
    rcv.convert(hdl='VHDL', path=out_path, name=name + '_split_receiver')

    # LINEAR
    connection_1 = Axis(intbv()[fxp.width * linear_0_coeff.layer.out_features:])
    lin_0 = linear(clk, reset,
                   in_fm=connection_0,
                   out_fm=connection_1,
                   coeffs=linear_0_coeff,
                   fxp=fxp)
    lin_0.convert(hdl='VHDL', path=out_path, name=name + '_split_linear_0')

    # TRANSMITTER
    txd = transmitter(clk, reset,
                      in_data=connection_1,
                      out_data=out_fm,
                      fxp=fxp)
    txd.convert(hdl='VHDL', path=out_path, name=name + '_split_transmitter')


def generate_generics_package(path, input_width, output_width):
    pck_name = 'pck_generic_values'
    pck_string = '-- This is automatically generated package file\n'
    pck_string += f'-- Date: {time.strftime("%Y-%m-%d %H:%M:%S")}\n'
    pck_string += f'package {pck_name} is\n'
    pck_string += f'\tconstant axi_width : integer := 32;\n'
    pck_string += f'\tconstant input_width : integer := {input_width};\n'
    pck_string += f'\tconstant output_width : integer := {output_width};\n'
    pck_string += f'end {pck_name};'
    with open(os.path.join(path, pck_name + '.vhd'), 'w') as pck_file:
        pck_file.write(pck_string)


def prun_weight(weight, prun):
    weight_flat = weight.flatten()
    indices = np.random.choice(np.arange(weight_flat.shape[0]), replace=False, size=int(weight_flat.shape[0] * prun))
    weight_flat[indices] = 0.
    weight.data = weight_flat.reshape(weight.shape).data


def prun(model, prun):
    # TODO: fix bias pruning
    prun_weight(model.classifier.linear0.weight, prun)
    # self.prun_weight(model.linear.bias)
    return model


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate neural network containing only single linear layer')
    parser.add_argument('--in_f', type=int, help='number of input features')
    parser.add_argument('--out_f', type=int, help='number of output features')
    parser.add_argument('--fxp_i', type=int, help='integer width for fixed point definition')
    parser.add_argument('--fxp_f', type=int, help='fraction width for fixed point definition')
    parser.add_argument('--prun', type=float, help='fraction of weights to prune')
    parser.add_argument('--path', type=str, help='Number of output features')
    parser.add_argument('--name', type=str, help='Number of output features')

    args = parser.parse_args()
    fxp = FixedDef(args.fxp_i, args.fxp_f)
    model = TestModel(args.in_f, (args.out_f,))
    model = prun(model, prun=args.prun)

    # full_name = f'in{args.in_f}_out{args.out_f}_fxp{fxp.m}_{fxp.f}_prun{int(10000 * args.prun):04d}'
    # net_path = os.path.join(args.path, args.name, full_name)
    gen_single_one_by_one(model, fxp, args.path, args.name)
    input_width = args.in_f * (args.fxp_i + args.fxp_f)
    output_width = args.out_f * (args.fxp_i + args.fxp_f)
    generate_generics_package(args.path, input_width, output_width)
