import os
import time

import numpy as np
import torch
import torch.nn as nn

from dl2hdl.basic.cpp_if import torch_data_set_to_cpp_string, numpy_ndaray_to_cpp_str
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.layers.tests.torch_dataset import RandomBin
from dl2hdl.model import MyHdlModel
from dl2hdl.tests.hdl_test import HdlTest


class TestSingleLayerNet(HdlTest):
    class _TestModel(nn.Module):

        def __init__(self, in_features, sizes):
            super().__init__()
            self.classifier = nn.Sequential()
            self.classifier.add_module('linear0', nn.Linear(in_features=in_features, out_features=sizes[0]))
            self.classifier.add_module(f'relu0', nn.ReLU())
            for i in range(1, len(sizes) - 1):
                self.classifier.add_module(f'linear{i}', nn.Linear(in_features=sizes[i - 1], out_features=sizes[i]))
                self.classifier.add_module(f'relu{i}', nn.ReLU())
            self.classifier.add_module(f'linear{len(sizes) - 1}',
                                       nn.Linear(in_features=sizes[len(sizes) - 2], out_features=sizes[len(sizes) - 1]))
            self.classifier.add_module('softmax', nn.Softmax(dim=-1))

        def forward(self, x):
            x = self.classifier(x)
            return x

    def setUp(self):
        super().setUp()
        self.raw_name = 'hls4ml_comparison_v0'
        self.fxp = FixedDef(6, 10)
        self.seq_len = 4
        self.in_f = 16
        self.hid_f = (64, 32, 32)
        self.out_f = 5
        self.pruning = 0.7
        self.net_name = self.raw_name

    def prun(self, model, prun=None):
        # TODO: fix bias pruning
        prun = prun if prun is not None else self.pruning
        self.prun_weight(model.classifier.linear0.weight, prun)
        self.prun_weight(model.classifier.linear1.weight, prun)
        self.prun_weight(model.classifier.linear2.weight, prun)
        self.prun_weight(model.classifier.linear3.weight, prun)
        # self.prun_weight(model.linear.bias)
        return model

    def test_01_net_sim(self):
        new = True
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: testing {self.seq_len}-{self.in_f}-{self.out_f}')
        test_set = RandomBin(shape=(self.in_f,), num_of_samples=self.seq_len)
        model = self._TestModel(self.in_f, (*self.hid_f, self.out_f,))
        os.makedirs(self.model_path, exist_ok=True)
        model_path = os.path.join(self.model_path, self.net_name + '.pt')
        if new:
            model = self.prun(model)
            torch.save(model.state_dict(), model_path)
        else:
            model.load_state_dict(torch.load(model_path))

        out_true = self.infer_net(model, test_set)
        out_true = [each.view(each.shape[1:]).tolist() for each in out_true]
        hdl_model = MyHdlModel(model, fxp=self.fxp)
        out_data = self._single_run(test_set.data, hdl_model, fxp=self.fxp)
        out_data = [[self.fxp.to_float(y) for y in x] for x in out_data]

        out_true_np = np.asarray(out_true)
        out_data_np = np.asarray(out_data)
        path = os.path.join(self.data_path, self.net_name)
        file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=self.fxp)
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_data_np), 'outputs')
        file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_true_np), 'outputs_true')
        os.makedirs(path, exist_ok=True)
        f = open(os.path.join(path, 'test_data.h'), 'w')
        print(file_str, file=f)
        f.close()

        numpy_array_to_txt_vec(self.fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(self.fxp.to_fixed(out_data_np), path, 'test_output')

        max_diff_allowed = 1. / 2 ** 3
        print(f'out_data:\n{out_data}')
        print(f'out_true:\n{out_true}')
        out_diff = np.abs(out_data_np - out_true_np)
        max_diff = np.amax(out_diff)
        mean_diff = np.mean(out_diff)
        print(f'max_diff_allowed = {max_diff_allowed}')
        print(f'max_diff = {max_diff}')
        print(f'mean_diff = {mean_diff}')
        self.assertTrue(max_diff < max_diff_allowed)

    def test_latency_02_conv(self):
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Creating model...')
        model = self._TestModel(self.in_f, (*self.hid_f, self.out_f,))
        model_path = os.path.join(self.model_path, self.net_name + '.pt')
        model.load_state_dict(torch.load(model_path))
        hdl_model = MyHdlModel(model, seq_len=self.seq_len, fxp=self.fxp)
        hdl_model.convert(path=self.vhdl_output_path, name=self.net_name)
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')
