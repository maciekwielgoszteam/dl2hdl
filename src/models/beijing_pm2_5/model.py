import torch
import torch.nn as nn
from ml.layers.lstm_hard import LSTMHardSigmoid


class BeijingModel(nn.Module):

    def __init__(self, in_dim, hidden_dim, out_dim):
        super().__init__()
        self.hidden_dim = hidden_dim
        self.lstm = LSTMHardSigmoid(input_size=in_dim, hidden_size=hidden_dim, batch_first=True)
        self.linear = nn.Linear(in_features=hidden_dim, out_features=out_dim)

    def _init_hidden(self, device, batch_size=1):
        return torch.zeros(1, batch_size, self.hidden_dim).to(device), torch.zeros(1, batch_size, self.hidden_dim).to(device)

    def forward(self, input_seq):
        hidden_init = self._init_hidden(input_seq.device, input_seq.shape[0])
        self.lstm.flatten_parameters()
        outputs, hidden = self.lstm(input_seq, hidden_init)
        outputs = self.linear(outputs[:, -1, :])
        return outputs
