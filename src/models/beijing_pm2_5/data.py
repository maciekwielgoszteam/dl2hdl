import torch
import csv
import datetime
from torch.utils.data import Dataset, DataLoader
import numpy as np


class BeijingData(Dataset):

    def create_dataset(self, lookback=72):
        dataX = []
        for i in range(len(self.data) - lookback):
            dataX.append(self.data[i:i + lookback])
        return np.array(dataX)

    @staticmethod
    def to_float(val):
        try:
            return float(val)
        except ValueError:
            return float("NaN")

    def normalize(self):
        mins = np.nanmin(self.data, axis=0)
        maxs = np.nanmax(self.data, axis=0)
        ranges = maxs - mins
        for i in range(self.data.shape[1]):
            try:
                self.data[:, i] = (self.data[:, i] - mins[i]) / (ranges[i] / 2.) - 1.
            except ZeroDivisionError:
                self.data[:, i] = 0.

    def __init__(self, dataset=None, data_path=None, lookback=72, out_len=None, start_from=0, normalize=False):
        if data_path is not None:
            with open(data_path) as csvfile:
                reader = csv.DictReader(csvfile)

                converted = []

                for row in reader:
                    row['cbwd'] = row['cbwd'].upper()
                    NW = (row['cbwd'] == 'NW')
                    NE = (row['cbwd'] == 'NE')
                    SE = (row['cbwd'] == 'SE')
                    SW = (row['cbwd'] == 'SW')
                    CV = (row['cbwd'] == 'CV')

                    converted.append([
                        self.to_float(row['pm2.5']),
                        float(row['DEWP']),
                        float(row['TEMP']),
                        float(row['PRES']),
                        NW,
                        NE,
                        SE,
                        SW,
                        CV,
                        float(row['Iws']),
                        float(row['Is']),
                        float(row['Ir'])
                    ])

            self.data = np.array(converted)
        else:
            self.data = dataset

        if normalize:
            self.normalize()

        dataX = self.create_dataset(lookback)
        self.labels = self.data[lookback:, 0]
        self.data = torch.from_numpy(dataX).float()
        if out_len is not None:
            self.labels = self.labels[start_from:start_from + out_len]
            self.data = self.data[start_from:start_from + out_len]
        pass

    def __getitem__(self, item):
        return self.data[item], self.labels[item]

    def __len__(self):
        return self.data.shape[0]

    @property
    def data_np(self):
        return self.data.numpy()


if __name__ == '__main__':
    test_data_path = "/home/michal/datasets/Beijing_PM2.5/PRSA_data_2010.1.1-2014.12.31.csv"
    test_set = BeijingData(data_path=test_data_path, lookback=72)
    test_data_loader = DataLoader(test_set, batch_size=1)
    test_set_np_data = test_set.data_np
    print(test_set_np_data.shape)
