import os
import argparse
import time
import torch

import numpy as np
import torch.nn as nn
from myhdl import intbv, Signal, ResetSignal
from models.beijing_pm2_5.model import BeijingModel
from dl2hdl.axi.axis import Axis
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.model import MyHdlModel
from config.config import config


def gen_single(model, fxp, seq_len, out_path, name):
    os.makedirs(out_path, exist_ok=True)
    axis_width = 32
    in_fm = Axis(intbv()[axis_width:])
    out_fm = Axis(intbv()[axis_width:])
    clk = Signal(bool(0))
    reset = ResetSignal(0, active=0, isasync=False)
    hdl_model = MyHdlModel(model, seq_len=seq_len, fxp=fxp)
    uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)
    # print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Converting...')
    uut.convert(hdl='VHDL', path=out_path, name=name)
    # print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')


def prun_weight(weight, prun):
    weight_flat = weight.flatten()
    indices = np.random.choice(np.arange(weight_flat.shape[0]), replace=False,
                               size=int(weight_flat.shape[0] * prun))
    weight_flat[indices] = 0.
    weight.data = weight_flat.reshape(weight.shape).data


def prun(model, prun=None):
    # TODO: fix bias pruning
    prun_weight(model.lstm.weight_ih_l0, prun)
    prun_weight(model.lstm.weight_hh_l0, prun)
    # self.prun_weight(model.lstm_0.bias_ih_l0, prun)
    # self.prun_weight(model.lstm_0.bias_ih_l0, prun)
    prun_weight(model.linear.weight, prun)
    # self.prun_weight(model.linear.bias, prun)
    return model


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate neural network containing only single linear layer')
    parser.add_argument('--fxp_i', type=int, help='integer width for fixed point definition')
    parser.add_argument('--fxp_f', type=int, help='fraction width for fixed point definition')
    parser.add_argument('--prun', type=float, help='fraction of weights to prune')
    parser.add_argument('--seq', type=int, help='sequence length')
    parser.add_argument('--path', type=str, help='Output path')
    parser.add_argument('--name', type=str, help='Number of output features')

    args = parser.parse_args()
    fxp = FixedDef(args.fxp_i, args.fxp_f)
    model = BeijingModel(12, 16, 1)
    model_path = os.path.join(config.get_path_out_model(), 'beijing_model_net.pt')
    model.load_state_dict(torch.load(model_path))
    model = prun(model, prun=args.prun)

    gen_single(model, fxp, args.seq, args.path, args.name)

