from dl2hdl.tests.hdl_test import HdlTest
from unittest import skip
import warnings
import torch
import numpy as np
import os
import time
from dl2hdl.basic.fixed_def import FixedDef
from dl2hdl.axi.axis import Axis
from myhdl import intbv, Signal, ResetSignal
from dl2hdl.basic.testbench_if import numpy_array_to_txt_vec
from dl2hdl.model import MyHdlModel
from models.beijing_pm2_5.model import BeijingModel
from models.beijing_pm2_5.data import BeijingData
from dl2hdl.layers.tests.torch_dataset import RandomSeries


class TestBeijingModelNet(HdlTest):

    def prun(self, model, prun=None):
        # TODO: fix bias pruning
        prun = prun if prun is not None else self.pruning
        self.prun_weight(model.lstm.weight_ih_l0, prun)
        self.prun_weight(model.lstm.weight_hh_l0, prun)
        # self.prun_weight(model.lstm_0.bias_ih_l0, prun)
        # self.prun_weight(model.lstm_0.bias_ih_l0, prun)
        self.prun_weight(model.linear.weight, prun)
        # self.prun_weight(model.linear.bias, prun)
        return model

    def setUp(self):
        super().setUp()
        self.fxp = FixedDef(4, 12)
        self.test_sequences = 1
        self.seq_len = 4
        self.in_f = 12
        self.hid_f = 16
        self.out_dim = 1
        self.pruning = 0.75

        self.settings = (
            ((4, 1), 0.1970),
            ((4, 2), 0.1003),
            ((4, 3), 0.2493),
            ((4, 4), 0.2265),
            ((4, 5), 0.2265),
            ((4, 6), 0.2091),
            ((4, 7), 0.1922),
            ((4, 9), 0.2513),
            ((4, 10), 0.2291),
            ((4, 11), 0.1346),
            ((4, 12), 0.2640),
            ((4, 13), 0.2133),
            ((4, 14), 0.2850),
            ((4, 15), 0.2630),
            ((4, 17), 0.1495),
            ((4, 27), 0.2073),
        )

    @skip
    def test_01_net_sim(self):
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: testing {self.seq_len}-{self.in_f}-{self.hid_f}-{self.out_dim}')
        test_data_path = "/home/michal/datasets/Beijing_PM2.5/PRSA_data_2010.1.1-2014.12.31.csv"
        # test_set = BeijingData(data_path=test_data_path, lookback=self.seq_len, out_len=30, start_from=50, normalize=True)
        test_set = RandomSeries(shape=(self.seq_len, self.in_f), num_of_samples=self.test_sequences)
        trained_model = BeijingModel(self.in_f, self.hid_f, self.out_dim)
        trained_model_path = os.path.join(self.model_path, 'beijing_model_net.pt')
        trained_model.load_state_dict(torch.load(trained_model_path))
        model = self.prun(trained_model)
        model_path = os.path.join(self.model_path, 'beijing_model_net_v0.pt')
        torch.save(model.state_dict(), model_path)
        hdl_model = MyHdlModel(model, seq_len=self.seq_len, fxp=self.fxp)
        out_true = self.infer_net(model, test_set)
        out_true = [each[0].tolist() for each in out_true]
        out_data = self._single_run(test_set.data, hdl_model, fxp=self.fxp)
        out_data = [[self.fxp.to_float(y) for y in x] for x in out_data]

        out_true_np = np.asarray(out_true)
        out_data_np = np.asarray(out_data)
        path = os.path.join(self.data_path, 'beijing_model_net_data')
        os.makedirs(path, exist_ok=True)
        # file_str = f'// test data set\n// Created: {time.strftime("%Y-%m-%d %H:%M")}\n'
        # file_str += f'unsigned seq_len = {test_set.data.shape[0]};\n\r'
        # file_str += f'unsigned input_len = {test_set.data.shape[1]};\n\r'
        # file_str += f'unsigned output_len = {out_data_np.shape[1]};\n\r'
        # file_str += torch_data_set_to_cpp_string(test_set, 'test_set', fxp=self.fxp)
        # file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_data_np), 'outputs')
        # file_str += numpy_ndaray_to_cpp_str(self.fxp.to_fixed(out_true_np), 'outputs_true')
        # f = open(os.path.join(path, 'test_data.h'), 'w')
        # print(file_str, file=f)
        # f.close()

        numpy_array_to_txt_vec(self.fxp.to_fixed(test_set.data.numpy()), path, 'test_data')
        numpy_array_to_txt_vec(self.fxp.to_fixed(out_data_np), path, 'test_output')

        decimal = 5
        warnings.warn(f'Precision for equality test is set to decimal={decimal}, that is not much... :( ')
        print(f'out_data:\n{out_data}')
        print(f'out_true:\n{out_true}')
        np.testing.assert_array_almost_equal(out_data, out_true, decimal=decimal)

    @skip
    def test_02_net_conv(self):
        model = BeijingModel(self.in_f, self.hid_f, self.out_dim)
        model_path = os.path.join(self.model_path, 'beijing_model_net_v0.pt')
        model.load_state_dict(torch.load(model_path))
        axis_width = 32
        in_fm = Axis(intbv()[axis_width:])
        out_fm = Axis(intbv()[axis_width:])
        clk = Signal(bool(0))
        reset = ResetSignal(0, active=0, isasync=False)
        hdl_model = MyHdlModel(model, seq_len=self.seq_len, fxp=self.fxp)
        uut = hdl_model.get_hdl_block(clk, reset, in_fm, out_fm)
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Converting...')
        uut.convert(hdl='VHDL', path=self.vhdl_output_path, name='beijing_model_net')
        print(f'{time.strftime("%Y-%m-%d %H:%M:%S")}: Conversion completed')

    def test_03_net_gen(self):
        import subprocess
        for setting in self.settings:
            curr_time = time.strftime("%Y-%m-%d %H:%M:%S")
            print(f'{curr_time}: {setting[0]}-{setting[1]}')
            full_name = f'fxp{setting[0][0]}_{setting[0][1]}_prun{int(10000 * setting[1]):04d}'
            net_path = os.path.join(self.vhdl_output_path, 'beijing_model_net', full_name)
            cmd = ['python', 'generate_model.py']
            cmd += ['--fxp_i', str(setting[0][0])]
            cmd += ['--fxp_f', str(setting[0][1])]
            cmd += ['--prun', str(setting[1])]
            cmd += ['--path', net_path]
            cmd += ['--seq', str(self.seq_len)]
            cmd += ['--name', 'beijing_model_net']
            subprocess.call(cmd)