import math
import os

import numpy as np
import torch
from keras.models import load_model
from sklearn.metrics import mean_squared_error
from torch.utils.data import DataLoader

from config.config import config

from models.beijing_pm2_5.model import BeijingModel
from models.beijing_pm2_5.data import BeijingData


def transfer_keras_to_pytorch():
    model_path = config.get_path_out_model()
    keras_model_path = os.path.join(model_path, 'model-4a1b4306-7ef6-11e9-9f4e-72000690be10.h5')
    test_data_path = "/home/michal/datasets/Beijing_PM2.5/PRSA_data_2010.1.1-2014.12.31.csv"
    keras_model = load_model(keras_model_path)
    keras_model.summary()
    keras_weights = keras_model.get_weights()
    torch_model = BeijingModel(12, 16, 1)
    torch_model.lstm.weight_ih_l0.data = torch.from_numpy(np.transpose(keras_weights[0]).astype(np.float32))
    torch_model.lstm.weight_hh_l0.data = torch.from_numpy(np.transpose(keras_weights[1]).astype(np.float32))
    torch_model.lstm.bias_ih_l0.data = torch.from_numpy(np.transpose(keras_weights[2]).astype(np.float32))
    torch_model.lstm.bias_hh_l0.data = torch.from_numpy(
        np.transpose(np.zeros(keras_weights[2].shape)).astype(np.float32))
    torch_model.linear.weight.data = torch.from_numpy(np.transpose(keras_weights[3]).astype(np.float32))
    torch_model.linear.bias.data = torch.from_numpy(np.transpose(keras_weights[4]).astype(np.float32))

    test_data = BeijingData(data_path=test_data_path, lookback=72, out_len=100)
    test_data_x_np = test_data.data_np

    keras_predicted_y = keras_model.predict(test_data_x_np, batch_size=1, verbose=1).flatten()
    print('Keras output:')
    print(keras_predicted_y)

    # keras_score = math.sqrt(mean_squared_error(test_data.labels, keras_predicted_y))
    # print(f'Train Score: {keras_score} RMSE')

    torch_test_data_loader = DataLoader(test_data, batch_size=1)
    torch_model.eval()
    torch_predicted_y = []
    with torch.no_grad():
        for data, target in torch_test_data_loader:
            output = torch_model(data)
            torch_predicted_y.append(output.numpy())

    torch_predicted_y = np.stack(torch_predicted_y, axis=0).flatten()
    print('Torch output:')
    print(torch_predicted_y)

    # torch_score = math.sqrt(mean_squared_error(test_data.labels, torch_predicted_y))
    # print(f'Train Score: {torch_score} RMSE')

    diff = np.abs(np.nan_to_num(keras_predicted_y) - np.nan_to_num(torch_predicted_y))
    print('Diff array:')
    print(diff)
    print(f'Average diff = {np.mean(diff)}')

    torch_model_path = os.path.join(model_path, 'beijing_model_net.pt')
    torch.save(torch_model.state_dict(), torch_model_path)


if __name__ == '__main__':
    transfer_keras_to_pytorch()
