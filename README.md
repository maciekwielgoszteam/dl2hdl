# DL2HDL v0.1
## How to use
### Basic network conversion
Create network model using standard PyTorch API. You can train you model using standard PyTorch approach.

To generate VHDL create MyHdlModel, it requires fixed point definition:

```python
import torch.nn as nn
from dl2hdl.model import MyHdlModel
from dl2hdl.basic.fixed_def import FixedDef


class MyNet(nn.Module):

    def __init__(self):
        super().__init__()
        self.linear_0 = nn.Linear(in_features=8, out_features=4)
        self.relu_1 = nn.ReLU()
        self.linear_2 = nn.Linear(in_features=4, out_features=2)
        self.softmax_3 = nn.Softmax(dim=1)

    def forward(self, input):
        outputs = self.linear_0(input)
        outputs = self.relu_1(outputs)
        outputs = self.linear_2(outputs)
        outputs = self.softmax_3(outputs)
        return outputs


hdl_model = MyHdlModel(torch_model=MyNet(), fxp=FixedDef(4, 4))
hdl_model.convert(path='', name='my_net')
```
Convert function will output ready VHDL file to provided path. It will generate standard AXI4Stream interface to your network.
#### Note: only a subset of PyTorch functionality is supported! 
### Examples
More advanced example, including MyHDL simulation and generation of sources for VHDL simulation, is provided in src/examples.
## Requirements
### MyHDL version:
http://www.myhdl.org/

This software uses MyHDL library in version 0.11

However, it required some changes and modified library is attached along with the software.
### Python version:
Tested on 3.6, but MyHDL updates since version 0.11 should allow it to work on 3.7
### VHDL version:
VHDL2008 - as required by MyHDL

## License:
License for modification of MyHDL library: GNU LESSER GENERAL PUBLIC LICENSE Version 2.1

The rest of the software is under MIT license

## Citation
If you find this software useful please cite:

Wielgosz M, Karwatowski M. Mapping Neural Networks to FPGA-Based IoT Devices for Ultra-Low Latency Processing. Sensors. 2019; 19(13):2981.
https://doi.org/10.3390/s19132981
