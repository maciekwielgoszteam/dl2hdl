import subprocess
from shutil import copyfile
import time
import os

date = f'{time.strftime("%Y-%m-%d_%H-%M-%S")}'
print(date)

net_src_path = '/home/michal/agh/workspace/tester_usp'
my_path = subprocess.check_output(['pwd'])
my_path = str(my_path)[2:-3]

available_src_relative = 'net_sources/available'
current_src_relative = 'net_sources/current'
available_src_abs = os.path.join(net_src_path, available_src_relative)
current_src_abs = os.path.join(net_src_path, current_src_relative)
file_prefix = 'latency_net_'
file_suffix = '.vhd'

tcl_path = os.path.join(my_path, 'out_test.tcl')
vivado_path = '/opt/Xilinx/Vivado/2018.3/bin/vivado'
vivado_args = ['-mode', 'tcl']
vivado_args.extend(['-source', tcl_path])

params = {'size_4-16-8-2_fxp_4-4': ['100', '120', '140', '160', '180', '200'],
          'size_4-16-8-2_fxp_4-6': ['110', '130', '150', '170', '190']}

for param, freqs in params.items():
    failed_once = False
    src_file = os.path.join(available_src_abs, file_prefix + param + file_suffix)
    dst_file = os.path.join(current_src_abs, file_prefix + 'v1' + file_suffix)
    copyfile(src_file, dst_file)
    for freq in freqs:
        vivado_args_tcl = vivado_args + ['-tclargs'] + [param] + [freq] + [date]
        vivado_cmd = [vivado_path] + vivado_args_tcl
        print(' '.join(vivado_cmd))
        try:
            ret_val = subprocess.run(vivado_cmd, check=True, stdout=subprocess.PIPE, universal_newlines=True).stdout
            print(ret_val)
        except subprocess.CalledProcessError as error:
            print('Timings failed')
            if not failed_once:
                failed_once = True
            else:
                break
