import subprocess
from shutil import copyfile
import time
import os

date = f'{time.strftime("%Y-%m-%d_%H-%M-%S")}'
print(date)

my_path = subprocess.check_output(['pwd'])
my_path = str(my_path)[2:-3]

available_src_relative = 'net_sources/available'
current_src_relative = 'net_sources/current'
available_src_abs = os.path.join(my_path, available_src_relative)
current_src_abs = os.path.join(my_path, current_src_relative)
file_prefix = 'latency_net_'
file_suffix = '.vhd'

tcl_path = os.path.join(my_path, 'scripts/freq_test.tcl')
vivado_path = '/home/plgcorwin/programs/Xilinx/Vivado/2018.3/bin/vivado'
vivado_args = ['-mode', 'tcl']
vivado_args.extend(['-source', tcl_path])

params = {
	'3-1_3142': ['285', '290', '295'],
	#'4-1_0000': ['290', '295'],
	#'4-2_0213': ['235', '240'],
	#'4-2_3580': ['245', '250'],
	#'4-3_0901': ['175', '180'],
	#'4-3_1663': ['180', '185'],
	#'4-5_2130': ['165', '170'],
	#'4-6_1942': ['165', '170'],
	#'4-7_1396': ['130', '135', '140'],
	#'4-7_1725': ['140', '145'],
	#'4-7_2250': ['140', '145'],
}

for param, freqs in params.items():
    failed_once = False
    src_file = os.path.join(available_src_abs, file_prefix + param + file_suffix)
    dst_file = os.path.join(current_src_abs, file_prefix + 'v1' + file_suffix)
    copyfile(src_file, dst_file)
    for freq in freqs:
        vivado_args_tcl = vivado_args + ['-tclargs'] + [param] + [freq] + [date]
        vivado_cmd = [vivado_path] + vivado_args_tcl
        print(' '.join(vivado_cmd))
        try:
            ret_val = subprocess.run(vivado_cmd, check=True, stdout=subprocess.PIPE, universal_newlines=True).stdout
            print(ret_val)
        except subprocess.CalledProcessError as error:
            print('Timings failed')
            if not failed_once:
                failed_once = True
            else:
                break
