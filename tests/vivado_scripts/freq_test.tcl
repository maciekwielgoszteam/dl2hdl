set date [lindex $argv 1]
set logFileName "log_${date}.txt"
set projPath [pwd]
set projName "nn_tester"
set settings [lindex $argv 0]

open_project "${projPath}/${projName}.xpr"
update_compile_order -fileset sources_1

# this is the best I found
set freq_inc {81 27 9 3 1}
#set freq_inc {81 27 9}
set inc_val_iter 0
set go_up true
set freq 300
set min_freq 5
set max_freq 667
set inc_val_iter 0
set freq_found false
set tries 0

while {!$freq_found} {

	set clockConstraintsPath "${projPath}/net_sources/current/system.xdc"
	set clockConstraintsFile [open $clockConstraintsPath w]
	puts $clockConstraintsFile ""
	close $clockConstraintsFile

	synth_design -rtl -name rtl_1

	set period [expr {double(1000) / $freq}]
	set period_half [expr {$period / 2}]
	create_clock -period $period -name clk -waveform "0.000 $period_half" [get_ports clk]
	save_constraints
	close_design

	set synth_name "synth_${settings}_freq_${freq}"
	set impl_name "impl_${settings}_freq_${freq}"
	set runs_names [get_runs $synth_name]
	foreach name $runs_names {
		reset_run $name
		delete_runs $name
	}
	create_run $synth_name -flow {Vivado Synthesis 2018}
	create_run $impl_name -parent_run $synth_name -flow {Vivado Implementation 2018}
	reset_run $synth_name
	launch_runs $impl_name -jobs 16
	wait_on_run -quiet $impl_name
	open_run $impl_name
	set slack [get_property SLACK [get_timing_paths]]
	close_design
	set log_file [open $logFileName a]
	puts $log_file "${synth_name},${freq},${settings},${slack}"
	close $log_file
	set timings_met [expr {$slack >= 0}]

	set tries [expr {$tries + 1}]
	if {$timings_met} then {
		if {$tries == 1} then {set go_up true}
		if {$go_up} then {
			if {$freq >= $max_freq} then {
				set freq_found true
				set freq $max_freq
			} else {
				if {[expr {$freq + [lindex $freq_inc  $inc_val_iter]}] > $max_freq} then {
					if {$inc_val_iter < ([llength $freq_inc] - 1)} then {
						set inc_val_iter [expr {$inc_val_iter + 1}]
						set go_up false
						set freq $max_freq
					} else {
						set freq_found true
					}
				} else {
					set freq [expr {$freq + [lindex $freq_inc  $inc_val_iter]}]
				}
			}
		} else {
			if {$freq >= $max_freq} then {
				set freq_found true
			} else {
				if {$inc_val_iter < ([llength $freq_inc] - 1)} then {
					set inc_val_iter [expr {$inc_val_iter + 1}]
					set go_up true
					set freq [expr {$freq + [lindex $freq_inc  $inc_val_iter]}]
				} else {
					set freq_found true
				}
			}
		}
	} else {
		if {$tries == 1} then {set go_up false}
		if {$go_up} then {
			if {$inc_val_iter < ([llength $freq_inc] - 1)} then {
				set inc_val_iter [expr {$inc_val_iter + 1}]
				set go_up false
				set freq [expr {$freq - [lindex $freq_inc  $inc_val_iter]}]
			} else {
				set freq [expr {$freq - [lindex $freq_inc  $inc_val_iter]}]
				set freq_found true
			}
		} else {
			set freq [expr {$freq - [lindex $freq_inc  $inc_val_iter]}]
		}
	}
}
exit 0
