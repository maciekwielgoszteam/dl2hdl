`timescale 1ns/1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 11/15/2018 11:53:23 AM
// Design Name:
// Module Name: simple_net_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description: Simple testbench to perform network inference
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module simple_net_tb();

    // Simulation parameters
    parameter PERIOD = 10;
//    parameter net = "net_flat";

        // Common parameters
    parameter data_width = 16;

//        // Flat parameters
//    parameter data_len = 160;
//    parameter out_len = 40;
//    parameter data_path = "/home/michal/agh/workspace/ml/dl2hdl/tests/manual_tb/net_flat/data_vec.txt";
//    parameter last_path = "/home/michal/agh/workspace/ml/dl2hdl/tests/manual_tb/net_flat/last_vec.txt";
//    parameter output_path = "/home/michal/agh/workspace/ml/dl2hdl/tests/manual_tb/net_flat/output_vec.txt";

        // 1D parameters
    parameter data_len = 360;
    parameter out_len = 40;
    parameter data_path = "/home/michal/agh/workspace/ml/dl2hdl/tests/manual_tb/net_1d/data_vec.txt";
    parameter last_path = "/home/michal/agh/workspace/ml/dl2hdl/tests/manual_tb/net_1d/last_vec.txt";
    parameter output_path = "/home/michal/agh/workspace/ml/dl2hdl/tests/manual_tb/net_1d/output_vec.txt";

    bit                  clk;
    bit                  sresetn;

    reg [data_width-1:0] net_wr_axis_tdata;
    reg                  net_wr_axis_tvalid;
    reg                  net_wr_axis_tready;
    reg                  net_wr_axis_tlast;

    reg [data_width-1:0] net_rd_axis_tdata;
    reg                  net_rd_axis_tvalid;
    reg                  net_rd_axis_tready;
    reg                  net_rd_axis_tlast;

    reg [data_width-1:0] tdata_vec [data_len-1:0];
    reg                  tlast_vec [data_len-1:0];
    reg [data_width-1:0] output_true_vec [out_len-1:0];
    reg [data_width-1:0] output_net_vec [out_len-1:0];
    reg                  error_vec [out_len-1:0];

    bit                  ERROR;

    integer              i;
    integer              j;

        // Doad test data
    initial begin
        $readmemh(data_path, tdata_vec);
        $readmemb(last_path, tlast_vec);
        $readmemh(output_path, output_true_vec);
        $display("File loaded");
    end

        // Clock generation
    always begin
        clk = 1'b0;
        #(PERIOD/2);
        clk = 1'b1;
        #(PERIOD/2);
    end

        // Reset generation
    initial begin
        sresetn = 1'b0;
        #15 sresetn = 1'b1;
    end

//    net_flat#() UUT(
    net_1D#() UUT(
        .clk            (clk),
        .reset          (sresetn),
        .in_data_tdata  (net_wr_axis_tdata),
        .in_data_tready (net_wr_axis_tready),
        .in_data_tvalid (net_wr_axis_tvalid),
        .in_data_tlast  (net_wr_axis_tlast),
        .out_data_tdata (net_rd_axis_tdata),
        .out_data_tready(net_rd_axis_tready),
        .out_data_tvalid(net_rd_axis_tvalid),
        .out_data_tlast (net_rd_axis_tlast)
    );

        // Write stim
    initial begin
        net_wr_axis_tvalid = 'b0;
        net_wr_axis_tdata = 'b0;
        net_wr_axis_tlast = 'b0;
        i = 0;
        #PERIOD;
        forever begin
            net_wr_axis_tdata = tdata_vec[i];
            net_wr_axis_tlast = tlast_vec[i];
            net_wr_axis_tvalid = 1'b1;
            #PERIOD;
            if (net_wr_axis_tready == 1'b1) begin
                i = i+1;
            end
            if (i >= data_len) begin
                break;
            end
        end
        net_wr_axis_tvalid = 'b0;
        #(PERIOD*20);
        assert (ERROR == 0) else $error("TEST FAILED");
        if (ERROR == 0) begin
            $display("TEST PASSED");
        end
        $finish;
    end

        // Read stim
    initial begin
        net_rd_axis_tready = 1'b1;
        j = 0;
        #PERIOD;
        forever begin
            if (net_rd_axis_tvalid == 1'b1) begin
                output_net_vec[j] = net_rd_axis_tdata;
                j = j+1;
            end
            #PERIOD;
            if (j >= out_len) begin
            end
        end
    end

    integer              k;
    always begin
        #PERIOD;
        for (k = 0; k < out_len; k = k+1) begin
            if (k < j) begin
                if (output_true_vec[k] == output_net_vec[k]) begin
                    error_vec[k] = 1'b0;
                end else begin
                    error_vec[k] = 1'b1;
                end
            end
        end
    end

    assign ERROR = (error_vec == 0) ? 0:1;

endmodule
