import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from mpl_toolkits.mplot3d import Axes3D
from scipy import interpolate

x_vals = dict()
for i in range(31):
    x_vals[f'{2*(i+2)}-{i+2}'] = i

y_vals = dict()
for i in range(12):
    y_vals[f'{i+1}'] = i

X = np.arange(0, len(x_vals))
Y = np.arange(0, len(y_vals))
X_mesh, Y_mesh = np.meshgrid(X, Y)

freqs = np.full((len(X), len(Y)), np.nan)
freqs[x_vals['8-4']][y_vals['4']] = 200.
freqs[x_vals['8-4']][y_vals['6']] = 200.
freqs[x_vals['8-4']][y_vals['8']] = 140.
freqs[x_vals['8-4']][y_vals['10']] = 140.
freqs[x_vals['8-4']][y_vals['12']] = 100.

freqs[x_vals['12-6']][y_vals['4']] = 200.
freqs[x_vals['12-6']][y_vals['6']] = 200.
freqs[x_vals['12-6']][y_vals['8']] = 140.
freqs[x_vals['12-6']][y_vals['10']] = 110.
# freqs[x_vals['12-6']][y_vals['12']] = 0.

freqs[x_vals['16-8']][y_vals['4']] = 200.
freqs[x_vals['16-8']][y_vals['6']] = 190.
freqs[x_vals['16-8']][y_vals['8']] = 130.
freqs[x_vals['16-8']][y_vals['10']] = 110.
freqs[x_vals['16-8']][y_vals['12']] = 0.

freqs[x_vals['20-10']][y_vals['4']] = 200.
freqs[x_vals['20-10']][y_vals['6']] = 180.
freqs[x_vals['20-10']][y_vals['8']] = 110.
freqs[x_vals['20-10']][y_vals['10']] = 0.
freqs[x_vals['20-10']][y_vals['12']] = 0.

freqs[x_vals['32-16']][y_vals['4']] = 200.

mask = ~np.isnan(freqs)

points = mask.nonzero()
values = freqs[points]
gridcoords = np.meshgrid(freqs.shape[0], freqs.shape[1])

out_foo = interpolate.interp2d(*points, values, kind='linear')  # {‘linear’, ‘cubic’, ‘quintic’}
outgrid = out_foo(X, Y)

# fig = plt.figure()
# ax = fig.gca(projection='3d')
# plt.figure(2)
# _ = ax.scatter(points[0], points[1], values)
# plt.xticks(np.arange(len(x_vals)), [x for x in x_vals])
# plt.yticks(np.arange(len(y_vals)), [x for x in y_vals])
plt.figure(1)
plt.contourf(X_mesh, Y_mesh, outgrid)
plt.xticks(np.arange(len(x_vals)), [x for x in x_vals])
plt.yticks(np.arange(len(y_vals)), [x for x in y_vals])
plt.show()

# fn = RegularGridInterpolator((X, Y, Z), freqs, bounds_error=False, fill_value=None)
#
# plt.yticks(np.arange(len(x_vals)), [x for x in x_vals])
# plt.xticks(np.arange(len(y_vals)), [x for x in y_vals])
#
# pts = np.array([[0, 3]])
# extrapolated_pts = fn(pts)
# print(extrapolated_pts)
# Z = freq
#
# surf = ax.scatter(X_mesh, Y_mesh, Z, c='b')
# plt_pts = ax.scatter(pts[0][0], pts[0][1], extrapolated_pts[0], c='r')
# plt.show()
